using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;
 
public class ChangeIOSBuildNumber 
{
 
    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
 
        if (buildTarget == BuildTarget.iOS)
        {
       
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
       
            // Get root
            PlistElementDict rootDict = plist.root;
       
            // Change value of CFBundleVersion in Xcode plist
            rootDict.SetString("Privacy - Photo Library Usage Description", "photo use");
            rootDict.SetString("Privacy - Camera Usage Description", "camera use");
       
            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}