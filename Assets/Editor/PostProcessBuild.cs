﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

namespace platformer.utils
{
	public class PostProcessBuild
	{
		[PostProcessBuild(1000)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
		{
			#if UNITY_IOS
			ProcessPostBuild(pathToBuiltProject);
			#endif
		}

		private static void ProcessPostBuild(string path)
		{
			#if UNITY_IOS
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject();
			proj.ReadFromFile(projPath);

			string target = proj.TargetGuidByName("Unity-iPhone");

			proj.AddBuildProperty(target, "ENABLE_BITCODE", "NO");
			proj.WriteToFile(projPath);
			#endif
		}
	}
}