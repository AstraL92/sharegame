Shader "Sprites/Face"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _FaceTex ("Face Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
        _Fade ("Fade", Float) = 0
        _FadeColor ("Fade Color", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
                half2 faceTexcoord : TEXCOORD1;
			};
			
			fixed4 _Color;
            float _Fade;
            fixed4 _FadeColor;
            
            float4 _FaceTex_ST;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
                OUT.faceTexcoord = TRANSFORM_TEX(IN.texcoord, _FaceTex);
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
            sampler2D _FaceTex;

			fixed4 frag(v2f IN) : SV_Target
			{
                float4 c = tex2D(_FaceTex, IN.faceTexcoord) * IN.color;
                float mask = tex2D(_MainTex, IN.texcoord).a;
                c.a *= mask;
                c.rgb = c.rgb*(1.0 - _Fade) + _Fade*_FadeColor.rgb;
				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
}
