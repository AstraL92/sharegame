﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class ImagePickerPlugin : MonoBehaviour
{
    static public ImagePickerPlugin instance { get; private set; }

    public enum Source
    {
        Camera = 0,
        Albums = 1
    };

    public delegate void TextureLoadedDelegate(Texture2D texture);

    private string filePath;
    private TextureLoadedDelegate textureLoadedCallback;

    void Awake()
    {
        _.Assert(instance == null);
        instance = this;
    }

    public void PickImage(Source source, TextureLoadedDelegate textureLoadedCallback)
    {
        Debug.Log("pick image");
        _.Assert(this.textureLoadedCallback == null);

        Debug.Log("_.Assert done");
        filePath = Application.temporaryCachePath + "/pickedImage.jpg";
        this.textureLoadedCallback = textureLoadedCallback;

        Debug.Log("File path : " + filePath);
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Debug.Log(" This is iPhone!!!");
            ImagePickerPluginPickImage((int)source, filePath);
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            System.IO.File.Copy(Application.dataPath + "/../TestData/testPicture.jpg", filePath, true);
            OnImageSaved("success (editor)");
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            textureLoadedCallback(Texture2D.whiteTexture);
        }
    }

    void OnImageSaved(string message)
    {
        StartCoroutine(TextureLoadCoroutine());
    }

    IEnumerator TextureLoadCoroutine()
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        string prefix = "file:///";
#else
        string prefix = "file://";
#endif

        WWW loader = new WWW(prefix + filePath);
        yield return loader;

        Texture2D texture = new Texture2D(1, 1, TextureFormat.RGB24, true, true);
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.filterMode = FilterMode.Trilinear;
        loader.LoadImageIntoTexture(texture);
        loader.Dispose();

        textureLoadedCallback(texture);

        filePath = null;
        textureLoadedCallback = null;
    }

    [DllImport("__Internal")]
    static private extern void ImagePickerPluginPickImage(int source, string filePath);
}
