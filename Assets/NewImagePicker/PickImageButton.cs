﻿using UnityEngine;
using UnityEngine.UI;

public class PickImageButton : MonoBehaviour
{
    public Image TargetImage;
    public ImageType ImageType;

    public void PickFromGallery()
    {
        StansImagePicker.SpriteType = (int)ImageType;
        IOSCamera.Instance.PickImage(ISN_ImageSource.Album);
    }

    public void PickFromCamera()
    {
        StansImagePicker.SpriteType = (int)ImageType;
        IOSCamera.Instance.PickImage(ISN_ImageSource.Camera);
    }
}
