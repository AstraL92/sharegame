﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json.Linq;

public static class _
{
    static public void Assert(bool expression)
    {
        if (!expression)
        {
#if UNITY_EDITOR
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(true);
            System.Diagnostics.StackFrame stackFrame = stackTrace.GetFrame(1);
            
            string fileName = stackFrame.GetFileName();
            int lineNumber = stackFrame.GetFileLineNumber();
            
            string message = "Assertion failed: file " + fileName + ", line " + lineNumber;
            
            Debug.LogError(message);
            
            if (UnityEditor.EditorUtility.DisplayDialog("Assertion failed", message, "Break", "Ignore"))
            {
                Debug.Break();
                UnityEditorInternal.InternalEditorUtility.OpenFileAtLineExternal(fileName, lineNumber);
            }
#else
            Debug.LogError("Assertion failed");
#endif
        }
    }

    static public float fmod(float a, float b)
    {
        return a - Mathf.Floor(a/b)*b;
    }

    static public bool AreAABBIntersected(Vector2 aMin, Vector2 aMax, Vector2 bMin, Vector2 bMax)
    {
        return (aMin.x <= bMax.x) && (aMin.y <= bMax.y) && (aMax.x >= bMin.x) && (aMax.y >= bMin.y);
    }

    static public System.DateTime? ParseIsoDateTime(string isoStr)
    {
        System.DateTime dateTime;
        if (System.DateTime.TryParseExact(isoStr, new string[] { "yyyy'-'MM'-'dd'T'HH':'mm':'ssK", "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK" }, null, System.Globalization.DateTimeStyles.RoundtripKind, out dateTime))
            return dateTime;
        else
            return null;
    }

    static public string DateTimeToIsoString(System.DateTime? dateTime)
    {
        if (dateTime != null)
            return dateTime.Value.ToString("o");
        else
            return null;
    }

    static public T FindInChildren<T>(Component component, string name) where T : Component
    {
        T[] childComponents = component.GetComponentsInChildren<T>();
        foreach (T childComponent in childComponents)
        {
            if (childComponent.gameObject.name == name)
            {
                return childComponent;
            }
        }

        return null;
    }

    static public void DestoryAllChildren(GameObject gameObject)
    {
        Transform[] children = gameObject.transform.GetComponentsInChildren<Transform>();
        foreach (Transform child in children)
        {
            if (child == gameObject.gameObject.transform)
            {
                continue;
            }
            
            child.parent = null;
            Object.Destroy(child.gameObject);
        }
    }

    static public Texture2D CropResizeTexture(Texture2D srcTexture, int targetWidth, int targetHeight, Vector2 textureScale, Vector2 textureOffset)
    {
        Material resizeMaterial = new Material(Shader.Find("Custom/Resize")); // this shader should be included in build!
        resizeMaterial.SetVector("_ResizeParams", new Vector4(textureScale.x, textureScale.y, textureOffset.x, textureOffset.y));
        
        RenderTexture renderTexture = new RenderTexture(targetWidth, targetHeight, 0, RenderTextureFormat.ARGB32);
        Graphics.Blit(srcTexture, renderTexture, resizeMaterial);

        RenderTexture prevActiveRenderTexture = RenderTexture.active;
        RenderTexture.active = renderTexture;

        Texture2D dstTexture = new Texture2D(targetWidth, targetHeight, TextureFormat.RGB24, false, false);
        dstTexture.ReadPixels(new Rect(0, 0, targetWidth, targetHeight), 0, 0);
        dstTexture.Apply();

        RenderTexture.active = prevActiveRenderTexture;

        return dstTexture;
    }
}

public static class _Extensions
{
    static public T FindInChildren<T>(this GameObject go, string name) where T : Component
    {
        return _.FindInChildren<T>(go.transform, name);
    }

    static public T FindInChildren<T>(this Component component, string name) where T : Component
    {
        return _.FindInChildren<T>(component, name);
    }

    static public JObject ToJObject(this Vector2 vector2)
    {
        return new JObject { { "x", vector2.x }, { "y", vector2.y } };
    }

    static public JObject ToJObject(this Vector3 vector3)
    {
        return new JObject { { "x", vector3.x }, { "y", vector3.y }, { "z", vector3.z } };
    }

    static public Vector2 ToVector2(this JToken token)
    {
        return new Vector2(token["x"].Value<float>(), token["y"].Value<float>());
    }

    static public Vector3 ToVector3(this JToken token)
    {
        return new Vector3(token["x"].Value<float>(), token["y"].Value<float>(), token["z"].Value<float>());
    }
};
