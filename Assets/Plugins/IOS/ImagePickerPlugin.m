//
//  ImagePickerPlugin.m
//  Unity-iPhone
//
//  Created by Pavel Pryamikov on 04.08.15.
//
//

#import <Foundation/Foundation.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientationAndResize:(int)maxSize;

@end

@implementation UIImage (fixOrientation)

- (UIImage *)fixOrientationAndResize:(int)maxSize {
    
    // No-op if the orientation is already correct
    //if (self.imageOrientation == UIImageOrientationUp) return self;
    
    CGFloat scale = 1.0;
    int maxDimension = self.size.width > self.size.height ? self.size.width : self.size.height;
    
    if (maxDimension > maxSize)
    {
        scale = (CGFloat)maxSize/(CGFloat)maxDimension;
    }
    
    int targetWidth = (int)round(self.size.width*scale);
    int targetHeight = (int)round(self.size.height*scale);
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, targetWidth, targetHeight);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, targetWidth, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, targetHeight);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, targetWidth, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, targetHeight, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, targetWidth, targetHeight,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,targetHeight,targetWidth), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,targetWidth,targetHeight), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end


@interface ImagePickerPlugin : NSObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (copy, nonatomic) NSString *filePath;

- (void)pickImage:(int)source;

@end

@implementation ImagePickerPlugin

- (void)pickImage:(int)source {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = source == 0 ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (source == 0 && [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow.rootViewController presentViewController:imagePickerController animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [info[UIImagePickerControllerOriginalImage] fixOrientationAndResize:1024];
        
        NSData *imageJPEGData = UIImageJPEGRepresentation(image, 1.0);
        [imageJPEGData writeToFile:self.filePath atomically:NO];
        
        UnitySendMessage("Image Picker Plugin", "OnImageSaved", "success");
    });
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end

void ImagePickerPluginPickImage(int source, const char* filePath)
{
    static ImagePickerPlugin *imagePickerPlugin = nil;
    if (!imagePickerPlugin) {
        imagePickerPlugin = [[ImagePickerPlugin alloc] init];
    }
    
    imagePickerPlugin.filePath = [NSString stringWithUTF8String:filePath];
    [imagePickerPlugin pickImage:source];
}



