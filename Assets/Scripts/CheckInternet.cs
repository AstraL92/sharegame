﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class CheckInternet : MonoBehaviour
    {
        public GameObject Connecting;
        private bool _isConnected;
        public void Recconect()
        {
            StartCoroutine(CheckInternetConnection());
        }  

         private IEnumerator CheckInternetConnection()
        {
            Connecting.SetActive(true);
            WWW www = new WWW("http://google.com");
            yield return www;

            _isConnected = www.error == null;

            if (_isConnected)
            {
                Connecting.SetActive(false);
                gameObject.SetActive(false);
            }
            else
            {
                Connecting.SetActive(false);
            }
        }
    }
}