﻿using UnityEngine;
using UnityEngine.UI;

public class ChooseBackgroundImagesHandler : MonoBehaviour
{
    public Image BackgroundImage;
    public Image WinImage;
    public Image LoseImage;

    public void SetImages()
    {
        Debug.Log("SetImages");

        if (LevelConstriuctorEnterPoint.CurrentLevel.Background != null)
        {
            BackgroundImage.sprite.texture.LoadImage(LevelConstriuctorEnterPoint.CurrentLevel.Background);
        }

        if (LevelConstriuctorEnterPoint.CurrentLevel.WinImage != null)
        {
            WinImage.sprite.texture.LoadImage(LevelConstriuctorEnterPoint.CurrentLevel.WinImage);
        }

        if (LevelConstriuctorEnterPoint.CurrentLevel.LoseImage != null)
        {
            LoseImage.sprite.texture.LoadImage(LevelConstriuctorEnterPoint.CurrentLevel.LoseImage);
        }
    }
}
