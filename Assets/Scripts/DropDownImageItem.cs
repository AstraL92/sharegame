﻿using UnityEngine;
using UnityEngine.UI;

public class DropDownImageItem : MonoBehaviour
{
    public Sprite Block;
    public Sprite Trap;
    public Sprite Item;
    public Sprite Mechanism;
    public Sprite Main;
    public Sprite Monster;

    public GameObject Bg;
    public RectTransform Arrow;
    
    private void Start()
    {
        Bg.SetActive(true);
        Arrow.rotation = new Quaternion(1, 0, 0, 0);

        gameObject.transform.GetChild(1).FindChild("Item Background").GetComponent<Image>().color = new Color(255,255,255,0);
        gameObject.transform.GetChild(2).FindChild("Item Background").GetComponent<Image>().sprite = Block;
        gameObject.transform.GetChild(3).FindChild("Item Background").GetComponent<Image>().sprite = Trap;
        gameObject.transform.GetChild(4).FindChild("Item Background").GetComponent<Image>().sprite = Item;
        gameObject.transform.GetChild(5).FindChild("Item Background").GetComponent<Image>().sprite = Mechanism;
        gameObject.transform.GetChild(6).FindChild("Item Background").GetComponent<Image>().sprite = Main;
        gameObject.transform.GetChild(7).FindChild("Item Background").GetComponent<Image>().sprite = Monster;
    }
   
    private void OnDisable()
    {
        Bg.SetActive(false);
        Arrow.rotation = new Quaternion(0, 0, 0, 0);
    }
}