﻿using Assets.Scripts.MenuControllers.Login;
using platformer.player;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayWinLosePopUp : MonoBehaviour
{
    private string _playerTag = "Player";

    [Header("Other text")]
    public Text PlayerNickname;
    public Text CurrTimeText;
    public Text PopUpTimer;

    [Header("Hearth")]
    public GameObject HP1;
    public GameObject HP2;
    public GameObject HP3;

    [Header ("Private fields")]

    [SerializeField]
    private PlayerController _playerController;

    [SerializeField]
    private int _hp;

    [SerializeField]
    private string _playerNickname;

    [SerializeField]
    private Text _healtText;

    [SerializeField]
    private string _healtString = "{0} Health left";

    private void OnEnable()
    {
        _playerController = GameObject.FindGameObjectWithTag(_playerTag).GetComponent<PlayerController>();

        _hp = _playerController.Model.CurrentHP;

        //_playerNickname = LoginController.UserNameStatic;

        //PlayerNickname.text = _playerNickname;
        Debug.Log("curr hp : " + _hp);
        PopUpTimer.text = CurrTimeText.text;

        SetCurrentHpIndicator(_hp);
    }

    private void SetCurrentHpIndicator(int currHP)
    {
        switch (currHP)
        {
            case 3:
                SetHealthText(currHP);
                break;
            case 2:
                HP3.SetActive(false);
                SetHealthText(currHP);

                break;
            case 1:
                HP3.SetActive(false);
                HP2.SetActive(false);
                SetHealthText(currHP);

                break;
            case 0:
                HP3.SetActive(false);
                HP2.SetActive(false);
                HP1.SetActive(false);
                SetHealthText(currHP);

                break;

            default:
                break;
        }
    }

    private void SetHealthText(int hp)
    {
        if (hp != 0)
            _healtText.text = string.Format(_healtString, hp);
        else
            _healtText.text = "No Healt left";
    }
}
