﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class ArrowHandler : MonoBehaviour
{

    public Action OnShowArrow;
    public Action OnHideArrow;

    public GameObject parentArrows;

    private GameObject left;
    private GameObject right;
    private GameObject top;
    private GameObject bottom;

    [HideInInspector]
    public CellHandler cellHendler;
    private void GetCellHendler()
    {
        cellHendler = GetComponent<CellHandler>();
    }

    void Start ()
    {
        Init();
	}

    private void Init()
    {
        left = parentArrows.transform.GetChild(0).gameObject;
        right = parentArrows.transform.GetChild(1).gameObject;
        top = parentArrows.transform.GetChild(2).gameObject;
        bottom = parentArrows.transform.GetChild(3).gameObject;


        OnShowArrow = Show;
        OnHideArrow = Hide;

        GetCellHendler();
    }

	void OnDisable()
	{
		OnShowArrow = null;
		OnHideArrow = null;
	}
	
    private void ActiveArrow()
    {
        var list_arrow = cellHendler.GetNeighborsForDuplicate().Where(id => id.Value.CellType ==  CellType.Empty).ToList ();

        foreach (var item in list_arrow)
        {
            if (item.Key == NeignborsEnum.BOTTOM)
            {
                bottom.SetActive(true);
            }
            if (item.Key == NeignborsEnum.TOP)
            {
                top.SetActive(true);
            }
            if (item.Key == NeignborsEnum.LEFT)
            {
                left.SetActive(true);
            }
            if (item.Key == NeignborsEnum.RIGHT)
            {
                right.SetActive(true);
            }
        }

    }

    private void Show()
    {
        parentArrows.SetActive(true);

        //Debug.LogWarning("show");

        ActiveArrow();
    }

    private void Hide()
    {
        right.SetActive(false);
        top.SetActive(false);
        bottom.SetActive(false);
        left.SetActive(false);

        //Debug.LogWarning("hide");

        parentArrows.SetActive(false);
    }

    public void Left()
    {
        Duplicate(NeignborsEnum.LEFT);
        //Debug.LogWarning("left");
    }

    private void Duplicate(NeignborsEnum num)
    {
        cellHendler.GetNeighborsForDuplicate()[num].SetOverradeSprite(cellHendler.GetOverrideSprite());
        cellHendler.GetNeighborsForDuplicate()[num].CellType = GridHandler.ActiveCell.CellType;
        Hide();
        GridHandler.ActiveCell = cellHendler.GetNeighborsForDuplicate()[num];
        GridHandler.ActiveCell.arrowHandler.OnShowArrow.Invoke();
    }

    public void Right()
    {
        //Debug.LogWarning("RIGHT");
        Duplicate(NeignborsEnum.RIGHT);
    }

    public void Top()
    {
        //Debug.LogWarning("TOP");
        Duplicate(NeignborsEnum.TOP);
    }

    public void Bottom()
    {
        //Debug.LogWarning("BOTTOM");
        Duplicate(NeignborsEnum.BOTTOM);
    }

}
