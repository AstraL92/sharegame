﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class BorderHandler : MonoBehaviour
{

    public GameObject parentBorder;

    public Action OnShowBorder;
    public Action OnHideBorder;

    private GameObject left;
    private GameObject right;
    private GameObject top;
    private GameObject bottom;

    [Serializable]
    public struct BorderOn
    {
        public bool left;
        public bool right;
        public bool top;
        public bool bottom;
    }
    private BorderOn borderOn;

    [HideInInspector]
    public CellHandler cellHendler;
    private void GetCellHendler()
    {
        cellHendler = GetComponent<CellHandler>();
    }

    void Start()
    {
        Init();
    }

    private void Init()
    {
        borderOn = new BorderOn();

        left = parentBorder.transform.GetChild(0).gameObject;
        right = parentBorder.transform.GetChild(1).gameObject;
        top = parentBorder.transform.GetChild(2).gameObject;
        bottom = parentBorder.transform.GetChild(3).gameObject;


        OnShowBorder = Show;
        OnHideBorder = Hide;

        GetCellHendler();
    }

    public BorderOn ShowBorderForCopy
    {
        get
        {
            return borderOn;
        }
        set
        {
            borderOn = value;

            parentBorder.SetActive(true);

            if (borderOn.bottom == true)
            {
                bottom.SetActive(true);
            }
            if (borderOn.top == true)
            {
                top.SetActive(true);
            }
            if (borderOn.left == true)
            {
                left.SetActive(true);
            }
            if (borderOn.right == true)
            {
                right.SetActive(true);
            }
        }
    }

    private void ActiveBorder()
    {
        var list_border = cellHendler.GetNeighborsForDuplicate().Where(id => id.Value.CellType == CellType.Empty || id.Value.CellType != cellHendler.CellType).ToList();

        foreach (var item in list_border)
        {
            if (item.Key == NeignborsEnum.BOTTOM || !cellHendler.GetNeighborsForDuplicate().ContainsKey(NeignborsEnum.BOTTOM))
            {
                bottom.SetActive(true);
                borderOn.bottom = true;
            }
            if (item.Key == NeignborsEnum.TOP || !cellHendler.GetNeighborsForDuplicate().ContainsKey(NeignborsEnum.TOP))
            {
                top.SetActive(true);
                borderOn.top = true;
            }
            if (item.Key == NeignborsEnum.LEFT || !cellHendler.GetNeighborsForDuplicate().ContainsKey(NeignborsEnum.LEFT))
            {
                left.SetActive(true);
                borderOn.left = true;
            }
            if (item.Key == NeignborsEnum.RIGHT || !cellHendler.GetNeighborsForDuplicate().ContainsKey(NeignborsEnum.RIGHT))
            {
                right.SetActive(true);
                borderOn.right = true;
            }
        }

		if (cellHendler.X == 0) 
		{
			left.SetActive(true);
			borderOn.left = true;
		}

		if (cellHendler.X == GridHandler.ColumnCount - 1) 
		{
			right.SetActive(true);
			borderOn.right = true;
		}

		if (cellHendler.Y == 0) 
		{
			top.SetActive(true);
			borderOn.top = true;
		}

		if (cellHendler.Y == GridHandler.RowCount - 1) 
		{
			bottom.SetActive(true);
			borderOn.bottom = true;
		}
    }

    private void Show()
    {
        parentBorder.SetActive(true);

        ActiveBorder();
    }

    private void Hide()
    {
        right.SetActive(false);
        top.SetActive(false);
        bottom.SetActive(false);
        left.SetActive(false);

        borderOn.bottom = false;
        borderOn.top = false;
        borderOn.left = false;
        borderOn.right = false;

        parentBorder.SetActive(false);
    }
}
