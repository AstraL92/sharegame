﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellHandler : MonoBehaviour
{
    [SerializeField]
    private CellType _type;

    [SerializeField]
    public int X;

    [SerializeField]
    public int Y;

    [SerializeField]
    private Image _selfImage;

	[SerializeField]
	private Sprite _overrideSprite;

	[Header("Drag n Drop")]
	[SerializeField]
	private DragMe _selfDrag;

	[SerializeField]
	private DropMe _selfDrop;

	[SerializeField]
	private CellLongTap _selfLongTap;

	[SerializeField]
	private DoubleTapMe _selfDoubleTap;

    private Dictionary<NeignborsEnum, CellHandler> neighborsDictionaryForDuplicate;

    public Action<bool> CheckNeighbors;

    private GridHandler gridRef;
    public GridHandler GridRef
    {
        get
        {
            return gridRef;
        }
        set
        {
            gridRef = value;
        }
    }

    public BorderHandler borderHandler;
    private void GetBorderHandler()
    {
        borderHandler = GetComponent<BorderHandler>();
    }

    public ArrowHandler arrowHandler;
    private void GetArrowHandler()
    {
        arrowHandler = GetComponent<ArrowHandler>();
    }

    void Start()
	{
        Init();
        GetNeighbors();

        StartCellConfig ();
		GetSelfImage ();

        GetArrowHandler();
        GetBorderHandler();

    }

    private void GetNeighbors()
    {
        // left
        if (X > 0)
        {
            neighborsDictionaryForDuplicate.Add(NeignborsEnum.LEFT, GridHandler.GetCellDictionary()[new Vector2(X - 1, Y)]);
        }

        // right
        if (X < GridHandler.RowCount - 1)
        {
            neighborsDictionaryForDuplicate.Add(NeignborsEnum.RIGHT, GridHandler.GetCellDictionary()[new Vector2(X + 1, Y)]);
        }

        // top
        if (Y > 0)
        {
            neighborsDictionaryForDuplicate.Add(NeignborsEnum.TOP, GridHandler.GetCellDictionary()[new Vector2(X, Y - 1)]);
        }

        // bottom
        if (Y < GridHandler.ColumnCount - 1)
        {
            neighborsDictionaryForDuplicate.Add(NeignborsEnum.BOTTOM, GridHandler.GetCellDictionary()[new Vector2(X, Y + 1)]);
        }
    }

    public bool CheckCellType(CellType typeCkeck)
    {
        return (typeCkeck == CellType) ? true : false;
    }

    public void EnableRay(bool enable)
    {
        _selfImage.raycastTarget = enable;
    }

    public Dictionary<NeignborsEnum, CellHandler> GetNeighborsForDuplicate()
    {
        return neighborsDictionaryForDuplicate;
    }

    private void SelectNeighbors(bool brakeMethod = false)
    {
        if (brakeMethod)
            return;

        bool left = SelectSameObj(NeignborsEnum.LEFT);
        bool right = SelectSameObj(NeignborsEnum.RIGHT);
        bool top = SelectSameObj(NeignborsEnum.TOP);
        bool bottom = SelectSameObj(NeignborsEnum.BOTTOM);
        
        if (bottom)
        {
            neighborsDictionaryForDuplicate[NeignborsEnum.BOTTOM].CheckNeighbors.Invoke (false);
        }
        if (top)
        {
            neighborsDictionaryForDuplicate[NeignborsEnum.TOP].CheckNeighbors.Invoke (false);
        }
        if (left)
        {
            neighborsDictionaryForDuplicate[NeignborsEnum.LEFT].CheckNeighbors.Invoke(false);
        }
        if (right)
        {
            neighborsDictionaryForDuplicate[NeignborsEnum.RIGHT].CheckNeighbors.Invoke(false);
        }

    }

    private bool SelectSameObj(NeignborsEnum num)
    {
        if (neighborsDictionaryForDuplicate.ContainsKey(num) && CheckCellType(neighborsDictionaryForDuplicate[num].CellType))
        {
            if (gridRef.cellGroupSame.ContainsValue(neighborsDictionaryForDuplicate[num]))
            {
                neighborsDictionaryForDuplicate[num].CheckNeighbors.Invoke(true);
                return false;
            }
            else
            {
                gridRef.cellGroupSame.Add(new Vector2(neighborsDictionaryForDuplicate[num].X, neighborsDictionaryForDuplicate[num].Y), neighborsDictionaryForDuplicate[num]);
                return true;
            }
        }
        return false;
    }

    private void Init()
    {
        neighborsDictionaryForDuplicate = new Dictionary<NeignborsEnum, CellHandler>();

        CheckNeighbors = SelectNeighbors;
    }

    void StartCellConfig ()
	{
		_selfDrag = GetComponent<DragMe> ();
		_selfDrop = GetComponent<DropMe> ();

		DragEnable (false);
	}

	public void DragEnable(bool b)
	{
		_selfDrag.enabled = b; 
	}

	public void DropEnable (bool b)
	{
		_selfDrop.enabled = b;
	}

	public void LongTapEnable (bool b)
	{
		_selfLongTap.enabled = b;
	}

	public void DoubleTapEnable (bool b)
	{
		_selfDoubleTap.enabled = b;
	}

    private void GetSelfImage()
    {
        _selfImage = GetComponent<Image>();
    }

    public Image SelfImage
    {
        get
        {
            return _selfImage;
        }
    }

    public void ChangeColor(Color color)
    {
        _selfImage.color = color; 
    }

    public CellType CellType
    {
        get
        {
            return _type;
        }
        set
        {
            _type = value;
        }
    }

    public void SetCellArrayPos(int x, int y)
    {
        X = x;
        Y = y;
    }

    public void SetOverradeSprite()
    {
        _selfImage.overrideSprite = _overrideSprite;
    }

    public Sprite GetOverrideSprite()
    {
        return _selfImage.overrideSprite;
    }

    public void SetOverradeSprite(Sprite sprite)
    {
        _selfImage.overrideSprite = sprite;
    }

	public void Deselect()
	{
		borderHandler.OnHideBorder ();
		_selfLongTap.GreenCellBorder.SetActive (false);
		DragEnable(false);
		DropEnable(true);
	}

    public void Delete()
	{
        DragEnable(false);
        DropEnable(true);
        CellType = CellType.Empty;
		_selfImage.overrideSprite = null;
	}
}