﻿using UnityEngine;
using System.Collections;
using HedgehogTeam.EasyTouch;
using System;

[RequireComponent (typeof(BoxCollider2D), typeof(CellHandler))]
public class CellLongTap : MonoBehaviour
{
	public GameObject GreenCellBorder;
	
	public static Action LongTap;

	private CellHandler cellHandler;

	void Start ()
	{
		EasyTouch.On_LongTapStart -= On_LongTapStart;

		EasyTouch.On_LongTapStart += On_LongTapStart;

		cellHandler = GetComponent<CellHandler> ();
	}

	void OnEnable ()
	{
	}

	void OnDisable ()
	{
		EasyTouch.On_LongTapStart -= On_LongTapStart;
	}

	void OnDestroy ()
	{
		EasyTouch.On_LongTapStart -= On_LongTapStart;
	}

	private void On_LongTapStart (Gesture gesture)
	{
		if (gesture.pickedObject == gameObject) 
		{
			print ("Long tap");

			if (cellHandler.CellType != CellType.Empty) 
			{
				cellHandler.GridRef.CellToolsPanel.Cancel ();
				
				if (GridHandler.ActiveCell != null) 
				{
					GridHandler.ActiveCell.GridRef.CellToolsPanel.MoveBtn.isOn = true;
				}
				GridHandler.ActiveCell = cellHandler;
				GridHandler.ActiveCell.GridRef.CellToolsPanel.DuplicateBtn.isOn = false;

				GreenCellBorder.transform.SetParent (GridHandler.ActiveCell.gameObject.transform);

				GreenCellBorder.SetActive (true);

				cellHandler.DragEnable (true);

				if (LongTap != null) 
				{
					LongTap ();
				}
			}
		}
	}

}
