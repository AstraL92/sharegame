﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CellTools : MonoBehaviour
{
	[Header ("Tools buttons")]
	public Toggle MoveBtn;
	public Button CopyBtn;
	public Toggle DuplicateBtn;
	public Button DeleteBtn;
	public Button CancelBtn;

	[Space (10)]
	public GridHandler gridRef;


	[Space (10)]
	public GameObject parent;

	//public Action OnShow;
	//public Action OnHide;

	void Awake ()
	{
		//OnShow = Show;
		//OnHide = Hide;
	}

	void Start ()
	{
	}

	void OnEnable ()
	{
		//OnShow = Show;
		//OnHide = Hide;
	}

	void OnDisable ()
	{
		//OnShow = null;
		//OnHide = null;
	}

	public void Move ()
	{
		GridHandler.ActiveCell.arrowHandler.OnHideArrow.Invoke ();

		GridHandler.ActiveCell.DragEnable (true);

		gridRef.EnableRayCast.Invoke (true);
	}

	public void Copy ()
	{
		
	}

	public void Duplicate ()
	{
		GridHandler.ActiveCell.arrowHandler.OnShowArrow.Invoke ();

		GridHandler.ActiveCell.DragEnable (false);

		gridRef.EnableRayCast.Invoke (false);
	}

	public void Delete ()
	{
		foreach (var item in gridRef.cellGroupSame) {
			item.Value.Delete ();
		}

		gridRef.DeselectAllCells ();
		gridRef.ClearSameDictionary.Invoke ();

		if (GridHandler.ActiveCell != null) 
		{
			GridHandler.ActiveCell.Delete ();
			GridHandler.ActiveCell.arrowHandler.OnHideArrow.Invoke ();
		}

		SetActive (false);

		gridRef.EnableRayCast.Invoke (true);

		InteractibleBtn (BtnCellTools.DUPLICATE, true);
		InteractibleBtn (BtnCellTools.MOVE, true);
	}

	public void Cancel ()
	{
		if (GridHandler.ActiveCell != null) 
		{
			GridHandler.ActiveCell.arrowHandler.OnHideArrow.Invoke ();
			GridHandler.ActiveCell.DragEnable (false);
		}

		gridRef.EnableRayCast.Invoke (true);
		gridRef.DeselectAllCells ();
		gridRef.ClearSameDictionary.Invoke ();

		InteractibleBtn (BtnCellTools.DUPLICATE, true);
		InteractibleBtn (BtnCellTools.MOVE, true);

		SetActive (false);
	}


	public void Hide ()
	{
		SetActive (false);
		InteractibleBtn (BtnCellTools.DUPLICATE, true);
		InteractibleBtn (BtnCellTools.MOVE, true);
	}

	public void Show ()
	{
		Interactible ();

		SetActive (true);
		if (gridRef.cellGroupSame.Count > 1) {
			InteractibleBtn (BtnCellTools.MOVE, false);
			InteractibleBtn (BtnCellTools.DUPLICATE, false);
		}
	}

	public void Interactible ()
	{
		DuplicateBtn.isOn = false;
		MoveBtn.isOn = true;

		if (GridHandler.ActiveCell.CellType == CellType.Player || GridHandler.ActiveCell.CellType == CellType.Door) {
			InteractibleBtn (BtnCellTools.DUPLICATE, false);
		} else {
			InteractibleBtn (BtnCellTools.DUPLICATE, true);
		}
	}

	public void InteractibleBtn (BtnCellTools type, bool active)
	{
		switch (type) {
		case BtnCellTools.MOVE:
			MoveBtn.interactable = active;
			break;
		case BtnCellTools.DUPLICATE:
			DuplicateBtn.interactable = active;
			break;
		case BtnCellTools.COPY:
			CopyBtn.interactable = active;
			break;
		case BtnCellTools.DELETE:
			DeleteBtn.interactable = active;
			break;
		case BtnCellTools.CANCEL:
			CancelBtn.interactable = active;
			break;
		default:
			break;
		}
	}

	private void SetActive (bool active)
	{
		parent.SetActive (active);
	}

}