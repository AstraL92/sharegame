using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField]private Sprite _greenCellImage;
	private Sprite _previousCellImage;
	
	public CellHandler SelfCellHandler;
    public Image receivingImage;
	private Color normalColor;

    public void OnDrop(PointerEventData data)
    {
        
        if (receivingImage == null)
            return;

        Sprite dropSprite = GetDropSprite(data);

        if (dropSprite != null)
        {
            receivingImage.overrideSprite = dropSprite;
            _previousCellImage = dropSprite;

            DeleteDuplicateElement(data, CellType.Player);
            DeleteDuplicateElement(data, CellType.Door);

            SelfCellHandler.CellType = GetDropId(data);
            SelfCellHandler.GridRef.RecentElements.AddRecentElement(SelfCellHandler.CellType);

            try
            {
                GridHandler.ActiveCell.arrowHandler.OnHideArrow();
            }
            catch (System.Exception)
            {
                Debug.Log("No arrow handler");
            }

            if (GridHandler.ActiveCell != null)
            {
                GridHandler.ActiveCell.DragEnable(false);
            }

            GridHandler.ActiveCell = SelfCellHandler;

            GridHandler.ActiveCell.GridRef.CellToolsPanel.Interactible();

            SelfCellHandler.DragEnable(true);
        }

        try
        {
            if (SelfCellHandler.X != data.pointerDrag.GetComponent<CellHandler>().X || SelfCellHandler.Y != data.pointerDrag.GetComponent<CellHandler>().Y)
            {
                SelfCellHandler.DragEnable(true);
                data.pointerDrag.GetComponent<DragMe>().DestroyIcon();
                data.pointerDrag.GetComponent<CellHandler>().Delete();

                GridHandler.ActiveCell = SelfCellHandler;
            }
        }
        catch (System.Exception)
        {
            //Debug.LogError("EXCEPTION");
        }
    }

    private void DeleteDuplicateElement(PointerEventData data, CellType ct)
    {
        if (GetDropId(data) == ct)
        {
            CellHandler dragHandler = data.pointerDrag.GetComponent<CellHandler>();
            foreach (var item in GridHandler.GetCellDictionary())
            {
                if (dragHandler != item.Value && item.Value.CellType == ct)
                {
                    item.Value.Delete();
                }
            }
        }
    }

    private Sprite GetDropSprite (PointerEventData data)
	{
		var originalObj = data.pointerDrag;
		if (originalObj == null)
			return null;
		
		var dragMe = originalObj.GetComponent<DragMe> ();
		if (dragMe == null)
			return null;
		
		var srcImage = originalObj.GetComponent<Image> ();
		if (srcImage.overrideSprite != null)
			return srcImage.overrideSprite;
		

		return srcImage.sprite;
	}

	private CellType GetDropId (PointerEventData data)
	{
		var originalObj = data.pointerDrag;
		if (originalObj.GetComponent<ElementHandler> () != null) 
		{
			return originalObj.GetComponent<ElementHandler> ().ElementType;
		} 
		else 
		{
			return originalObj.GetComponent<CellHandler> ().CellType;
		}
	}


	public void OnEnable ()
	{
		// do semething
	}

	public void OnPointerEnter (PointerEventData data)
	{
		_previousCellImage = receivingImage.overrideSprite;

		if (data.pointerDrag == null)
			return;

		if (data.pointerDrag.name != "Grid") 
			receivingImage.overrideSprite = GetDropSprite(data);
	}

	public void OnPointerExit (PointerEventData data)
	{
		receivingImage.overrideSprite = _previousCellImage;
	}
}