﻿using UnityEngine;
using UnityEngine.UI;

public class ElementHandler : MonoBehaviour 
{
	public Image SelfImage;

	[SerializeField]
	private CellType _elementType;

	[SerializeField]
	private DragMe _dragMe;

	[SerializeField]
	private ElementGroup _elementGroup;

	void Start()
	{
		Initialization ();
	}
	
	void Initialization ()
	{
		_dragMe = gameObject.AddComponent<DragMe> ();
	}

	public void SetSelfImage(Sprite s)
	{
		SelfImage.sprite = s;
	}

    public CellType ElementType
    {
        get
        {
            return _elementType;
        }
        set
        {
            _elementType = value;
        }
    }

	public ElementGroup ElementGroup
	{
        get
        {
            return _elementGroup;
        }
        set
        {
            _elementGroup = value;
        }
	}
}