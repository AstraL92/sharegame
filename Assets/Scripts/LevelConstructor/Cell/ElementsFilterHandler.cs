﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElementsFilterHandler : MonoBehaviour
{
    public GameObject ElementsParent;

    private List<ElementHandler> _elementsList = new List<ElementHandler>();

    void Start()
    {
        _elementsList.AddRange(ElementsParent.GetComponentsInChildren<ElementHandler>());
        Filter((int)ElementGroup.Block);
    }

    public void Filter(int eg)
    {
        foreach (var item in _elementsList)
        {
            if ((int)item.ElementGroup != eg)
            {
                item.gameObject.SetActive(false);
            }
            else
            {
                item.gameObject.SetActive(true);
            }
        }
    }
}

[Serializable]
public enum ElementGroup
{
    Block = 0,
    Platform = 1,
    Trap = 2,
    Mechanism = 3,
    Item = 4,
    Monster = 5,
	Main = 6
}
