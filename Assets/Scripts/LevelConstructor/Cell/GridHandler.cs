﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class GridHandler : MonoBehaviour
{
	private static CellHandler _activeCell;
	public static CellHandler ActiveCell
	{
		set
		{
			if (_activeCell != null) 
			{
				_activeCell.GetComponent<CellLongTap> ().GreenCellBorder.SetActive (false);
				_activeCell.DragEnable (false);
			}

			_activeCell = value;

			if (_activeCell == null)
				return;
			
			_activeCell.GridRef.DeselectAllCells ();
			_activeCell.DragEnable (true);
			_activeCell.GetComponent<CellLongTap> ().GreenCellBorder.SetActive (true);
		}

		get
		{
			return _activeCell;
		}
	}

    public static List<CellType> LevelStructureList;

    [Serializable]
    private struct CellImageType
    {
        public CellType type;
        public Sprite sprite;
    }

    [SerializeField]
    private List<CellImageType> cellImage;

	public bool IsDoorAvalible;
	public bool IsPlayerAvalible;

	public GameObject CheckPanel;

    public GameObject EmptyCell;
    public GameObject CellParent;

	public static Dictionary<Vector2, CellHandler> cellDictionary;
    public Dictionary<Vector2, CellHandler> cellGroupSame;
    public Dictionary<CellType, Sprite> cellImageFromType;

    public GridLayoutGroup GridLayout;

    public CellTools CellToolsPanel;
    public RecentElementHandler RecentElements;

	[Header("Grid size")]
	public static int RowCount;
	public static int ColumnCount;

    public Action<bool> EnableRayCast;
    public Action ClearSameDictionary;

    void Start()
    {
        Init();

		SubscribeEvents ();
		
        ConfigureGridLayout();

        InstantiateCells(RowCount, ColumnCount);
		Debug.Log ("Is inap : " + LevelConstriuctorEnterPoint.IsSnapshot);

		if (LevelConstriuctorEnterPoint.IsSnapshot) 
		{
			foreach (var item in GridHandler.cellDictionary) 
			{
				Debug.Log ("name : " + item.Value.name);
				item.Value.DragEnable (false);
				item.Value.DropEnable (false);
				item.Value.LongTapEnable (false);
				item.Value.DoubleTapEnable (false);
			}
			LevelConstriuctorEnterPoint.IsSnapshot = false;
		}
    }

    void OnDisable()
    {
        UnSubscribeEvents();
    }

    void Init()
    {
        cellImageFromType = new Dictionary<CellType, Sprite>();

        foreach (var item in cellImage)
        {
            cellImageFromType.Add(item.type, item.sprite);
        }

        cellDictionary = new Dictionary<Vector2, CellHandler>();
        cellGroupSame = new Dictionary<Vector2, CellHandler>();
        if (LevelStructureList == null)
        {
            LevelStructureList = new List<CellType>();
            LevelStructureList.Clear();
        }

        IsDoorAvalible = false;
        IsPlayerAvalible = false;

        ClearSameDictionary += ClearDictionary;
    }

    private void ClearDictionary()
    {
        foreach (var item in cellGroupSame)
        {
            item.Value.borderHandler.OnHideBorder.Invoke();
        }

        cellGroupSame.Clear();
    }

    public static Dictionary<Vector2, CellHandler> GetCellDictionary()
    {
        return cellDictionary;
    }

    void SubscribeEvents ()
	{
        CellLongTap.LongTap += CellLongTap_LongTap;

        DoubleTapMe.DoubleTap += CellDoublTap_DoubleTap;
	}

    void UnSubscribeEvents()
    {
        CellLongTap.LongTap -= CellLongTap_LongTap;
        ClearSameDictionary -= ClearDictionary;
        DoubleTapMe.DoubleTap -= CellDoublTap_DoubleTap;
    }

    private void CellDoublTap_DoubleTap()
    {
        if (cellDictionary.Count > 0)
        {
            ClearDictionary();
        }

        ActiveCell.CheckNeighbors.Invoke (false);

        CellToolsPanel.Show();

        foreach (var item in cellGroupSame)
        {
            item.Value.borderHandler.OnShowBorder.Invoke ();
        }

		if (cellGroupSame.Count > 1) 
		{
			ActiveCell = null;
		}
    }

    void CellLongTap_LongTap ()
	{
        CellToolsPanel.Show();
	}

    private void ConfigureGridLayout()
    {
        GridLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        GridLayout.constraintCount = RowCount;
    }

    private void InstantiateCells(int rowCount, int columnCount)
    {
        for (int y = 0; y < columnCount; y++)
        {
            for (int x = 0; x < rowCount; x++)
            {
                InstantiateEmptyCells(x,y);
            }
        }
    }

    int countCell = 0;
    private void InstantiateEmptyCells(int x, int y)
    {
		// instantiate
        GameObject _currentGameObject = Instantiate(EmptyCell) as GameObject;
        _currentGameObject.transform.SetParent(CellParent.transform);
        _currentGameObject.GetComponent<RectTransform>().localScale = Vector3.one;

		// set cell data
		CellHandler _currentCellHandler = _currentGameObject.GetComponent<CellHandler> ();
        if (LevelStructureList.Count > 0)
        { 
            _currentCellHandler.CellType = LevelStructureList[countCell];
            if (LevelStructureList[countCell] != CellType.Empty)
            {
                _currentCellHandler.SetOverradeSprite(cellImageFromType[LevelStructureList[countCell]]);
            }
        }
        else
        {
            _currentCellHandler.CellType = CellType.Empty;
        }
        _currentCellHandler.SetCellArrayPos(x, y);

        _currentCellHandler.GridRef = this;

        EnableRayCast += _currentCellHandler.EnableRay;

        cellDictionary.Add(new Vector2(x, y), _currentCellHandler);

        countCell++;
    }

	public void SwitchOnCellToolsPanel()
	{
        CellToolsPanel.Show();
    }

	public void SetGridColumnCount(int x)
	{
		ColumnCount = x;
	}

	public void SetGridRowCount(int y)
	{
		RowCount = y;
	}

	public void AddAllCellToStaticList()
	{
		IsPlayerAvalible = CheckCellForType(CellType.Player);
		IsDoorAvalible = CheckCellForType(CellType.Door);

		if (!IsDoorAvalible || !IsPlayerAvalible)
        {
			CheckPanel.SetActive (true);
		}

        LevelStructureList.Clear();

        foreach (var cell in cellDictionary)
        {
            LevelStructureList.Add(cell.Value.CellType);
        }

        LevelConstriuctorEnterPoint.CurrentLevel.SetConfig(new LevelConfig(ColumnCount, RowCount, LevelStructureList));
    }

    public bool CheckCellForType(CellType id)
    {
        foreach (var item in cellDictionary)
        {
            if (item.Value.CellType == id)
            {
                return true;
            }
        }

        return false;
    }

	public void DeselectAllCells()
	{
		foreach (var item in cellDictionary) 
		{
			item.Value.Deselect ();
		}
	}

    public void DellCellForPos(int x, int y)
    {
        foreach (var item in cellDictionary)
        {
            if (item.Value.X == x && item.Value.Y == y)
            {
                item.Value.DragEnable(false);
            }
        }
    }

    public void TestRun()
	{
		AddAllCellToStaticList ();

        if (IsDoorAvalible && IsPlayerAvalible)
        {
            WinPopUp.IsLevelFronFeed = false;

            SceneManager.LoadScene("Game");

        }
    }
}

[Serializable]
public enum CellType
{
    //block
    DefaultBlock,
    SandBlock,
    PulloutBlock,
    IceBlock,
    SlimeBlock,

    //trap
    ShakesObstacle,
    Thorns,
    PulloutThrons,
    Fire,
    Cannon,
    FireCannon,

    //item
    MovableBlock,
    TNT_Block,

    //Mechanism
    Jumper,
    Button,
    Door,
    Title,

    //monsters
    Glitch,
    Trojanium,
    Coddy,
    Bug,
    CheatAchunga,

    //
    Player,
    Empty,

    //platforms
    Platform,
    MovablePlatformX,
    MovablePlatformY,

    //duplicate
    PlatformIce,
    PlatformFire,
    DefaultBlock2,
    DefaultBlock3,
    DefaultBlock4,
    DefaultBlock5,
    DefaultBlock6,
    DefaultBlock7,
    DefaultBlock8,
    DefaultBlock9,

}

[Serializable]
public enum NeignborsEnum
{
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}

[Serializable]
public enum BtnCellTools
{
    MOVE,
    DUPLICATE,
    COPY,
    DELETE,
    CANCEL
}