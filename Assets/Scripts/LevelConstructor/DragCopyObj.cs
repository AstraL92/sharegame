﻿using UnityEngine;
using System.Collections;

public class DragCopyObj : MonoBehaviour
{

    public Transform canvas;

    private RectTransform transformRect;

	void Start ()
    {
        transformRect = GetComponent<RectTransform>();
    }
	
    public void Drag()
    {
        transformRect.anchoredPosition = Input.mousePosition;
    }

    public void Down()
    {
        transform.SetParent(canvas);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
