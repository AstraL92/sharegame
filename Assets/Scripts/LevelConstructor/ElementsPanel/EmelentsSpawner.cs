﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class EmelentsSpawner : MonoBehaviour
{
    [Header("Objects")]
    public GameObject ElementPrefabGameObj;
    public GameObject ElementsParentGameObj;

    [Header("Values")]
    public int ElementsCount;

    [Serializable]
    public struct Elements
    {
        public CellType type;
        public Sprite sprite;
    }

    public List<Elements> elementsImg;

    void Start()
    {
        SpawnElemnts();
    }

    private void SpawnElemnts()
    {
        for (int i = 0; i < ElementsCount; i++)
        {
            GameObject go = Instantiate(ElementPrefabGameObj);
            go.GetComponent<RectTransform>().SetParent(ElementsParentGameObj.GetComponent<RectTransform>());
            go.GetComponent<RectTransform>().localScale = Vector3.one;
            ElementHandler eh = go.GetComponent<ElementHandler>();

            eh.ElementType = elementsImg[i].type;
            eh.SetSelfImage(elementsImg[i].sprite);
        }
    }
}
