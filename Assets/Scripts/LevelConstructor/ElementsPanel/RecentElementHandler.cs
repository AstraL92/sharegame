﻿using UnityEngine;
using System.Collections.Generic;

public class RecentElementHandler : MonoBehaviour
{
    public GameObject ElementsParent;

    private List<ElementHandler> _recentElements = new List<ElementHandler>();

    void Start()
    {
        _recentElements.AddRange(ElementsParent.GetComponentsInChildren<ElementHandler>());

        foreach (var item in _recentElements)
        {
            item.gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
    }

    public void AddRecentElement(CellType type)
    {
        foreach (var item in _recentElements)
        {
            if (item.ElementType == type)
            {
                item.gameObject.SetActive(true);
                item.transform.SetAsFirstSibling();
            }
        }
    }
}
