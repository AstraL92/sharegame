﻿using UnityEngine;

public class FanBehaviour : MonoBehaviour
{
    private float phi;

    private void Update()
    {
        phi = _.fmod(phi + Time.deltaTime*360.0f, 360.0f);
        transform.localEulerAngles = new Vector3(0.0f, 0.0f, phi);
    }
}
