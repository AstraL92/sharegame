﻿using UnityEngine;

public class GlitchDamageHandler : MonoBehaviour
{
    [SerializeField]
    public MovementController MovementController;
 
    public void OnDamage()
    {
        MovementController.SwitchScale();
    }
}
