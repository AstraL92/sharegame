﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class HashTagsFinder : MonoBehaviour
{
    public Text LevelDescription;

    public List<string> HashTags = new List<string>();
    private Regex _regex = new Regex(@"(?<=#)\w+");

    public void FindHashTags()
    {
        HashTags.Clear();

        var matches = _regex.Matches(LevelDescription.text);

        foreach (Match m in matches)
        {
            HashTags.Add(m.Value);
        }

        //LogHashTags(_hashTags);
        //AddHashTagsToLevelObj();
    }

    private void AddHashTagsToLevelObj()
    {
        LevelConstriuctorEnterPoint.CurrentLevel.HashTags = new List<string>();
        LevelConstriuctorEnterPoint.CurrentLevel.HashTags = HashTags;
        HashTags.Clear();
    }

    private void LogHashTags(List<string> hasTags)
    {
        foreach (var item in hasTags)
        {
            Debug.Log("Tag : " + item + "\n");
        }

        hasTags.Clear();
    }
}
