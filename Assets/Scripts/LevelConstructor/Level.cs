﻿using UnityEngine;
using System.Collections.Generic;

public class Level
{
    public int LevelId;
    public string Description;
    public byte Published;
    public string Config;
    public List<string> HashTags;
    public byte[] LevelPreview;
    public byte[] WinImage;
    public byte[] LoseImage;
    public byte[] PlayerAvatar;
    public byte[] Background;
    public List<byte[]> AdditioalImage;

    public void SetConfig(LevelConfig lc)
    {
        Config = JsonUtility.ToJson(lc);
    }

    public string GetConfig()
    {
        return Config;
    }
}

public class LevelConfig
{
    public int ColumnCount;
    public int RowCount;
    public List<CellType> LevelSeed;

    public LevelConfig(int columnCount, int rowCount, List<CellType> levelSeed)
    {
        ColumnCount = columnCount;
        RowCount = rowCount;
        LevelSeed = levelSeed;
    }
}

public class StartLevelViewModel
{
	public int Id;
    public string Config;
    public byte Published;
    public string WinImage;
    public string LoseImage;
    public string AvatarImage;
    public string Background;
    public List<string> AdditionalImages;

    public void SetConfig(LevelConfig lc)
    {
        Config = JsonUtility.ToJson(lc);
    }

    public string GetConfig()
    {
        return Config;
    }
}
