﻿using UnityEngine;
using UnityEngine.UI;

public class LevelConstriuctorEnterPoint : MonoBehaviour
{
    public static bool IsSnapshot = false;
    public static bool IsEditLevel = false;
    public static Level CurrentLevel;
    public static StartLevelViewModel CurrentLevelFromFeed;

    public GameObject StartPopUp;
    public GameObject NavigationsButtons;
    public GameObject SnapshotButtons;
    public GameObject Grid;

	public Image Gackground;

    void Start()
    {
        Screen.orientation = ScreenOrientation.Landscape;

        UiSwitch();
    }

    private void UiSwitch()
    {
        if (GridHandler.LevelStructureList != null && GridHandler.LevelStructureList.Count > 0)
        {
            if (IsSnapshot)
            {
                Grid.SetActive(true);
                StartPopUp.SetActive(false);

                SnapshotButtons.SetActive(true);
                NavigationsButtons.SetActive(false);

				if (LevelImagesLoader._bg != null)
				Gackground.sprite = LevelImagesLoader._bg;
            }
            else
            {
                Grid.SetActive(true);
                StartPopUp.SetActive(false);

				if (LevelImagesLoader._bg != null)
				Gackground.sprite = LevelImagesLoader._bg;
            }
        }
        else
        {
            CurrentLevel = new Level();
        }
    }
}
