﻿using System;
using UnityEngine;

namespace platformer.levelObjects
{
    public class BlockTNT : MonoBehaviour, IExplosion
    {
        public TNTDetonator Detonator;

        [SerializeField]
        private float _magnitude2d;

        private Rigidbody2D _rb2d;


        void Start()
        {
            _rb2d = GetComponent<Rigidbody2D>();
        }

        void FixedUpdate()
        {
            _magnitude2d = (float)Math.Truncate(Mathf.Max(Mathf.Abs(-_rb2d.velocity.y)));
        }

        public void OnExplosion()
        {
            Detonator.Detonation();
        }

        public int Magnitude
        {
            get
            {
                return (int)_magnitude2d;
            }
        }
    }
}
