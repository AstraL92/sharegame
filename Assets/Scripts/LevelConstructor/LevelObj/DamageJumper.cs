﻿using UnityEngine;
using System.Collections;
using platformer.common.api;
using platformer.utils;

public class DamageJumper : MonoBehaviour
{
    [SerializeField]
    private Vector2 _jumpForce = new Vector2(0, 10);

    void OnTriggerEnter2D(Collider2D other)
    {
        IVelocityImpact affectedObject = other.GetComponent<IVelocityImpact>();

        if (affectedObject != null)
        {
            affectedObject.ImpactVelocity(_jumpForce.ConvertToVector3());
        }
    }
}
