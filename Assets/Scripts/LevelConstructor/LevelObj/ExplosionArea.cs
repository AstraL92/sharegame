﻿using UnityEngine;
using System.Collections;
using System;

public class ExplosionArea : MonoBehaviour
{
    public Action OnExplosion;

    void OnTriggerEnter2D(Collider2D other)
    {

        IExplosion affectedObject = other.GetComponent<IExplosion>();

        if (affectedObject != null)
        {
            affectedObject.OnExplosion();
        }
    }
}

public interface IExplosion
{
    void OnExplosion();
}
