﻿using platformer.common.api;
using platformer.levelObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    [SerializeField]
    private int _damage = 1;
    [SerializeField]
    private Collider2D _collider;
    [SerializeField]
    private GameObject _visual;
    [SerializeField]
    private GameObject FireAura;
     
    void OnTriggerEnter2D(Collider2D other)
    {
        IDamageReviever affectedObject = other.GetComponent<IDamageReviever>();

        if (affectedObject != null)
        {
            affectedObject.OnDamage(_damage);
        }

        BlockTNT tnt = other.GetComponent<BlockTNT>();
        if (tnt != null)
        {
            tnt.Detonator.Detonation();
        }
    }

    private void Show()
    {
        _visual.SetActive(true);
        _collider.enabled = true;
    }

    private void Hide()
    {
        _visual.SetActive(false);
        _collider.enabled = false;
    }

    void OnEnable()
    {
        StartCoroutine("ActivateFireAura");
    }

    IEnumerator ActivateFireAura()
    {
        yield return new WaitForSeconds(1);
        FireAura.SetActive(true);
        yield return null;
    }
}