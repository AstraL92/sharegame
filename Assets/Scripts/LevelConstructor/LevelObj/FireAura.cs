﻿using UnityEngine;
using System;
using System.Collections;

public class FireAura : MonoBehaviour
{
    public Action OnFire;

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("fire " + other.gameObject.name);

        IBurning affectedObject = other.GetComponent<IBurning>();

        if (affectedObject != null)
        {
            affectedObject.OnFire();
        }
    }

    void OnEnable()
    {
        StartCoroutine("DeactivateFireAura");
    }

    IEnumerator DeactivateFireAura()
    {
        transform.localPosition = Vector3.zero;
        yield return new WaitForSeconds(2);
        gameObject.SetActive(false);
        yield return null;
    }
}

public interface IBurning
{
    void OnFire();
}
