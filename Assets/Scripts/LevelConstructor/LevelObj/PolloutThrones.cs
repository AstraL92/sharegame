﻿using UnityEngine;
using System.Collections;

namespace platformer.levelObjects
{
    public class PolloutThrones : MonoBehaviour
    {
        public GameObject LeftTrigger;
        public GameObject RightTrigger;

        public void ActivateTrigger()
        {
            LeftTrigger.SetActive(true);
            RightTrigger.SetActive(true);
        }

        public void DeActivateTrigger()
        {
            LeftTrigger.SetActive(false);
            RightTrigger.SetActive(false);
        }
    }
}
