﻿using UnityEngine;

public class SandBlock : MonoBehaviour, IExplosion
{
	[SerializeField] private Animator _animator;

	private string ActivateTrigger = "Activate";

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player") 
		{
			_animator.SetTrigger (ActivateTrigger);
		}
	}

    public void OnExplosion()
    {
		_animator.SetTrigger (ActivateTrigger);
    }
}
