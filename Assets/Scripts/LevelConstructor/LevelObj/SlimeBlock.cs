﻿using UnityEngine;
using System.Collections;
using System;

public class SlimeBlock : MonoBehaviour, IExplosion, IBurning
{
    public GameObject Fire;

    public void OnExplosion()
    {
        Fire.SetActive(true);
    }

    public void OnFire()
    {
        Fire.SetActive(true);
    }
}
