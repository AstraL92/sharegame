﻿using UnityEngine;
using platformer.player;

public class SlimeBlockTrigger : MonoBehaviour {

    void OnTriggerStay2D(Collider2D other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player != null)
        {
            player.OnSlimeBlock = true;
        }

        if (other.gameObject.GetComponent<ExplosionArea>())
        {
            other.gameObject.GetComponent<ExplosionArea>().OnExplosion += OnExplosion;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player != null)
        {
            Debug.Log("On slime exit");

            player.OnSlimeBlock = false;
        }

        if (other.gameObject.GetComponent<ExplosionArea>())
        {
            other.gameObject.GetComponent<ExplosionArea>().OnExplosion -= OnExplosion;
        }
    }

    private void OnExplosion()
    {

    }
}
