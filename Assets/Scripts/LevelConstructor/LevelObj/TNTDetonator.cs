﻿using UnityEngine;
using System.Collections;
using platformer.levelObjects;

public class TNTDetonator : MonoBehaviour
{
    public BlockTNT TNT;
    public GameObject ExplosionEffect;
    [SerializeField]
    private BoxCollider2D _explosionArea;

    public ExplosionArea _explosion;

    [SerializeField]
    private int _magnitudeBoundary = 6;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (TNT.Magnitude > _magnitudeBoundary && other.gameObject.GetInstanceID() != TNT.gameObject.GetInstanceID())
        {
            _explosionArea.enabled = true;

            StartCoroutine("ExplosionWait");
        }
    }

    public void Detonation()
    {
        _explosionArea.enabled = true;

        StartCoroutine("ExplosionWait");
    }

    IEnumerator ExplosionWait()
    {
        yield return new WaitForSeconds(.1f);
        GameObject _explosionEffect = Instantiate(ExplosionEffect) as GameObject;
        _explosionEffect.transform.position = this.gameObject.transform.position;
        print("boom");
        Destroy(TNT.gameObject);
    }
}
