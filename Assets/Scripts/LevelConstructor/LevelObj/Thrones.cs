﻿using UnityEngine;
using platformer.common.api;

public class Thrones : MonoBehaviour
{
    public bool IsPollout;

    [SerializeField]
    private GlitchDamageHandler _glitchDamageHandler;

    [SerializeField]
    private int _damage = 1;
    [SerializeField]
    private Collider2D _collider;
    [SerializeField]
    private GameObject _visual;

    void OnTriggerEnter2D(Collider2D other)
    {
        IDamageReviever affectedObject = other.GetComponent<IDamageReviever>();

        if (affectedObject != null)
        {
            affectedObject.OnDamage(_damage);
        }

        if (_glitchDamageHandler != null)
        {
            _glitchDamageHandler.OnDamage();
        }
    }

    private void Show()
    {
        _visual.SetActive(true);
        _collider.enabled = true;
    }

    private void Hide()
    {
        _visual.SetActive(false);
        _collider.enabled = false;
    }

    public void OnExplosion()
    {
        if (!IsPollout)
        {
            Destroy(gameObject);
        }
    }
}