﻿using UnityEngine;
using platformer.common.api;
using platformer.utils;

public class MobKillHandler : MonoBehaviour
{
	public GameObject MobBody;

    [SerializeField]
    private Animator _animator;

	[SerializeField]
	private Vector2 _jumpForce = new Vector2(0, 10);

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") 
		{
			IVelocityImpact affectedObject = col.GetComponent<IVelocityImpact>();

			if (affectedObject != null)
			{
				affectedObject.ImpactVelocity(_jumpForce.ConvertToVector3());
			}

            if (_animator != null)
                _animator.SetTrigger("Die");

            Destroy (MobBody, 0.3f);
		}
	}
}