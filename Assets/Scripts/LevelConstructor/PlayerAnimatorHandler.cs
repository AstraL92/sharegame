﻿using UnityEngine;

public class PlayerAnimatorHandler : MonoBehaviour
{
    [SerializeField] private Animator _playerAnimator;
    [SerializeField] private PlayerSoundHandler _playerSoundHandler;


    private string _runTrigger = "Run";
	private string _stopRunTrigger = "StopRun";
	private string _jumpTrigger = "Jump";
	private string _harmedTrigger = "Harmed";
	private string _idle1Trigger = "Idle1";
	private string _idle2Trigger = "Idle2";
	private string _idle3Trigger = "Idle3";
	private string _stayOnGroundTrigger = "StayOnGround";
	private string _fallTrigger = "Fall";
	private string _doubleJumpTrigger = "DoubleJump";

    public void RunSound()
    {
        _playerSoundHandler.Run();
    }

    public void StopRunSound()
    {
        _playerSoundHandler.StopRun();
    }

    public void MuteSteps()
    {
        _playerSoundHandler.enabled = false;
    }

    public void Run()
	{
        _playerAnimator.SetTrigger (_runTrigger);
	}

	public void StopRun()
	{
        StopRunSound();

        _playerAnimator.SetTrigger (_stopRunTrigger);
    }

	public void Jump()
	{
        _playerSoundHandler.Jump();

        _playerAnimator.SetTrigger (_jumpTrigger);
	}

	public void Fall()
	{
		_playerAnimator.SetTrigger (_fallTrigger);
	}

	public void DoubleJump()
	{
		_playerAnimator.SetTrigger (_doubleJumpTrigger);
	}

	public void Harmed()
	{
		_playerAnimator.SetTrigger (_harmedTrigger);
	}

	public void Idle1()
	{
		_playerAnimator.SetTrigger (_idle1Trigger);
	}

	public void Idle2()
	{
		_playerAnimator.SetTrigger (_idle2Trigger);
	}

	public void Idle3()
	{
		_playerAnimator.SetTrigger (_idle3Trigger);
	}

	public void StayOnGround()
	{
        _playerAnimator.SetTrigger (_stayOnGroundTrigger);
	}
}