﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Assets.Scripts.MenuControllers.Login;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;

public class PublishLevelHandler : MonoBehaviour
{
	public GameObject Spinner;
    public GameObject Quit;

    [Header("Level info")]
    public RawImage Preview;
    public Text Description;
    public HashTagsFinder HashTagFinder;

    [Header("Images")]
    public Image BackgroundImage;
    public Image WinImage;
    public Image LoseImage;

	[Header("PrivateSettings")]
	public Text PrivateSwitchButtonText;

    public RequestSendHandler RequestSendHandler;
    private string _addlevelUrl = "api/Level/addLevel";
    private string _imageUrl = "api/Level/uploadLevelImage?levelId={0}&levelImageType={1}";
    private string _publishUrl = "api/Level/publishLevel?levelId={0}";
    private byte[] _textureBuffer = new byte[0];

    private int _levelId;
    private int _imageCouner = 0;

    void OnEnable()
    {
        //new level push complete
        WebAsync.OnNewLevelCreated += WebAsync_OnNewLevelCreated;
		LevelConstriuctorEnterPoint.CurrentLevel.Published = 1;
    }

    void OnDisable()
    {
        WebAsync.OnNewLevelCreated -= WebAsync_OnNewLevelCreated;
    }

    private void WebAsync_OnNewLevelCreated(object sender, EventArgs e)
    {
        _levelId = Int32.Parse(WebAsync.WebResponseString);
        // start courotine send images
        string url = RequestSendHandler.BaseServerUrl + string.Format(_imageUrl, _levelId, _imageCouner);

        switch ((ImageType)_imageCouner)
        {
            case ImageType.LevelPreview:
                try
                {
                    _imageCouner = (int)ImageType.WinImage;
                    StartCoroutine(LoadImage(url, Preview.texture as Texture2D));
                }
                catch (Exception)
                {
                    WebAsync_OnNewLevelCreated(null, null);
                }
                break;
            case ImageType.WinImage:
                try
                {
                    _imageCouner = (int)ImageType.LoseImage;
                    StartCoroutine(LoadImage(url, LevelImagesLoader._win.texture));
                }
                catch (Exception)
                {
                    WebAsync_OnNewLevelCreated(null, null);
                }
                break;
            case ImageType.LoseImage:
                try
                {
                    _imageCouner = (int)ImageType.Background;
                    StartCoroutine(LoadImage(url, LevelImagesLoader._lose.texture));
                }
                catch (Exception)
                {
                    WebAsync_OnNewLevelCreated(null, null);
                }
                break;
            case ImageType.PlayerAvatar:
                //StartCoroutine(LoadImage(url, Preview.texture as Texture2D));
                break;
            case ImageType.Background:
                try
                {
                    StartCoroutine(LoadImage(url, LevelImagesLoader._bg.texture));
                }
                catch (Exception)
                {

                }

                Quit.SetActive(true);
                SetlevelPublicAttribute(_levelId);
                //_imageCouner = (int)ImageType.WinImage;
                break;
            case ImageType.AdditioalImage:
                //StartCoroutine(LoadImage(url, Preview.texture as Texture2D));
                break;
            default:
                break;
        }
    }

    private void SetlevelPublicAttribute(int levelId)
    {
        Uri uri = new Uri(RequestSendHandler.BaseServerUrl + string.Format(_publishUrl, levelId));
        RequestSendHandler.SendRequest(uri, "", HttpMethod.Post, ContentType.ApplicationJson, LoginController.TokenType + " " + LoginController.UserToken);
    }

    public void Publish()
    {
        SetLevelDescription();
        SetLevelTags();


        
		Spinner.SetActive (true);
        SendPublishRequest();
    }

    private void SetLevelDescription()
    {
        LevelConstriuctorEnterPoint.CurrentLevel.Description = Description.text;
    }

    private void SetLevelTags()
    {
        HashTagFinder.FindHashTags();
        LevelConstriuctorEnterPoint.CurrentLevel.HashTags = HashTagFinder.HashTags;
    }

    private void SendPublishRequest()
    {
        Uri uri = new Uri(RequestSendHandler.BaseServerUrl + _addlevelUrl);

        print("url :" + uri.AbsoluteUri);
        print("object :" + JsonUtility.ToJson(LevelConstriuctorEnterPoint.CurrentLevel));

        //for webasync switch
        RequestSendHandler.RequestTypeInt = 23;

        RequestSendHandler.SendRequest(uri, LevelConstriuctorEnterPoint.CurrentLevel,
            HttpMethod.Post, ContentType.ApplicationJson,
            LoginController.TokenType + " " + LoginController.UserToken);
    }

    private IEnumerator LoadImage(string url, Texture2D tex)
    {
        yield return new WaitForEndOfFrame();

        _textureBuffer = tex.EncodeToJPG(QuickLZ.IMAGE_COMPRESSION_QUALITY);

        byte[] imageCompressed = QuickLZ.compress(_textureBuffer);

        SendFile(url, imageCompressed);
    }

    public string SendFile(string url, byte[] file, NameValueCollection formFields = null)
    {
        string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");

        var request = (HttpWebRequest)WebRequest.Create(url);
        request.Headers.Add("Authorization", LoginController.TokenType + " " + LoginController.UserToken);
        request.ContentType = "multipart/form-data; boundary=" +
                              boundary;
        request.Method = "POST";
        request.KeepAlive = true;
        //stream contains request as you build it
        Stream memStream = new MemoryStream();

        byte[] boundarybytes = Encoding.ASCII.GetBytes("\r\n--" +
                                                       boundary + "\r\n");
        byte[] endBoundaryBytes = Encoding.ASCII.GetBytes("\r\n--" +
                                                          boundary + "--");

        //this is a multipart/form-data template for all non-file fields in your form data
        string formdataTemplate = "\r\n--" + boundary +
                                  "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

        if (formFields != null)
        {
            //utilizing the template, write each field to the request, convert this data to bytes, and store temporarily in memStream
            foreach (string key in formFields.Keys)
            {
                string formitem = string.Format(formdataTemplate, key, formFields[key]);
                byte[] formitembytes = Encoding.UTF8.GetBytes(formitem);
                memStream.Write(formitembytes, 0, formitembytes.Length);
            }
        }
        //this is a multipart/form-data template for all fields containing files in your form data
        string headerTemplate =
            "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
            "Content-Type: application/octet-stream\r\n\r\n";
        //Using the template write all files in your files[] input array to the request
        //"uplTheFile" is the destination file's name
        //        for (int i = 0; i < files.Length; i++)
        //        {
        memStream.Write(boundarybytes, 0, boundarybytes.Length);
        string header = string.Format(headerTemplate, "uplTheFile", "file1");
        byte[] headerbytes = Encoding.UTF8.GetBytes(header);

        memStream.Write(headerbytes, 0, headerbytes.Length);
        memStream.Write(file, 0, file.Length);

        memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
        request.ContentLength = memStream.Length;
        
        //Write the data through to the request
        using (Stream requestStream = request.GetRequestStream())
        {
            memStream.Position = 0;
            var tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
        }
        
        //Capture the response from the server
        using (WebResponse response = request.GetResponse())
        {
            Stream stream2 = response.GetResponseStream();
            var reader2 = new StreamReader(stream2);

            Debug.Log(reader2.ReadToEnd());
            WebAsync_OnNewLevelCreated(null, null);
            return reader2.ReadToEnd();
        }
    }

	public void ChangePrivateParameter()
	{
		if (LevelConstriuctorEnterPoint.CurrentLevel.Published == 0) 
		{
			LevelConstriuctorEnterPoint.CurrentLevel.Published = 1;
			PrivateSwitchButtonText.text = "Public";
		} 
		else 
		{
			LevelConstriuctorEnterPoint.CurrentLevel.Published = 0;
			PrivateSwitchButtonText.text = "Private";
		}
	}

    public enum ImageType
    {
        LevelPreview = 0,
        WinImage = 1,
        LoseImage = 2,
        PlayerAvatar = 3,
        Background = 4,
        AdditioalImage = 5
    }
}
