﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using HedgehogTeam.EasyTouch;

public class Snapshot : MonoBehaviour
{
    public GameObject PublishLevelPopup;
    public Canvas MainCanvas;
    public RectTransform SnapAreaRectTransform;
    public RawImage SnapPreview;

	[Header ("Zoom")]
	public ScrollRect GridScrollRect;
	public GridLayoutGroup Grid;

	private const int MaxSize = 125;
	private const int MinSize = 75;
	private const int Speed = 1;


    [Header("Custom Param")]
    public Rect SnapRect;

    private float _rectX;
    private float _rectY;
    private Texture2D _screenCap;

    private int _snapWidth;
    private int _snapHeight;

	private float _curDist;
	private Vector3 _prevDist;
	private float _touchDelta;

	public void ZoomPlus()
	{
		Grid.cellSize = new Vector2(Grid.cellSize.x + 1, Grid.cellSize.y + 1);
	}

	public void ZoomMinus()
	{
		Grid.cellSize = new Vector2(Grid.cellSize.x - 1, Grid.cellSize.y - 1);
	}

    void Start()
    {
        _snapWidth = (int)(SnapAreaRectTransform.rect.width * MainCanvas.scaleFactor);
        _snapHeight = (int)(SnapAreaRectTransform.rect.height * MainCanvas.scaleFactor);

        _screenCap = new Texture2D(_snapWidth, _snapHeight, TextureFormat.RGB24, false); // 1 
    }

    public void TakeSnapshot()
    {
        _rectX = (Screen.width / 2) - (_snapWidth / 2);
        _rectY = (Screen.height / 2) - (_snapHeight / 2);

        //Debug.LogWarningFormat("X: {0}; Y: {1};", _rectX, _rectY);

        SnapRect = new Rect(_rectX, _rectY, _snapWidth, _snapHeight);
        StartCoroutine("Capture");
    }

    IEnumerator Capture()
    {
        yield return new WaitForEndOfFrame();
        _screenCap.ReadPixels(SnapRect, 0, 0);
        _screenCap.Apply();

        SnapPreview.texture = _screenCap;
        PublishLevelPopup.SetActive(true);
        gameObject.SetActive(false);
    }

	public void EditLevel()
	{
		foreach (var item in GridHandler.cellDictionary) 
		{
			item.Value.DragEnable (true);
			item.Value.DropEnable (true);
			item.Value.LongTapEnable (true);
			item.Value.DoubleTapEnable (true);

		}
	}
}