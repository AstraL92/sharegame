﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestLevelStructureError : MonoBehaviour {

	public GridHandler GH;

	public Text HeaderText;
	public GameObject Player;
	public GameObject Door;

	void OnEnable()
	{
		if (!GH.IsPlayerAvalible) 
		{
			Player.SetActive (true);
			Door.SetActive (false);

			HeaderText.text = "To test level, you got to place player somewhere on level.";
		}

		if (!GH.IsDoorAvalible) 
		{
			Player.SetActive (false);

			Door.SetActive (true);
			HeaderText.text = "To test level, you got to place door somewhere on level.";
		}

		if (!GH.IsPlayerAvalible && !GH.IsDoorAvalible) 
		{
			Player.SetActive (true);
			Door.SetActive (true);
			HeaderText.text = "To test level, you got to place player or door somewhere on level.";
		}
	}
}
