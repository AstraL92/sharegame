﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TmpLoadWinLoseImages : MonoBehaviour
{
	[Header ("Background")]
	public GameObject Background;
    public Sprite DefaultBg;
	
	private int _imageLoadingPhase = 0;

    public static bool CanShowEndPopUp = false;

	void Start ()
	{
        CanShowEndPopUp = false;

		LoadImages();
	}

	void LoadImages ()
	{
        if (WinPopUp.IsLevelFronFeed)
        {
            switch (_imageLoadingPhase)
            {
                case 0:
                    _imageLoadingPhase++;
                    if (LevelConstriuctorEnterPoint.CurrentLevelFromFeed.Background != "")
                    {
                        Debug.Log(RequestSendHandler.BaseServerUrl + LevelConstriuctorEnterPoint.CurrentLevelFromFeed.Background);
                        
                        StartCoroutine(LoadBacgroundImage(LevelConstriuctorEnterPoint.CurrentLevelFromFeed.Background));
                    }
                    else
                    {
                        LoadImages();
                    }
                    break;

                case 1:
                    _imageLoadingPhase++;

                    if (LevelConstriuctorEnterPoint.CurrentLevelFromFeed.WinImage != "")
                    {
                        StartCoroutine(LoadWinImage(LevelConstriuctorEnterPoint.CurrentLevelFromFeed.WinImage));
                    }
                    else
                    {
                        LoadImages();
                        CanShowEndPopUp = true;
                    }
                    break;

                case 2:
                    if (LevelConstriuctorEnterPoint.CurrentLevelFromFeed.LoseImage != "")
                    {
                        StartCoroutine(LoadLoseImage(LevelConstriuctorEnterPoint.CurrentLevelFromFeed.LoseImage));
                    }
                    else
                    {
                        CanShowEndPopUp = true;
                    }
                    break;

                default:
                    break;
            }
        }
        else
        {
            CanShowEndPopUp = true;
            if (LevelImagesLoader._bg != null)
            {
                Background.GetComponent<Image>().sprite = LevelImagesLoader._bg;
            }
            if (LevelImagesLoader._win != null)
            {
                WinPopUp.WinSprite = LevelImagesLoader._win;
            }
            else
            {
                WinPopUp.WinSprite = DefaultBg;
            }

            if (LevelImagesLoader._lose != null)
                WinPopUp.LoseSprite = LevelImagesLoader._lose;
            else
            {
                WinPopUp.LoseSprite = DefaultBg;
            }
        }
	}

	private IEnumerator LoadBacgroundImage (string url)
	{
        string s = string.Format(RequestSendHandler.BaseServerUrl + "api/Level/getLevelImage?path={0}", url);
        Debug.Log("BG : " + s);
        var www = new WWW(s);

        yield return www;
        if (www.isDone)
        {
            var json = www.text;
            JResult ss = JsonUtility.FromJson<JResult>(json);
            byte[] image = Convert.FromBase64String(ss.Bytes);

            Texture2D tex = new Texture2D(1, 1);

            tex.LoadImage(QuickLZ.decompress(image));

            WinPopUp.Background = Sprite.Create(tex,
            new Rect(0, 0, tex.width, tex.height),
            new Vector2(0f, 1f));
            Background.GetComponent<Image>().sprite = WinPopUp.Background;
            LoadImages();
        }
        else
        {
            Debug.Log("req error. " + www.error);
        }
    }

	private IEnumerator LoadWinImage (string url)
	{
        string s = string.Format(RequestSendHandler.BaseServerUrl + "api/Level/getLevelImage?path={0}", url);
        Debug.Log("Win : " + s);
        var www = new WWW(s);

        yield return www;
        if (www.isDone)
        {
            var json = www.text;
            JResult ss = JsonUtility.FromJson<JResult>(json);
            byte[] image = Convert.FromBase64String(ss.Bytes);

            Texture2D tex = new Texture2D(1, 1);

            tex.LoadImage(QuickLZ.decompress(image));

            WinPopUp.WinSprite = Sprite.Create(tex,
            new Rect(0, 0, tex.width, tex.height),
            new Vector2(0f, 1f));
            CanShowEndPopUp = true;
            LoadImages();
        }
        else
        {
            Debug.Log("req error. " + www.error);
        }
	}

	private IEnumerator LoadLoseImage (string url)
	{
        string s = string.Format(RequestSendHandler.BaseServerUrl + "api/Level/getLevelImage?path={0}", url);
        Debug.Log("lose : " + s);
        var www = new WWW(s);

        yield return www;
        if (www.isDone)
        {
            var json = www.text;
            JResult ss = JsonUtility.FromJson<JResult>(json);
            byte[] image = Convert.FromBase64String(ss.Bytes);

            Texture2D tex = new Texture2D(1, 1);

            tex.LoadImage(QuickLZ.decompress(image));

            WinPopUp.LoseSprite = Sprite.Create(tex,
            new Rect(0, 0, tex.width, tex.height),
            new Vector2(0f, 1f));
            CanShowEndPopUp = true;
        }
        else
        {
            Debug.Log("req error. " + www.error);
        }
    }
}
