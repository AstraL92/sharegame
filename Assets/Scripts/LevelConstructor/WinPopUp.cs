﻿using UnityEngine;
using System.Collections;
using platformer.game;
using UnityEngine.UI;
using System;
using Assets.Scripts.MenuControllers.Login;
using platformer.levelEditor;

public class WinPopUp : MonoBehaviour
{
    public static bool IsLevelFronFeed = false;
	public RequestSendHandler RequestSendHandler;

    public GameObject Panel;
    public Image PanelBG;
    public bool IsWinPop;
    public GameController GC;
    public GameObject EndGamePanel;
    public GameObject BGPhoto;

    public bool IsWin;

	public static Sprite Background;
    public static Sprite WinSprite;
    public static Sprite LoseSprite;

	private string _isPassed = "api/Stats/UserPassedLevel?levelId={0}";
    
	void OnDisable()
    {
        Time.timeScale = 1f;
    }

    public void SetTimeScaleToOne()
    {
        Time.timeScale = 1f;
    }

    public void SetPanelActive()
    {
        StopAllCoroutines();
        StartCoroutine(_setPanelActive());
    }

    IEnumerator _setPanelActive()
    {
        while (!TmpLoadWinLoseImages.CanShowEndPopUp) { yield return null; }

        if (IsLevelFronFeed)
        {
            if (IsWin)
            {
                BGPhoto.GetComponent<Image>().sprite = WinSprite;
                BGPhoto.SetActive(true);
                EndGamePanel.SetActive(true);
                //Time.timeScale = 0f;

                if (LevelGrid.PlayedType == 0)
                {
                    Uri uri = new Uri(RequestSendHandler.BaseServerUrl + string.Format(_isPassed, LevelConstriuctorEnterPoint.CurrentLevelFromFeed.Id));
                    RequestSendHandler.SendRequest(uri, "", HttpMethod.Post, ContentType.ApplicationJson, LoginController.TokenType + " " + LoginController.UserToken);
                }
            }
            else
            {
                BGPhoto.GetComponent<Image>().sprite = LoseSprite;
                BGPhoto.SetActive(true);
                EndGamePanel.SetActive(true);
                //Time.timeScale = 0f;
            }
        }
        else
        {
            if (IsWinPop)
                PanelBG.sprite = WinSprite;
            else
                PanelBG.sprite = LoseSprite;

            Panel.SetActive(true);
            //Time.timeScale = 0f;
        }
    }

    public void SetIsLevelFromFeed()
    {
        IsLevelFronFeed = false;
    }

    public void Publish()
    {
        LevelConstriuctorEnterPoint.IsSnapshot = true;
    }
}
