﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelImageCrop : MonoBehaviour
{
    [Header ("Images")]
    public Image ConstructorBackgroundImage;
    public Image BackgroundImage;
    public Image WinImage;
    public Image LoseImage;

    [Header ("Params")]
    public Canvas MainCanvas;
    public RectTransform SnapAreaRectTransform;
    public Rect SnapRect;
    public GameObject SnapRectOj;
    public GameObject BottonOkObject;
    public GameObject BottonCancelObject;

    private int _snapWidth;
    private int _snapHeight;
    private Texture2D _screenCapBG;
    private Texture2D _screenCapWin;
    private Texture2D _screenCapLose;
    private float _rectX;
    private float _rectY;


    private string SOSNOOLEY;

    void nasrat(string kaka)
    {
        //Debug.Log(kaka);
        SOSNOOLEY += kaka + System.Environment.NewLine;
    }

    void OnGUI()
    {
        //GUILayout.Label(SOSNOOLEY);
    }

    void Start()
    {
        _snapWidth = (int)(SnapAreaRectTransform.rect.width * MainCanvas.scaleFactor);
        _snapHeight = (int)(SnapAreaRectTransform.rect.height * MainCanvas.scaleFactor);
        _screenCapBG = new Texture2D(_snapWidth, _snapHeight, TextureFormat.RGB24, false);
        _screenCapWin = new Texture2D(_snapWidth, _snapHeight, TextureFormat.RGB24, false);
        _screenCapLose = new Texture2D(_snapWidth, _snapHeight, TextureFormat.RGB24, false);
    }

    public void CropButtonClick()
    {
        _rectX = (Screen.width / 2) - (_snapWidth / 2);
        _rectY = (Screen.height / 2) - (_snapHeight / 2);

        SnapRect = new Rect(_rectX, _rectY, _snapWidth, _snapHeight);
        StartCoroutine(Crop());
    }

    IEnumerator Crop()
    {
        nasrat("Crop courotine");
        UiObjectrsSetActive(false);

        switch ((ImageType)StansImagePicker.SpriteType)
        {
            case ImageType.Background:
                yield return new WaitForEndOfFrame();
                TakeCropImage(BackgroundImage, true, _screenCapBG);
                nasrat("SET image : " + ((ImageType)StansImagePicker.SpriteType).ToString());
                break;

            case ImageType.Win:
                yield return new WaitForEndOfFrame();
                TakeCropImage(WinImage, false, _screenCapWin);
                nasrat("SET image : " + ((ImageType)StansImagePicker.SpriteType).ToString());
                break;

            case ImageType.Lose:
                yield return new WaitForEndOfFrame();
                TakeCropImage(LoseImage, false, _screenCapLose);
                nasrat("SET image : " + ((ImageType)StansImagePicker.SpriteType).ToString());
                break;

            case ImageType.Avatar:
                break;

            default:
                break;
        }
    }

    private void TakeCropImage(Image image, bool isBG, Texture2D tex)
    {
        tex.ReadPixels(SnapRect, 0, 0);
        tex.Apply();

        UiObjectrsSetActive(true);

        image.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));

        if (isBG)
        {
            nasrat("set constructor bg");
            ConstructorBackgroundImage.sprite = image.sprite;
        }

        gameObject.SetActive(false);
    }

    private void UiObjectrsSetActive(bool b)
    {
        SnapRectOj.SetActive(b);
        BottonCancelObject.SetActive(b);
        BottonOkObject.SetActive(b);
    }
}
