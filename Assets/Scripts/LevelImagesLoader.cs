﻿using UnityEngine;
using UnityEngine.UI;

public class LevelImagesLoader : MonoBehaviour
{
    public Image WinImage;
    public Image LoseImage;
    public Image BackgroundImage;

    public static Sprite _win;
    public static Sprite _lose;
    public static Sprite _bg;

    void Start()
    {
        WinImage.sprite = _win;
        LoseImage.sprite = _lose;
        BackgroundImage.sprite = _bg;
    }

    public void Close()
    {
        _win = WinImage.sprite;
        _lose = LoseImage.sprite;
        _bg = BackgroundImage.sprite;
    }
}