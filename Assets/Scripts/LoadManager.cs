﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class LoadManager : MonoBehaviour
{
    public Config Configuration;

    [Space (10)]
    public Text taskText;
    public Slider proggres;
    public Transform spinner;
    public GameObject parent;

    [Space (10)]
    public List<MyAction> action;

    private float timer;

    void Start ()
    {
        taskText.gameObject.SetActive (Configuration.showTask);
        proggres.gameObject.SetActive (Configuration.showTask);
        spinner.gameObject.SetActive (Configuration.showTask);

        if (!Configuration.showTask)
        {
            action [0].Invoke ();
        }
        else
        {
            StartCoroutine ("TeskDelay");
        }
    }

    IEnumerator TeskDelay ()
    {
        TimeLoad = Configuration.speedGenerate + .5f + .7f + .8f + Configuration.speedGenerate / Configuration.coutCell;

        //StartGenerateSpinner ();
        taskText.text = "task : start";

        yield return new WaitForSeconds (.5f);
        //Init ();
        taskText.text = action [1].nameTask;
        action [1].Invoke ();

        yield return new WaitForSeconds (.7f);
        //StartGenerateElement ();
        taskText.text = action [2].nameTask;
        action [2].Invoke ();

        yield return new WaitForSeconds (.8f);
        //StartGenericGrid ();
        taskText.text = action [3].nameTask;
        action [3].Invoke ();

        yield return new WaitForSeconds (Configuration.speedGenerate);
        taskText.text = "done";
        yield return new WaitForSeconds (1f);

        parent.SetActive (false);
        enabled = false;
        //Done ();
    }

    public float TimeLoad 
    {
        get
        {
            return proggres.maxValue;
        }
        set
        {
            proggres.maxValue = value;
        }
    }

    void FixedUpdate ()
    {
        if (Configuration.showTask)
        {
            timer += Time.deltaTime;

            proggres.value = timer;

            spinner.Rotate (0, 0, -5);
        }
    }
}

[Serializable]
public class Config
{
    [Header ("Config")]
    [Range (0, 60)]
    public float speedGenerate = 10;
    public bool showTask;

    [HideInInspector]
    public int coutCell;
}

[Serializable]
public class MyAction 
{
    public string nameTask;
    public UnityEvent myEvent;

    public void Invoke ()
    {
        myEvent.Invoke ();
    }
}
