﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class AlertController : MonoBehaviour
    {
        public Image UserAvatar;
        public Text UserName;

        private UsersController _usersController;
        private FollowingController _followingController;
        private Text _socialButtonText;
        private string _userId;
        private Image _socialBtnImage;

        private FriendsProfileController _friendsProfileController;
        private string _type;


        public void SetUserData(Image userAvatar, string userName, UsersController usersController, FollowingController followingController, Text socialButtonText, string userId, Image socialBtnImage)
        {
            UserAvatar.sprite = userAvatar.sprite;
            UserName.text = userName;
            _usersController = usersController;
            _followingController = followingController;
            _socialButtonText = socialButtonText;
            _userId = userId;
            _socialBtnImage = socialBtnImage;
            _type = "SetUserData";
        }

        public void SetUserDataFromProfile(FriendsProfileController friendsProfileController, FollowingController followingController, Text socialButtonText, Image socialBtnImage)
        {
            _friendsProfileController = friendsProfileController;
            UserAvatar.sprite = _friendsProfileController.Avatar.sprite;
            UserName.text = _friendsProfileController.UserName.text;
            _followingController = followingController;
            _socialButtonText = socialButtonText;
            _socialBtnImage = socialBtnImage;
            _type = "SetUserDataFromProfile";
        }


        public void YesBtnClick()
        {
            if (_type == "SetUserData")
            {
                switch (_socialButtonText.text)
                {
                    case "Following":
                    {
                        _usersController.FollowingTypeInt = (int) FollowingType.None;
                        _followingController.UnFollowUser(_userId, _socialButtonText, _socialBtnImage,
                            FollowingType.None);
                        break;
                    }

                    case "Friends":
                    {
                        _usersController.FollowingTypeInt = (int) FollowingType.Follower;
                        _followingController.UnFollowUser(_userId, _socialButtonText, _socialBtnImage,
                            FollowingType.Follower);
                        break;
                    }
                }
            }
            else
            {
                switch (_socialButtonText.text)
                {
                    case "Following":
                    {
                        var userController = _friendsProfileController.UsersController;
                        if (userController != null)
                            {
                                userController.FollowingTypeInt = (int)FollowingType.None;
                                userController.SocialButtonText.text = "Follow";
                                userController.SocialBtnImage.sprite = userController.FollowBtn;
                            }

                            _followingController.UnFollowUser(_friendsProfileController.UserId, _socialButtonText, _socialBtnImage,
                                FollowingType.None);
                            break;
                        }

                    case "Friends":
                        {
                            var userController = _friendsProfileController.UsersController;
                            if (userController != null)
                            {
                                userController.FollowingTypeInt = (int)FollowingType.Follower;
                                userController.SocialButtonText.text = "+Friend";
                                userController.SocialBtnImage.sprite = userController.AddFriendBtn;
                            }
                            _followingController.UnFollowUser(_friendsProfileController.UserId, _socialButtonText, _socialBtnImage,
                                FollowingType.Follower);
                            break;
                        }
                }
            }
            
            gameObject.SetActive(false);
        }
    }
}