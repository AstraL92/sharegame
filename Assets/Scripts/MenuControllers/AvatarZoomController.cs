﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class AvatarZoomController : MonoBehaviour
    {
        private const float MaxSize = 1.5f;
        private const float MinSize = 0.5f;
        private const float Speed = 0.05f;
        private Vector3 _curDist;
        private Vector3 _prevDist;
        
        private float _touchDelta;

        private void Update()
        {
            ZoomAvatar();
        }

        private void ZoomAvatar()
        {
            gameObject.transform.parent.parent.GetComponent<ScrollRect>().enabled = Input.touchCount != 2;

            if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved &&
                Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                
                _curDist = Input.GetTouch(0).position - Input.GetTouch(1).position;
                _prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) -
                             (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition));

                _touchDelta = _curDist.magnitude - _prevDist.magnitude;

                gameObject.transform.localScale =
                    new Vector3(
                        Mathf.Clamp(gameObject.transform.localScale.x + Speed*Math.Sign(_touchDelta), MinSize,
                            MaxSize),
                        Mathf.Clamp(gameObject.transform.localScale.y + Speed*Math.Sign(_touchDelta), MinSize,
                            MaxSize), 1);
            }
        }
    }
}