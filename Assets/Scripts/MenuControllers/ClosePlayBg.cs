﻿using UnityEngine;

namespace Assets.Scripts.MenuControllers
{
    public class ClosePlayBg : MonoBehaviour
    {
        public void CloseBg()
        {
            var playBgArray = GameObject.FindGameObjectsWithTag("PlayBg");
            for (int i = 0; i < playBgArray.Length; i++)
            {
                playBgArray[i].SetActive(false);
            }
        }
    }
}