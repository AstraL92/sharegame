﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers.CommentsController
{
    [Serializable]
    public class ContentController : MonoBehaviour
    {
        private readonly List<string> _authorId = new List<string>();
        private readonly List<Image> _avatarList = new List<Image>();
        private readonly List<LevelPostController> _levelPostControllerList = new List<LevelPostController>();
        private readonly List<string> _urlAvatarsList = new List<string>();
        private readonly List<string> _urlLevelsList = new List<string>();
        private readonly List<Image> _levelsList = new List<Image>();

        public FeedType FeedType;

        public GameObject LevelPost;
        public GameObject LevelReportPopUp;
        public GameObject LikedUsers;
        public GameObject NoFollowerPanel;
        public RequestSendHandler RequestSendHandler;

        public Button[] TopBtns;

        private IEnumerator levelLoadingCoroutine;

        public void UpdateLevelsFeed(int pageNumber)
        {
            string requestUrl = "";
            switch (FeedType)
            {
                case FeedType.All:
                {
                    requestUrl = string.Format(NetworkRequests.FeedLevelsRequest, RequestSendHandler.BaseServerUrl);
                    break;
                }

                case FeedType.Top:
                {
                    requestUrl = string.Format(NetworkRequests.NewLevelsRequest, RequestSendHandler.BaseServerUrl);
                    break;
                }
                case FeedType.Liked:
                {
                    requestUrl = string.Format(NetworkRequests.GetLevelsThatUserLikeRequest,
                        RequestSendHandler.BaseServerUrl);
                    break;
                }
            }
            var uri = new Uri(requestUrl);

            var newLevel = new NewLevel {PageNumber = pageNumber, DisplayRecods = 10};

            RequestSendHandler.RequestTypeInt = 4;
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void OnEnable()
        {
            WebAsync.OnNewLevelsTrue += WebAsyncOnOnNewLevelsTrue;
            WebAsync.OnGetUserByIdTrue += WebAsyncOnOnGetUserByIdTrue;
            WebAsync.OnGetUserImageTrue += WebAsyncOnOnGetUserImageTrue;
            WebAsync.OnGetLevelImageTrue += WebAsyncOnOnGetLevelImageTrue;
        }


        private void OnDisable()
        {
            WebAsync.OnNewLevelsTrue -= WebAsyncOnOnNewLevelsTrue;
            WebAsync.OnGetUserByIdTrue -= WebAsyncOnOnGetUserByIdTrue;
            WebAsync.OnGetUserImageTrue -= WebAsyncOnOnGetUserImageTrue;
            WebAsync.OnGetLevelImageTrue -= WebAsyncOnOnGetLevelImageTrue;
        }

        private void WebAsyncOnOnGetUserImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.AvatarType != "UserAvatar")
            {
                string str = WebAsync.WebResponseString;
                //Debug.Log("_urlAvatarsList.Count : " + _urlAvatarsList.Count);
                if (_urlAvatarsList.Count > 0)
                {
                    var userAvatar = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(userAvatar.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    _avatarList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 1f));
                   

                    if (_urlAvatarsList.Count > 1)
                    {
                        _urlAvatarsList.RemoveAt(0);
                        _avatarList.RemoveAt(0);
                        LoadUserImage();
                    }
                    else
                    {
                        GameObject[] userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");

                        for (int i = 0; i < userProfilesBtns.Length; i++)
                        {
                            userProfilesBtns[i].GetComponent<Button>().enabled = true;
                        }

                        _urlAvatarsList.Clear();
                        _avatarList.Clear();
                        LoadLevelImage();
                    }
                }
            }
        }

        private void WebAsyncOnOnGetLevelImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.LevelType == "FeedLevels")
            {
                string str = WebAsync.WebResponseString;
                if (_urlLevelsList.Count > 0)
                {
                    var levelPreview = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(levelPreview.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    texture.LoadImage(decompressedImage);
                    _levelsList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 1f));

                    _levelPostControllerList[0].Connecting.SetActive(false);
                    //Debug.Log("_urlLevelsList.Count : " + _urlLevelsList.Count);
                    if (_urlLevelsList.Count > 1)
                    {
                        _urlLevelsList.RemoveAt(0);
                        _levelsList.RemoveAt(0);
                        _levelPostControllerList.RemoveAt(0);
                        LoadLevelImage();
                    }
                    else
                    {
                        _urlLevelsList.Clear();
                        _levelPostControllerList.Clear();
                        _levelsList.Clear();
                    }    
                }
            }
        }

        private void WebAsyncOnOnGetUserByIdTrue(object sender, EventArgs eventArgs)
        {
            if (GameObject.Find("MenuContextView").GetComponent<LoginController>().GetUserById == "Feed")
            {
                string str = WebAsync.WebResponseString;
                UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);
                gameObject.transform.parent.parent.GetComponent<ScrollRect>().enabled = true;
                for (int i = 0; i < _levelPostControllerList.Count; i++)
                {
                    for (int j = 0; j < userInfo.Length; j++)
                    {
                        if (_levelPostControllerList[i].AuthorId == userInfo[j].Id)
                        {
                            _levelPostControllerList[i].AuthorName.text = userInfo[j].Name;
                            _levelPostControllerList[i].LevelList = userInfo[j].PublishedLevels;
                            _levelPostControllerList[i].FollowingTypeInt = (int) userInfo[j].FollowingType;
                            string url = userInfo[j].AvatarUrl;
                            if (url == "")
                            {
                                _levelPostControllerList[i].AuthorAvatar.gameObject.SetActive(false);
                            }
                            else
                            {
                                _urlAvatarsList.Add(url);
                                _avatarList.Add(_levelPostControllerList[i].AuthorAvatar);    
                            }
                        }
                    }
                }
                if (_urlAvatarsList.Count > 0)
                {
                    LoadUserImage();
                }
                else
                {
                    GameObject[] userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");

                    for (int i = 0; i < userProfilesBtns.Length; i++)
                    {
                        userProfilesBtns[i].GetComponent<Button>().enabled = true;
                    }

                    _urlAvatarsList.Clear();
                    _avatarList.Clear();
                    LoadLevelImage();
                }
                
            }
        }

        private void LoadUserImage()
        {
            string requestUrl = string.Format(NetworkRequests.GetUserImageRequest + _urlAvatarsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 26;
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void LoadLevelImage()
        {
            Debug.Log("load level image");
            string requestUrl = string.Format(NetworkRequests.GetLevelImageRequest + _urlLevelsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 27;
            LoginController.LevelType = "FeedLevels";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }


//        private IEnumerator LoadImage()
//        {
//            for (int i = 0; i < _urlList.Count; i++)
//            {
//                var www = new WWW(_urlList[i]);
//                yield return www;
//                _avatarList[i].sprite = Sprite.Create(www.texture,
//                    new Rect(0, 0, www.texture.width, www.texture.height),
//                    new Vector2(0f, 1f));
//            }
//
//            GameObject[] userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");
//
//            for (int i = 0; i < userProfilesBtns.Length; i++)
//            {
//                userProfilesBtns[i].GetComponent<Button>().enabled = true;
//            }
//
//
//            _urlList.Clear();
//            _avatarList.Clear();
//        }

        public IEnumerator EnableTopBtns()
        {
            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < TopBtns.Length; i++)
            {
                TopBtns[i].enabled = true;
            }
        }

        public void DisableTopBtns()
        {
            for (int i = 0; i < TopBtns.Length; i++)
            {
                TopBtns[i].enabled = false;
            }
            StartCoroutine(EnableTopBtns());
        }

        private void WebAsyncOnOnNewLevelsTrue(object sender, EventArgs eventArgs)
        {
            levelLoadingCoroutine = NewLevelTrue();
            StartCoroutine(levelLoadingCoroutine);
        }

        private IEnumerator NewLevelTrue()
        {
            string str = WebAsync.WebResponseString;
            GetFeedLevelObject[] getFeedLevel = JsonHelper.GetJsonArray<GetFeedLevelObject>(str);
            if (FeedType == FeedType.All && getFeedLevel.Length == 0)
            {
                NoFollowerPanel.SetActive(true);
                gameObject.transform.parent.parent.GetComponent<ScrollRect>().enabled = false;
                GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
            }
            else
            {
                gameObject.transform.parent.parent.GetComponent<ScrollRect>().enabled = true;

                for (int i = 0; i < getFeedLevel.Length; i++)
                {
                    var newLevelPost =
                        Instantiate(LevelPost, LevelPost.transform.position, LevelPost.transform.rotation) as GameObject;

                    newLevelPost.transform.SetParent(gameObject.transform);
                    newLevelPost.transform.localScale = Vector2.one;
                    if (i == getFeedLevel.Length - 1 && getFeedLevel.Length == 10)
                    {
                        newLevelPost.transform.FindChild("Accordion")
                            .FindChild("ViewMoreLevelPosts")
                            .gameObject.SetActive(true);
                    }
                    newLevelPost.GetComponent<LevelPostController>()
                        .SetData(getFeedLevel[i].Description,
                            getFeedLevel[i].Stats.AllPlays,
                            getFeedLevel[i].Link, getFeedLevel[i].LikesCount, getFeedLevel[i].AuthorId,
                            getFeedLevel[i].FirstLikers, getFeedLevel[i].LastComments, getFeedLevel[i].Comments,
                            getFeedLevel[i].Id, getFeedLevel[i].IsLiked, getFeedLevel[i].PlayedType);

                    _urlLevelsList.Add(getFeedLevel[i].LevelPreview);
                    _levelsList.Add(newLevelPost.GetComponent<LevelPostController>().LevelPreview);
                    _authorId.Add("\"" + newLevelPost.GetComponent<LevelPostController>().AuthorId + "\"");
                    newLevelPost.GetComponent<LevelPostController>().ContentGameobjectName = "pageFeed";
                    _levelPostControllerList.Add(newLevelPost.GetComponent<LevelPostController>());
                    yield return new WaitForSeconds(0.1f);
                }

                List<string> distinct = _authorId.Distinct().ToList();
                string[] tmpArray = distinct.ToArray();
                RequestUserInfoById(tmpArray);
            }
        }

        private void RequestUserInfoById(string[] id)
        {
            string requestUrl = string.Format(NetworkRequests.UserByIdRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            string str = "[";

            for (int i = 0; i < id.Length; i++)
            {
                if (id.Length == 1 || i == id.Length - 1)
                {
                    str += id[i];
                }
                else
                {
                    str += id[i] + ",";
                }
            }

            str += "]";

            RequestSendHandler.RequestTypeInt = 5;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
            GameObject.Find("MenuContextView").GetComponent<LoginController>().GetUserById = "Feed";
        }

        public void ClearFeedContent()
        {
            if (levelLoadingCoroutine != null)
            {
                StopCoroutine(levelLoadingCoroutine);
            }
            _levelPostControllerList.Clear();
            _authorId.Clear();
            _urlAvatarsList.Clear();
            _avatarList.Clear();
            _urlLevelsList.Clear();
            _levelsList.Clear();
            NoFollowerPanel.SetActive(false);
            for (int i = 1; i < gameObject.transform.childCount; i++)
            {
                Destroy(gameObject.transform.GetChild(i).gameObject);
            }
            GC.Collect();
            Resources.UnloadUnusedAssets();
        }
    }

    public class NewLevel
    {
        public int DisplayRecods;
        public int PageNumber;
    }

    [Serializable]
    public class GetFeedLevelObject
    {
        public string AuthorId;
        public List<long> Comments;
        public int CreationData;
        public string Description;
        public List<string> FirstLikers;
        public List<string> HashTags;
        public long Id;
        public bool IsLiked;
        public List<LevelCommentViewModel> LastComments;
        public string LevelPreview;
        public int LikesCount;
        public string Link;
        public int PlayedType;
        public LevelStats Stats;
    }

    [Serializable]
    public class LevelStats
    {
        public int AllPlays;
        public int Passed;
        public float PassingRate;
        public int UnuquePlays;
    }

    [Serializable]
    public class LevelCommentViewModel
    {
        public int Created;
        public long Id;
        public string Text;
        public string UserId;
        public string UserName;
    }

    [Serializable]
    public class User
    {
        public string[] UserId;
    }

    public enum FeedType
    {
        All,
        Liked,
        Top
    }
}