﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers;
using Assets.Scripts.MenuControllers.Login;
using Assets.Scripts.MenuControllers.SearchController;
using platformer.levelEditor;
using platformer.menu.signals;
using platformer.menu.view;
using platformer.network;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using TheNextFlow.UnityPlugins;

namespace Assets.Scripts.MenuControllers.CommentsController
{
    public class LevelPostController : MonoBehaviour
    {
        private readonly List<GameObject> _commentList = new List<GameObject>();
   
        public Image AuthorAvatar;
        public string AuthorId;
        public Text AuthorName;
        public Text ButtonText;
        public InputField CommentMessage;
        public GameObject[] CommentsArray;

        public List<LevelCommentViewModel> LastCommentsList;
        public Text LevelDescription;
        public long LevelId;
        public Image LevelPreview;
        public Image Like;
        public Text LikedUsers;

        public string Link;
        public GameObject Message;
        public Text PlayCount;
//        public GameObject PostMoreBtnPanel;
        public GameObject ViewMoreCommentsGameObject;

        public GameObject ViewMoreLevelPosts;
        public string ContentGameobjectName = "";
        public int FollowingTypeInt;
        public int PlayedType;
        public GameObject Connecting;

        public Button CyberGui;
    
        private string _author;
        private bool _check;
        private GameObject _commentGameObject;
        private List<long> _commentsIdList;
        private int _counter;

        private int _likesCtn;
        private string _newMessageText;
        private RequestSendHandler _requestSendHandler;
        private readonly List<string> _likedUsersList = new List<string>();
        private int _likesCount;

        private HorizontalScrollSnap _menuSnapScroll;
        private PageButtonView _btnProfile;
        private GameObject _likedUsers;
        private GameObject _postMoreBtnPanel;

        private float _commentFieldHeight = 50;
        private float _commentHeight;
        
        public List<long> LevelList = new List<long>(); 

        private void Start()
        {
            InitializeStart();
        }

        private void InitializeStart()
        {
            _requestSendHandler = GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>();
            _commentGameObject = GameObject.Find("MenuContextView").GetComponent<LoginController>().Comment;
            _author = GameObject.Find("MenuContextView").GetComponent<LoginController>().UserName.text;
            _menuSnapScroll = GameObject.Find("PagesScrollSnap").GetComponent<HorizontalScrollSnap>();
            _btnProfile = GameObject.Find("btnProfile").GetComponent<PageButtonView>();
            _postMoreBtnPanel = GameObject.Find("MenuContextView").GetComponent<LoginController>().PostMoreBtnPanel;

        }

        public void OnPostMoreBtnPanelClick()
        {
            _postMoreBtnPanel.SetActive(true);
            _postMoreBtnPanel.GetComponent<PostMoreBtnPanelController>().Link = Link;
        }
        private void OnEnable()
        {
            WebAsync.OnGetCommentsByIdTrue += WebAsyncOnOnGetCommentsByIdTrue;
            WebAsync.OnAddLikeTrue += WebAsyncOnOnAddLikeTrue;
            WebAsync.OnAddCommentTrue += WebAsyncOnOnAddCommentTrue;
            WebAsync.OnGetStartLevelTrue += WebAsyncOnOnGetStartLevelTrue;
            WebAsync.OnRemoveLikeTrue += WebAsyncOnOnRemoveLikeTrue; 
        }
        

        private void OnDisable()
        {
            WebAsync.OnGetCommentsByIdTrue -= WebAsyncOnOnGetCommentsByIdTrue;
            WebAsync.OnAddLikeTrue -= WebAsyncOnOnAddLikeTrue;
            WebAsync.OnAddCommentTrue -= WebAsyncOnOnAddCommentTrue;
            WebAsync.OnGetStartLevelTrue -= WebAsyncOnOnGetStartLevelTrue;
            WebAsync.OnRemoveLikeTrue -= WebAsyncOnOnRemoveLikeTrue;
        }

        private void WebAsyncOnOnRemoveLikeTrue(object sender, EventArgs eventArgs)
        {
            if (Index.SublingIndex == gameObject.transform.GetSiblingIndex())
            {
                Like.color = Color.white;
            }
        }

        private void WebAsyncOnOnAddLikeTrue(object sender, EventArgs eventArgs)
        {
            if (Index.SublingIndex == gameObject.transform.GetSiblingIndex())
            {
                Like.color = Color.red;
                switch (_likesCtn)
                {
                    case 0:
                    {
                        LikedUsers.text = _author;
                        break;
                    }

                    case 1:
                    {
                        LikedUsers.text += ", " + _author;
                        break;
                    }

                    case 2:
                    {
                        LikedUsers.text += ", " + _author;
                        break;
                    }
                    
                    case 3:
                    {
                        SetLikes(_likedUsersList, _likesCount + 1, true);
                        break;
                    }
                }
            }
        }

        private void WebAsyncOnOnGetStartLevelTrue(object sender, EventArgs eventArgs)
        {
            if (Index.SublingIndex == gameObject.transform.GetSiblingIndex())
            {
                string str = WebAsync.WebResponseString;
                var level = JsonUtility.FromJson<StartLevelViewModel>(str);
                LevelConstriuctorEnterPoint.CurrentLevelFromFeed = level;
                WinPopUp.IsLevelFronFeed = true;
                LevelGrid.PlayedType = PlayedType;

                Screen.orientation = ScreenOrientation.Landscape;

                SceneManager.LoadScene("Game");
            }
        }

        private void WebAsyncOnOnAddCommentTrue(object sender, EventArgs eventArgs)
        {
            int index = Int32.Parse(WebAsync.WebResponseString);
            if (Index.SublingIndex == gameObject.transform.GetSiblingIndex())
            {
                CommentMessage.text = "";
                if (!ViewMoreCommentsGameObject.activeInHierarchy)
                {
                    if (!CommentsArray[0].activeInHierarchy && !CommentsArray[1].activeInHierarchy)
                    {
                        CommentsArray[0].SetActive(true);
                        CommentsArray[0].transform.FindChild("Message").GetComponent<Text>().text = _newMessageText;
                        CommentsArray[0].transform.FindChild("Author").GetComponent<Text>().text = _author;
                        _commentsIdList.Add(index);
                    }
                    else if (CommentsArray[0].activeInHierarchy && !CommentsArray[1].activeInHierarchy)
                    {
                        CommentsArray[1].SetActive(true);
                        CommentsArray[1].transform.FindChild("Message").GetComponent<Text>().text = _newMessageText;
                        CommentsArray[1].transform.FindChild("Author").GetComponent<Text>().text = _author;
                        _commentsIdList.Add(index);
                    }
                    else if (CommentsArray[0].activeInHierarchy && CommentsArray[1].activeInHierarchy)
                    {
                        ViewMoreCommentsGameObject.SetActive(true);
                        _commentsIdList.Add(index);
//                    ButtonText.text = "<b> 1 more comments</b>";
                        ButtonText.text = "<b> " + (_commentsIdList.Count - 2) + " more comments</b>";
                    }
                }
                else
                {
                    if (gameObject.transform.GetChild(0).GetChild(1).name == "Comment(Clone)")
                    {
                        var newComment =
                            Instantiate(_commentGameObject, _commentGameObject.transform.position,
                                _commentGameObject.transform.rotation)
                                as GameObject;
                        newComment.transform.SetParent(gameObject.transform.GetChild(0).transform);
                        newComment.transform.SetSiblingIndex(gameObject.transform.GetChild(0).childCount - 4);
                        newComment.transform.localScale = Vector2.one;
                        newComment.transform.FindChild("Message").GetComponent<Text>().text = _newMessageText;
                        newComment.transform.FindChild("Author").GetComponent<Text>().text = _author;

                        _commentList.Add(newComment);
                        _commentsIdList.Add(index);

                        if (ButtonText.text == "<b> hide comments</b>")
                        {
                            newComment.SetActive(true);
                        }
                        else
                        {
                            newComment.SetActive(false);
                            ButtonText.text = "<b> " + _commentList.Count + " more comments</b>";
                        }
                    }
                    else
                    {
                        _commentsIdList.Add(index);
                        ButtonText.text = "<b> " + (_commentsIdList.Count - 2) + " more comments</b>";
                    }
                }
            }
        }

        private void WebAsyncOnOnGetCommentsByIdTrue(object sender, EventArgs eventArgs)
        {
            if (Index.SublingIndex == gameObject.transform.GetSiblingIndex())
            {
                string str = WebAsync.WebResponseString;
                LevelCommentViewModel[] levelCommentViewModels = JsonHelper.GetJsonArray<LevelCommentViewModel>(str);

                Destroy(CommentsArray[0].gameObject);
                Destroy(CommentsArray[1].gameObject);

                for (int i = 0; i < levelCommentViewModels.Length; i++)
                {
                    var newComment =
                        Instantiate(_commentGameObject, _commentGameObject.transform.position,
                            _commentGameObject.transform.rotation)
                            as GameObject;
                    newComment.transform.SetParent(gameObject.transform.GetChild(0).transform);
                    newComment.transform.SetSiblingIndex(gameObject.transform.GetChild(0).childCount - 4);
                    newComment.transform.localScale = Vector2.one;
                    newComment.transform.FindChild("Message").GetComponent<Text>().text = levelCommentViewModels[i].Text;
                    newComment.transform.FindChild("Author").GetComponent<Text>().text = levelCommentViewModels[i].UserName;
                    newComment.SetActive(false);
                }

                for (int i = 0; i < gameObject.transform.GetChild(0).childCount; i++)
                {
                    if (gameObject.transform.GetChild(0).GetChild(i).name == "Comment(Clone)")
                    {
                        _commentList.Add(gameObject.transform.GetChild(0).GetChild(i).gameObject);
                    }
                }
                _check = true; 
                if (_commentList.Count <= 5)
                {
                    for (int i = 0; i < _commentList.Count; i++)
                    {
                        _commentList[i].SetActive(true);
                        gameObject.GetComponent<LayoutElement>().preferredHeight += 25;
                        _commentHeight += 25;
                    }
                    
                    _commentList.Clear();
                    ButtonText.text = "<b> hide comments</b>";
                    _counter++;
                }
                else
                {
                    ButtonText.text = "<b> " + (_commentList.Count - 5) + " more comments</b>";
                    for (int i = 0; i < 5; i++)
                    {
                        _commentList[i].SetActive(true);
                        gameObject.GetComponent<LayoutElement>().preferredHeight += 25;
                        _commentHeight += 25;
                    }
                    gameObject.GetComponent<LayoutElement>().preferredHeight += 25;
                    _commentHeight += 25;
                    _commentList.Clear();

                    for (int i = 0; i < gameObject.transform.GetChild(0).childCount; i++)
                    {
                        if (gameObject.transform.GetChild(0).GetChild(i).name == "Comment(Clone)" &&
                            !gameObject.transform.GetChild(0).GetChild(i).gameObject.activeInHierarchy)
                        {
                            _commentList.Add(gameObject.transform.GetChild(0).GetChild(i).gameObject);
                        }
                    }
                }
            }
        }

        private void RequestComments(long[] id)
        {
            string requestUrl = string.Format(NetworkRequests.CommentByIdRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 6;

            string str = "[";

            for (int i = 0; i < id.Length; i++)
            {
                if (id.Length == 1 || i == id.Length - 1)
                {
                    str += id[i];
                }
                else
                {
                    str += id[i] + ",";
                }
            }

            str += "]";

            _requestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        public void ViewMoreComments()
        {
            switch (_counter)
            {
                case 0:
                {
                    if (!_check)
                    {
                        Index.SublingIndex = gameObject.transform.GetSiblingIndex();
                        long[] tmpArray = _commentsIdList.ToArray();
                        RequestComments(tmpArray);
                    }
                    else
                    {
                        if (_commentList.Count <= 5)
                        {
                            ButtonText.text = "<b> hide comments</b>";
                            for (int i = 0; i < _commentList.Count; i++)
                            {
                                _commentList[i].SetActive(true);
                                gameObject.GetComponent<LayoutElement>().preferredHeight +=  25;
                                _commentHeight += 25;
                            }
                            gameObject.GetComponent<LayoutElement>().preferredHeight +=  25;
                            _commentHeight += 25;
                            _commentList.Clear();
                            _counter++;
                        }
                        else
                        {
                            for (int i = 0; i < 5; i++)
                            {
                                _commentList[i].SetActive(true);
                                gameObject.GetComponent<LayoutElement>().preferredHeight += 25;
                                _commentHeight += 25;
                            }
                            gameObject.GetComponent<LayoutElement>().preferredHeight += 25;
                            _commentHeight += 25;
//                            gameObject.GetComponent<LayoutElement>().preferredHeight += _commentList.Count*20;
//                            _commentHeight += 100;
                            _commentList.Clear();
                            for (int i = 0; i < gameObject.transform.GetChild(0).childCount; i++)
                            {
                                if (gameObject.transform.GetChild(0).GetChild(i).name == "Comment(Clone)" &&
                                    !gameObject.transform.GetChild(0).GetChild(i).gameObject.activeInHierarchy)
                                {
                                    _commentList.Add(gameObject.transform.GetChild(0).GetChild(i).gameObject);
                                }
                            }

                            ButtonText.text = "<b> " + _commentList.Count + " more comments</b>";
                        }
                    }
                    break;
                }
                case 1:
                {
                    for (int i = 3; i < gameObject.transform.GetChild(0).childCount - 3; i++)
                    {
                        Destroy(gameObject.transform.GetChild(0).GetChild(i).gameObject);
                    }
                    gameObject.transform.GetChild(0).GetChild(1).gameObject.name = "LastComment1";
                    gameObject.transform.GetChild(0).GetChild(2).gameObject.name = "LastComment2";
                    CommentsArray[0] = gameObject.transform.GetChild(0).GetChild(1).gameObject;
                    CommentsArray[1] = gameObject.transform.GetChild(0).GetChild(2).gameObject;

                    ButtonText.text = "<b> " + (_commentsIdList.Count - 2) + " more comments</b>";
                    Debug.Log(_commentHeight);
                    gameObject.GetComponent<LayoutElement>().preferredHeight -= _commentHeight;
                    _commentHeight = 0;
                    _counter--;
                    _check = false;
                    _commentList.Clear();
                    break;
                }
            }
        }

        public void OpenCloseMessages()
        {
            if (!Message.activeInHierarchy)
            {
                gameObject.GetComponent<LayoutElement>().preferredHeight += _commentFieldHeight;    
            }
            else
            {
                gameObject.GetComponent<LayoutElement>().preferredHeight -= _commentFieldHeight;
            }
            Message.SetActive(!Message.activeInHierarchy);
        }

        public void OnLikeBtnClick()
        {
            Index.SublingIndex = gameObject.transform.GetSiblingIndex();
            if (Like.color == Color.red)
            {
                string requestUrl = string.Format(NetworkRequests.RemoveLikeRequest + LevelId,
                    RequestSendHandler.BaseServerUrl);
                var uri = new Uri(requestUrl);

                RequestSendHandler.RequestTypeInt = 12;
                _requestSendHandler.SendRequest(uri, "", HttpMethod.Post, ContentType.ApplicationJson,
                    LoginController.TokenType + " " + LoginController.UserToken);
            }
            else
            {
                string requestUrl = string.Format(NetworkRequests.AddLikeRequest + LevelId, RequestSendHandler.BaseServerUrl);
                var uri = new Uri(requestUrl);

                RequestSendHandler.RequestTypeInt = 9;
                _requestSendHandler.SendRequest(uri, "", HttpMethod.Post, ContentType.ApplicationJson,
                    LoginController.TokenType + " " + LoginController.UserToken);
            }
        }

        public void SetData(string levelDescription, int playCount, string link, int likesCount,
            string authorId, List<string> firstLikers, List<LevelCommentViewModel> lastComments, List<long> comments,
            long levelId, bool isLiked, int playedType)
        {
            
//            string str = levelPreview.Replace("\"", "");
//            string url = RequestSendHandler.BaseServerUrl + str;
//            StartCoroutine(LoadImage(url, LevelPreview));
            LevelDescription.text = levelDescription;
            PlayCount.text = playCount.ToString();
            Link = link;
            AuthorId = authorId;
            _commentsIdList = comments;
            LevelId = levelId;
            PlayedType = playedType;

            if (isLiked)
            {
                Like.color = Color.red;
            }

            if (firstLikers.Count > 0)
            {
                SetLikes(firstLikers, likesCount, false);
            }
            else
            {
                _likesCtn = 0;
                LikedUsers.text = "No likes";
            }

            switch (lastComments.Count)
            {
                case 0:
                {
                    ViewMoreCommentsGameObject.SetActive(false);
                    break;
                }

                case 1:
                {
                    CommentsArray[0].SetActive(true);
                    ViewMoreCommentsGameObject.SetActive(false);
                    CommentsArray[0].transform.FindChild("Message").GetComponent<Text>().text = lastComments[0].Text;
                    CommentsArray[0].transform.FindChild("Author").GetComponent<Text>().text = lastComments[0].UserName;
                    gameObject.GetComponent<LayoutElement>().preferredHeight += 25;
//                    _commentHeight += 20;
                    break;
                }

                case 2:
                {
                    CommentsArray[0].SetActive(true);
                    CommentsArray[1].SetActive(true);

                    CommentsArray[0].transform.FindChild("Message").GetComponent<Text>().text = lastComments[0].Text;
                    CommentsArray[0].transform.FindChild("Author").GetComponent<Text>().text = lastComments[0].UserName;

                    CommentsArray[1].transform.FindChild("Message").GetComponent<Text>().text = lastComments[1].Text;
                    CommentsArray[1].transform.FindChild("Author").GetComponent<Text>().text = lastComments[1].UserName;
                    

                    if (_commentsIdList.Count - 2 > 0)
                    {
                        gameObject.GetComponent<LayoutElement>().preferredHeight += 75;
//                        _commentHeight += 60;
                        ViewMoreCommentsGameObject.transform.FindChild("moreComments").GetComponent<Text>().text = "<b>" +
                                                                                                                   (_commentsIdList
                                                                                                                       .Count -
                                                                                                                    2) +
                                                                                                                   " more comments </b>";
                    }
                    else
                    {
                        gameObject.GetComponent<LayoutElement>().preferredHeight += 50;
//                        _commentHeight += 40;
                        ViewMoreCommentsGameObject.SetActive(false);
                    }

                    break;
                }
            }
        }

        private void SetLikes(List<string> firstLikers, int likesCount, bool isLiked)
        {
            LikedUsers.text = "";
            switch (firstLikers.Count)
            {
                case 1:
                {
                    LikedUsers.text += firstLikers[0];

                    _likedUsersList.Add(firstLikers[0]);
                    _likesCtn = 1;
                    break;
                }

                case 2:
                {
                    LikedUsers.text += firstLikers[0] + ", ";
                    LikedUsers.text += firstLikers[1];

                    _likedUsersList.Add(firstLikers[0]);
                    _likedUsersList.Add(firstLikers[1]);
                    _likesCtn = 2;
                    break;
                }

                case 3:
                {
                    LikedUsers.text += firstLikers[0] + ", ";
                    LikedUsers.text += firstLikers[1] + ", ";
                    LikedUsers.text += firstLikers[2];

                    _likedUsersList.Add(firstLikers[0]);
                    _likedUsersList.Add(firstLikers[1]);
                    _likedUsersList.Add(firstLikers[2]);
                    _likesCtn = 3;
                    break;
                }
            }

            if (!isLiked)
            {
                if (likesCount - _likesCtn > 0)
                {
                    _likesCount = likesCount - _likesCtn;
                    LikedUsers.text += " and " + _likesCount + " others";
                }
            }
            else
            {
                _likesCount++;
                LikedUsers.text += " and " + _likesCount + " others";
            }
            
        }

        private IEnumerator LoadImage(string url, Image avatarSprite)
        {
            var www = new WWW(url);
            yield return www;
            avatarSprite.sprite = Sprite.Create(www.texture,
                new Rect(0, 0, www.texture.width, www.texture.height),
                new Vector2(0f, 1f));
            Connecting.SetActive(false);
        }

        public void ShowLikedUsers()
        {
            if (ContentGameobjectName == "pageFeed")
            {
                _likedUsers = gameObject.transform.parent.GetComponent<ContentController>().LikedUsers;
            }
//            else if (ContentGameobjectName == "pageProfile")
//            {
//                _likedUsers =
//                    gameObject.transform.parent.GetComponent<UserLevelController.ContentController>().LikedUsers;
//            }
//            else if (ContentGameobjectName == "friendsLevels")
//            {
//                _likedUsers =
//                    gameObject.transform.parent.parent.parent.parent.parent.GetComponent<FriendsProfileController>().LikedUsers;
//            }
            else
            {
                _likedUsers =
                       gameObject.transform.parent.GetComponent<UserLevelController.ContentController>().LikedUsers;
                _likedUsers.transform.SetAsLastSibling();
            }
            _likedUsers.SetActive(true);
            _likedUsers.GetComponent<LikedUsersController>().LoadLikedUsers(LevelId, 1);
        }

        public void OnSendCommentBtnClick()
        {
            if (CommentMessage.text != "")
            {
                Index.SublingIndex = gameObject.transform.GetSiblingIndex();
                string requestUrl = string.Format(NetworkRequests.AddCommentRequest, RequestSendHandler.BaseServerUrl);
                var uri = new Uri(requestUrl);
                _newMessageText = CommentMessage.text;

                var comment = new Comment {LevelId = (int) LevelId, Text = CommentMessage.text};

                RequestSendHandler.RequestTypeInt = 10;
                _requestSendHandler.SendRequest(uri, comment, HttpMethod.Post, ContentType.ApplicationJson,
                    LoginController.TokenType + " " + LoginController.UserToken);
                gameObject.GetComponent<LayoutElement>().preferredHeight += 40;
               _commentHeight += 40;
            }
        }

        public void OnPlayGameBtnClick()
        {
            Index.SublingIndex = gameObject.transform.GetSiblingIndex();
            string requestUrl = string.Format(NetworkRequests.GetStartLevelRequest + LevelId,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 11;
            _requestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        public void OnViewMoreLevelPostsBtnClick()
        {
        var pageNumber = (gameObject.transform.parent.childCount + 9) / 10;
            switch (ContentGameobjectName)
            {
                case "pageFeed":
                {
                    gameObject.transform.parent.GetComponent<ContentController>().UpdateLevelsFeed(pageNumber);
                    break;
                }

                case "pageProfile":
                {
                    gameObject.transform.parent.GetComponent<UserLevelController.ContentController>().UpdateUserLevelFeed(pageNumber, 20);
                    break;
                }

                case "searchByTag":
                {
                    gameObject.transform.parent.GetComponent<SearchContentController>().UpdateSearchByTag(pageNumber);
                    break;
                }

                case "friendsLevels":
                {
                    gameObject.transform.parent.parent.parent.parent.parent.GetComponent<FriendsProfileController>().ViewAllFriendLevels();
                    break;
                }
            }
            
            ViewMoreLevelPosts.SetActive(false);
        }

        public void ViewFriendsFrofile()
        {
//            if (ContentGameobjectName == "pageFeed")
//            {
//                _menuSnapScroll.GoToScreen((int) PageType.Profile - 1);

                var friendsProfile = GameObject.Find("MenuContextView").GetComponent<LoginController>().FriendProfile;
                    
                var userProfile =
                    _menuSnapScroll.gameObject.transform.FindChild("Content")
                        .FindChild("pageProfile")
                        .FindChild("PanelProfile");

                var authorName = GameObject.Find("MenuContextView").GetComponent<LoginController>().UserName;
                if (AuthorName.text == authorName.text)
                {
                    userProfile.SetAsLastSibling();
                    _btnProfile.OnBtnClick();
                }
                else
                {
                    friendsProfile.gameObject.SetActive(true);
//                    friendsProfile.transform.SetAsLastSibling();
                    friendsProfile.GetComponent<FriendsProfileController>()
                        .SetFriendData(AuthorAvatar, AuthorName, (FollowingType) FollowingTypeInt, AuthorId);
                }
//            }
        }
    }

    public class Comment
    {
        public int LevelId;
        public string Text;
    }

    public enum LevelPlayedType
    {
        NotPlayed = 0,
        IsPlayed = 1,
        IsPassed = 2
    }
}