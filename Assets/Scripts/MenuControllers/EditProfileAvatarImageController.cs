﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class EditProfileAvatarImageController : MonoBehaviour
    {
        public Canvas MainCanvas;
        public Image NewAvatarImage;
        public RequestSendHandler RequestSendHandler;
        public RectTransform SnapAreaRectTransform;
        public Image UserAvatar;

        [Header("Custom Param")] public Rect SnapRect;
        private int _radius;

        private float _rectX;
        private float _rectY;
        private Texture2D _screenCap;

        private int _snapHeight;
        private int _snapWidth;
        private byte[] _textureBuffer = new byte[0];
        public Image center;


        private void Start()
        {
            _snapWidth = (int) (SnapAreaRectTransform.rect.width*MainCanvas.scaleFactor);
            _snapHeight = (int) (SnapAreaRectTransform.rect.height*MainCanvas.scaleFactor);
            _screenCap = new Texture2D(_snapWidth, _snapHeight, TextureFormat.RGB24, false); // 1 
        }

        public void TakeSnapshot()
        {
            _rectX = (Screen.width/2) - (_snapWidth/2);
            _rectY = (Screen.height/1.8f) - (_snapHeight/2);

            SnapRect = new Rect(_rectX, _rectY, _snapWidth, _snapHeight);
            StartCoroutine(Capture());
        }

        private IEnumerator Capture()
        {
            yield return new WaitForEndOfFrame();
            _screenCap.ReadPixels(SnapRect, 0, 0);
            _screenCap.Apply();
            UserAvatar.gameObject.SetActive(true);
            UserAvatar.sprite = Sprite.Create(_screenCap,
                new Rect(0, 0, _screenCap.width, _screenCap.height),
                new Vector2(0f, 1f));
            LoginController.UserAvatarStatic = _screenCap;

            _textureBuffer = _screenCap.EncodeToJPG(QuickLZ.IMAGE_COMPRESSION_QUALITY);

            byte[] compressedImage = QuickLZ.compress(_textureBuffer);

            //Debug.Log("uploading avatar: " + _textureBuffer.Length + " | " + QuickLZ.compress(_textureBuffer).Length);

            SendFile(string.Format(NetworkRequests.ChangeAvatarRequest, RequestSendHandler.BaseServerUrl), compressedImage);
            //SendFile(string.Format(NetworkRequests.ChangeAvatarRequest, RequestSendHandler.BaseServerUrl),
                //_textureBuffer);
            center.gameObject.SetActive(true);
            NewAvatarImage.transform.position = Vector2.zero;
            NewAvatarImage.sprite = null;
            gameObject.SetActive(false);
        }

        public void OnBtnCancelClick()
        {
            gameObject.SetActive(false);
        }

        public void OnBtnSaveClick()
        {
            ChangeImage();
        }

        public void OnBtnChooseImageClick()
        {
            gameObject.SetActive(false);
        }

        public void LoadNewImage(Texture2D newImage)
        {
            NewAvatarImage.sprite = Sprite.Create(newImage, new Rect(0, 0, newImage.width, newImage.height),
                Vector2.zero);
        }

        private void ChangeImage()
        {
            center.gameObject.SetActive(false);
            TakeSnapshot();
        }

        public string SendFile(string url, byte[] file, NameValueCollection formFields = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");

            var request = (HttpWebRequest) WebRequest.Create(url);
            request.Headers.Add("Authorization", LoginController.TokenType + " " + LoginController.UserToken);
            request.ContentType = "multipart/form-data; boundary=" +
                                  boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            //stream contains request as you build it
            Stream memStream = new MemoryStream();

            byte[] boundarybytes = Encoding.ASCII.GetBytes("\r\n--" +
                                                           boundary + "\r\n");
            byte[] endBoundaryBytes = Encoding.ASCII.GetBytes("\r\n--" +
                                                              boundary + "--");

            //this is a multipart/form-data template for all non-file fields in your form data
            string formdataTemplate = "\r\n--" + boundary +
                                      "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

            if (formFields != null)
            {
                //utilizing the template, write each field to the request, convert this data to bytes, and store temporarily in memStream
                foreach (string key in formFields.Keys)
                {
                    string formitem = string.Format(formdataTemplate, key, formFields[key]);
                    byte[] formitembytes = Encoding.UTF8.GetBytes(formitem);
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }
            }
            //this is a multipart/form-data template for all fields containing files in your form data
            string headerTemplate =
                "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
                "Content-Type: application/octet-stream\r\n\r\n";
            //Using the template write all files in your files[] input array to the request
            //"uplTheFile" is the destination file's name
            //        for (int i = 0; i < files.Length; i++)
            //        {
            memStream.Write(boundarybytes, 0, boundarybytes.Length);
            string header = string.Format(headerTemplate, "uplTheFile", "file1");
            byte[] headerbytes = Encoding.UTF8.GetBytes(header);

            memStream.Write(headerbytes, 0, headerbytes.Length);
            memStream.Write(file, 0, file.Length);
            //Convert files to byte arrays for upload

            //            using (var fileStream = new FileStream(files[i], FileMode.Open, FileAccess.Read))
            //            {
            //                var buffer = new byte[1024];
            //                var bytesRead = 0;
            //                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            //                {
            //                    memStream.Write(buffer, 0, bytesRead);
            //                }
            //            }
            //        }

            memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
            request.ContentLength = memStream.Length;
            //Write the data through to the request
            using (Stream requestStream = request.GetRequestStream())
            {
                memStream.Position = 0;
                var tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            }
            //Capture the response from the server
            using (WebResponse response = request.GetResponse())
            {
                Stream stream2 = response.GetResponseStream();
                var reader2 = new StreamReader(stream2);
                
//                Debug.Log(reader2.ReadToEnd());
                return reader2.ReadToEnd();

            }

            

        }
    }
}