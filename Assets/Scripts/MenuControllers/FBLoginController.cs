﻿using System;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers.Login;
using Facebook.MiniJSON;
using Facebook.Unity;
using platformer.network;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Json = MiniJSON.Json;

namespace Assets.Scripts.MenuControllers
{
    [Serializable]
    public class FBLoginController : MonoBehaviour
    {
        public RequestSendHandler RequestSendHandler;

        private readonly List<string> _friendsIds = new List<string>(); 

        private void OnEnable()
        {
            RequestSendHandler = GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>();
            //Debug.Log("FBLoginController OnEnable");
            WebAsync.OnFaceBookSyncTrue += WebAsyncOnOnFaceBookSyncTrue;
            WebAsync.OnFollowedFbUsersTrue += WebAsyncOnOnFollowedFbUsersTrue;
        }

        private void OnDisable()
        {
            //Debug.Log("FBLoginController OnDisable");
            WebAsync.OnFaceBookSyncTrue -= WebAsyncOnOnFaceBookSyncTrue;
            WebAsync.OnFollowedFbUsersTrue -= WebAsyncOnOnFollowedFbUsersTrue;
        }

        private void WebAsyncOnOnFollowedFbUsersTrue(object sender, EventArgs eventArgs)
        {
            gameObject.SetActive(false);
            SceneManager.LoadScene("Menu");
        }

        private void WebAsyncOnOnFaceBookSyncTrue(object sender, EventArgs eventArgs)
        {
            var str = WebAsync.WebResponseString.Replace("\"", "");
            PlayerPrefs.SetString("DeviceId", str);
            var ppDeviceId = PlayerPrefs.GetString("DeviceId");

            //if (ppDeviceId != SystemInfo.deviceUniqueIdentifier)
            //{
            //    SceneManager.LoadScene("Menu");       
            //}

            GetInvitableFriends();
        }

        public void OnFbLoginBtnClick()
        {
            if (!FB.IsInitialized)
            {
                FB.Init(SetInit, OnHideUnity);
            }
            else
            {
                FBLogin();
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            Time.timeScale = !isGameShown ? 0 : 1;
        }

        private void SetInit()
        {
            Debug.Log("FB Init Done");
            if (FB.IsLoggedIn)
            {
                Debug.Log("FB Logged In");
            }
            else
            {
                FBLogin();
            }
        }

        private void FBLogin()
        {
            var perms = new List<string> { "public_profile", "email", "user_friends" };
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }

        private void AuthCallback(ILoginResult result)
        {
            if (FB.IsLoggedIn)
            {
                FB.API("/me?fields=id", Facebook.Unity.HttpMethod.GET, MyInfo);
//                GetInvitableFriends();
            }
            else
            {
                Debug.Log("User cancelled login");
            }
        }

        public void GetInvitableFriends()
        {
            string queryString = "/me/friends?fields=id";
            FB.API(queryString, Facebook.Unity.HttpMethod.GET, GetInvitableFriendsCallback);
        }

        private void GetInvitableFriendsCallback(IGraphResult result)
        {
            if (result.Error != null)
            {
                Debug.LogError(result.Error);
                return;
            }

            var dict = Json.Deserialize(result.RawResult) as Dictionary<string, object>;
            var datas = (List<object>)dict["data"];
            
            foreach (var iterator in datas)
            {
                var data = iterator as Dictionary<string, object>;
                _friendsIds.Add("\"" + data["id"] + "\"");
            }

            var tmpArray = _friendsIds.ToArray();
            FollowedFbUsers(tmpArray);
        }

        private void FollowedFbUsers(string[] id)
        {
            string requestUrl = string.Format(NetworkRequests.FollowedFbUsersRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            string str = "[";

            for (int i = 0; i < id.Length; i++)
            {
                if (id.Length == 1 || i == id.Length - 1)
                {
                    str += id[i];
                }
                else
                {
                    str += id[i] + ",";
                }
            }

            str += "]";

            RequestSendHandler.RequestTypeInt = 24;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

       private void MyInfo(IGraphResult result)
        {
            if (result.Error != null)
            {
                Debug.Log("Error Response:\n" + result.Error);
            }
            else if (!FB.IsLoggedIn)
            {
                Debug.Log("Login cancelled by Player");
            }
            else
            {
                var myId = result.ResultDictionary["id"].ToString();
                FacebookSync(myId);
            }
        }

        private void FacebookSync(string myId)
        {
            string requestUrl = string.Format(NetworkRequests.FaceBookSyncRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 17;

            var faceboockId = new FaceboockId{FaceBookId = myId};
            RequestSendHandler.SendRequest(uri, faceboockId, HttpMethod.Post, ContentType.ApplicationJson, LoginController.TokenType + " " + LoginController.UserToken);
        }
    }

    [Serializable]
    public class FaceboockId
    {
        public string FaceBookId;
    }
 }