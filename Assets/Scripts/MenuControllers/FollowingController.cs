﻿using System;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class FollowingController : MonoBehaviour
    {
        public int FollowingTypeInt;
        public RequestSendHandler RequestSendHandler;
        public Text SocialButtonText;
        public Sprite AddFriendBtn;
        public Sprite FollowingBtn;
        public Sprite FollowBtn;
        
        public Sprite FriendsBtn;
        public Image SocialBtnImage;
        public GameObject FriendProfile;
        public Sprite AddFriendBtnLong;
        public Sprite FollowingBtnLong;
        public Sprite FollowBtnLong;
        public Sprite FriendsBtnLong;

        private void OnEnable()
        {
            WebAsync.OnFollowUserTrue += WebAsyncOnOnFollowUserTrue;
            WebAsync.OnUnFollowUserTrue += WebAsyncOnOnUnFollowUserTrue;
        }

        private void OnDisable()
        {
            WebAsync.OnFollowUserTrue -= WebAsyncOnOnFollowUserTrue;
            WebAsync.OnUnFollowUserTrue -= WebAsyncOnOnUnFollowUserTrue;
        }

        private void WebAsyncOnOnUnFollowUserTrue(object sender, EventArgs eventArgs)
        {
            ChangeSocialButtonText();
        }

        private void WebAsyncOnOnFollowUserTrue(object sender, EventArgs eventArgs)
        {
            ChangeSocialButtonText();
        }

        private void ChangeSocialButtonText()
        {
            switch ((FollowingType)FollowingTypeInt)
            {
                case FollowingType.Followed:
                {
                    SocialButtonText.text = "Following";
                    SocialBtnImage.sprite = !FriendProfile.activeInHierarchy ? FollowingBtn : FollowingBtnLong;
                    SocialBtnImage.SetNativeSize();
                    break;
                }
                case FollowingType.Follower:
                {
                    SocialButtonText.text = "+Friend";
                    SocialBtnImage.sprite = !FriendProfile.activeInHierarchy ? AddFriendBtn : AddFriendBtnLong;
                    SocialBtnImage.SetNativeSize();
                    break;
                }
                case FollowingType.Friend:
                {
                    SocialButtonText.text = "Friends";
                    SocialBtnImage.sprite = !FriendProfile.activeInHierarchy ? FriendsBtn : FriendsBtnLong;
                    SocialBtnImage.SetNativeSize();
                    break;
                }
                case FollowingType.None:
                {
                    SocialButtonText.text = "Follow";
                    SocialBtnImage.sprite = !FriendProfile.activeInHierarchy ? FollowBtn : FollowBtnLong;
                    SocialBtnImage.SetNativeSize();
                    break;
                }
            }
        }

        public void FollowUser(string userId, Text socialButtonText, Image socialButtonImage, FollowingType followingType)
        {
            SocialButtonText = socialButtonText;
            SocialBtnImage = socialButtonImage;
            FollowingTypeInt = (int)followingType;
            string requestUrl = string.Format(NetworkRequests.FollowUSerRequest,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);
            string str = "\"" + userId + "\"";

            RequestSendHandler.RequestTypeInt = 19;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        public void UnFollowUser(string userId, Text socialButtonText, Image socialButtonImage, FollowingType followingType)
        {
            SocialButtonText = socialButtonText;
            SocialBtnImage = socialButtonImage;
            FollowingTypeInt = (int)followingType;
            string requestUrl = string.Format(NetworkRequests.UnFollowUSerRequest,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);
            string str = "\"" + userId + "\"";

            RequestSendHandler.RequestTypeInt = 20;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }
    }
}