﻿using Assets.Scripts.MenuControllers.Login;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class FriendInfoController : MonoBehaviour
    {
        public FriendsProfileController FriendsProfileController;
        public Image SocialButtonImg;
        public Text SocialButtonText;
        

        public void OnSocialBtnClick()
        {
            var followingController = GameObject.Find("MenuContextView").GetComponent<FollowingController>();
            var alert = GameObject.Find("MenuContextView").GetComponent<LoginController>().Alert;

            switch (SocialButtonText.text)
            {
                case "Following":
                {
                    alert.SetActive(true);
                    alert.GetComponent<AlertController>().SetUserDataFromProfile(FriendsProfileController, followingController, SocialButtonText, SocialButtonImg);
//                    if (FriendsProfileController.UsersController != null)
//                    {
//                        FriendsProfileController.UsersController.FollowingTypeInt = (int)FollowingType.None;
//                        FriendsProfileController.UsersController.SocialButtonText.text = "Follow";    
//                    }
//                    
//                    followingController.UnFollowUser(FriendsProfileController.UserId, SocialButtonText, SocialButtonImg,
//                        FollowingType.None);
                    break;
                }

                case "+Friend":
                {
                    var userController = FriendsProfileController.UsersController;
                    if (userController != null)
                    {
                        userController.FollowingTypeInt = (int)FollowingType.Friend;
                        userController.SocialButtonText.text = "Friends";
                        userController.SocialBtnImage.sprite = userController.FriendsBtn;
                    }
                    followingController.FollowUser(FriendsProfileController.UserId, SocialButtonText, SocialButtonImg,
                        FollowingType.Friend);
                    break;
                }

                case "Friends":
                {
                    alert.SetActive(true);
                    alert.GetComponent<AlertController>().SetUserDataFromProfile(FriendsProfileController, followingController, SocialButtonText, SocialButtonImg);
//                    if (FriendsProfileController.UsersController != null)
//                    {
//                        FriendsProfileController.UsersController.FollowingTypeInt = (int) FollowingType.Follower;
//                        FriendsProfileController.UsersController.SocialButtonText.text = "+Friend";
//                    }
//                    followingController.UnFollowUser(FriendsProfileController.UserId, SocialButtonText, SocialButtonImg,
//                        FollowingType.Follower);
                    break;
                }

                case "Follow":
                {
                    var userController = FriendsProfileController.UsersController;
                    if (userController != null)
                    {
                        userController.FollowingTypeInt = (int)FollowingType.Followed;
                        userController.SocialButtonText.text = "Following";
                        userController.SocialBtnImage.sprite = userController.FollowingBtn;
                    }
                    followingController.FollowUser(FriendsProfileController.UserId, SocialButtonText, SocialButtonImg,
                        FollowingType.Followed);
                    break;
                }
            }
        }
    }
}