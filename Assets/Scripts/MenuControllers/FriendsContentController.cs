﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers.CommentsController;
using Assets.Scripts.MenuControllers.Login;
using Assets.Scripts.MenuControllers.SearchController;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class FriendsContentController : MonoBehaviour
    {
        public GameObject LikedUsersPanel;
        public RequestSendHandler RequestSendHandler;
        public InputField SearchText;
        public SearchContentController SearchContentController;

        private readonly List<Image> _avatarList = new List<Image>();
        private readonly List<string> _urlAvatarsList = new List<string>();

        private void OnEnable()
        {
            WebAsync.OnGetFollowedAndFollowersTrue += WebAsyncOnOnGetFollowedAndFollowersTrue;
            WebAsync.OnGetUserByFirstNameSymbolsTrue += WebAsyncOnOnGetUserByFirstNameSymbolsTrue;
            WebAsync.OnGetUserImageTrue += WebAsyncOnOnGetUserImageTrue;
        }

        private void OnDisable()
        {
            WebAsync.OnGetFollowedAndFollowersTrue -= WebAsyncOnOnGetFollowedAndFollowersTrue;
            WebAsync.OnGetUserByFirstNameSymbolsTrue -= WebAsyncOnOnGetUserByFirstNameSymbolsTrue;
            WebAsync.OnGetUserImageTrue -= WebAsyncOnOnGetUserImageTrue;
        }

        private void WebAsyncOnOnGetUserImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.AvatarType == "SearchAvatar")
            {
                string str = WebAsync.WebResponseString;
                if (_urlAvatarsList.Count > 0)
                {
                    var userAvatar = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(userAvatar.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    _avatarList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 1f));
                    if (_urlAvatarsList.Count > 1)
                    {
                        _urlAvatarsList.RemoveAt(0);
                        _avatarList.RemoveAt(0);
                        LoadUserImage();
                    }
                    else
                    {
                        GameObject[] userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");

                        for (int i = 0; i < userProfilesBtns.Length; i++)
                        {
                            userProfilesBtns[i].GetComponent<Button>().enabled = true;
                        }

                        _urlAvatarsList.Clear();
                        _avatarList.Clear();
                        LoginController.AvatarType = "";
                    }
                }
            }
        }
        private void LoadUserImage()
        {
            string requestUrl = string.Format(NetworkRequests.GetUserImageRequest + _urlAvatarsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 26;
            LoginController.AvatarType = "SearchAvatar";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void WebAsyncOnOnGetUserByFirstNameSymbolsTrue(object sender, EventArgs eventArgs)
        {
            if (SearchContentController.SearchType == "SearchFriends")
            {
                StartCoroutine(NewUserTrue());
            }
        }

        private IEnumerator NewUserTrue()
        {
            string str = WebAsync.WebResponseString;
            UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);
            for (int i = 0; i < userInfo.Length; i++)
            {
                var newUserInfo = Instantiate(LikedUsersPanel, LikedUsersPanel.transform.position, LikedUsersPanel.transform.rotation) as GameObject;
                newUserInfo.transform.SetParent(gameObject.transform);
                newUserInfo.transform.localScale = Vector2.one;
                if (i == userInfo.Length - 1 && userInfo.Length == 10)
                {
                    newUserInfo.transform.FindChild("ViewMoreUsers").gameObject.SetActive(true);
                }
                newUserInfo.GetComponent<UsersController>().SetUserData(userInfo[i].Name, userInfo[i].FollowingType, userInfo[i].Id);

                string url = userInfo[i].AvatarUrl;
                if (url == "")
                {
                    newUserInfo.GetComponent<UsersController>().Avatar.gameObject.SetActive(false);
                }
                else
                {
                    _urlAvatarsList.Add(url);
                    _avatarList.Add(newUserInfo.GetComponent<UsersController>().Avatar);
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (_urlAvatarsList.Count > 0)
            {
                LoadUserImage();
            }
        }

        private void WebAsyncOnOnGetFollowedAndFollowersTrue(object sender, EventArgs eventArgs)
        {
            string str = WebAsync.WebResponseString;

            UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);

            for (int i = 0; i < userInfo.Length; i++)
            {
                var newlikedUsersPanel =
                    Instantiate(LikedUsersPanel, LikedUsersPanel.transform.position,
                        LikedUsersPanel.transform.rotation)
                        as GameObject;
                newlikedUsersPanel.transform.SetParent(gameObject.transform);
                newlikedUsersPanel.transform.localScale = Vector2.one;
                if (i == userInfo.Length - 1 && userInfo.Length == 10)
                {
                    newlikedUsersPanel.transform.FindChild("ViewMoreUsers").gameObject.SetActive(true);
                }
                newlikedUsersPanel.GetComponent<UsersController>()
                    .SetUserData(userInfo[i].Name, userInfo[i].FollowingType, userInfo[i].Id);

                string url = userInfo[i].AvatarUrl;
                if (url == "")
                {
                    newlikedUsersPanel.GetComponent<UsersController>().Avatar.gameObject.SetActive(false);
                }
                else
                {
                    _urlAvatarsList.Add(url);
                    _avatarList.Add(newlikedUsersPanel.GetComponent<UsersController>().Avatar);
                }
            }
            if (userInfo.Length > 0)
            {
                LoadUserImage();
            }
        }

        public void UpdateFriends(int pageNumber)
        {
            string requestUrl = string.Format(NetworkRequests.GetFollowedAndFollowersRequest,
                RequestSendHandler.BaseServerUrl);

            var uri = new Uri(requestUrl);

            var newLevel = new NewLevel {PageNumber = pageNumber, DisplayRecods = 10};

            RequestSendHandler.RequestTypeInt = 22;
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        public void ClearFriends()
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                Destroy(gameObject.transform.GetChild(i).gameObject);
            }
            _urlAvatarsList.Clear();
            _avatarList.Clear();
        }

        public void Search()
        {
            if (SearchText.text != "")
            {
                ClearFriends();
                string searchText = SearchText.text;
                SearchFriends(1, searchText);
            }
        }

        private void SearchFriends(int pageNumber, string link)
        {
            string requestUrl = string.Format(NetworkRequests.GetUserByFirstNameSymbolsRequest + link,
                   RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);
            var newLevel = new NewLevel { PageNumber = pageNumber, DisplayRecods = 10 };
            RequestSendHandler.RequestTypeInt = 16;
            SearchContentController.SearchType = "SearchFriends";
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }
    }


    public enum FollowingType
    {
        /// <summary>
        ///     Без подписок
        /// </summary>
        None = 0,

        /// <summary>
        ///     Ты подписан на него
        /// </summary>
        Followed = 1,

        /// <summary>
        ///     Он подписан на тебя
        /// </summary>
        Follower = 2,

        /// <summary>
        ///     Оба подписаны друг на друга
        /// </summary>
        Friend = 3
    }
}