﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MenuControllers.CommentsController;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class FriendsProfileController : MonoBehaviour
    {
        public Image Avatar;
        public Text UserName;
        public GameObject Content;
        public RequestSendHandler RequestSendHandler;
        public GameObject LevelPost;

        public Text SocialButtonText;
        public string UserId;
        public GameObject PlayerStats;
        public GameObject LikedUsers;
        public int FollowingTypeInt;

        public Text TopUserName;
        public Image SocialButtonImg;
        public Sprite AddFriendBtnLong;
        public Sprite FollowingBtnLong;
        public Sprite FollowBtnLong;
        public Sprite FriendsBtnLong;

        public UsersController UsersController;

        public void SetFriendData(Image avatar, Text userName, FollowingType followingType, string id)
        {
            ClearFrinedProfileInfo();
            FollowingTypeInt = (int)followingType;
            UserName.text = userName.text;
//            BtnText.text = socialBtnText;
//            TopUserName.text = UserName.text;
            if (!avatar.gameObject.activeInHierarchy)
            {
                Avatar.gameObject.SetActive(false);
            }
            else
            {
                Avatar.gameObject.SetActive(true);
            }
            Avatar.sprite = avatar.sprite;
            UserId = id;

            switch (followingType)
            {
                case FollowingType.Followed:
                {
                    SocialButtonText.text = "Following";
                    SocialButtonImg.sprite = FollowingBtnLong;
                    SocialButtonImg.SetNativeSize();
                    break;
                }
                case FollowingType.Follower:
                {
                    SocialButtonText.text = "+Friend";
                    SocialButtonImg.sprite = AddFriendBtnLong;
                    SocialButtonImg.SetNativeSize();
                    break;
                }
                case FollowingType.Friend:
                {
                    SocialButtonText.text = "Friends";
                    SocialButtonImg.sprite = FriendsBtnLong;
                    SocialButtonImg.SetNativeSize();
                    break;
                }
                case FollowingType.None:
                {
                    SocialButtonText.text = "Follow";
                    SocialButtonImg.sprite = FollowBtnLong;
                    SocialButtonImg.SetNativeSize();
                    break;
                }
            }
            
            RequestUserInfoById("\"" + UserId + "\"");
        }

        private void RequestUserInfoById(string id)
        {
            string requestUrl = string.Format(NetworkRequests.GetUserInfoRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 21;
            RequestSendHandler.SendRequest(uri, id, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void GetLevelsById(long[] id)
        {
            string requestUrl = string.Format(NetworkRequests.GetLevelByIdRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            string str = "[";

            for (int i = 0; i < id.Length; i++)
            {
                if (id.Length == 1 || i == id.Length - 1)
                {
                    str += id[i];
                }
                else
                {
                    str += id[i] + ",";
                }
            }

            str += "]";

            RequestSendHandler.RequestTypeInt = 18;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void OnEnable()
        {
            WebAsync.OnGetLevelByIdTrue += WebAsyncOnOnGetLevelByIdTrue;
            WebAsync.OnGetUserInfoTrue += WebAsyncOnOnGetUserInfoTrue;
        }

        private void OnDisable()
        {
            WebAsync.OnGetLevelByIdTrue -= WebAsyncOnOnGetLevelByIdTrue;
            WebAsync.OnGetUserInfoTrue -= WebAsyncOnOnGetUserInfoTrue;
        }
        private void WebAsyncOnOnGetUserInfoTrue(object sender, EventArgs eventArgs)
        {
            var str = WebAsync.WebResponseString;
            var userInfo = JsonUtility.FromJson<UserInfo>(str);
            //old
//            PlayerStats.transform.FindChild("LevelsCreated").GetComponent<Text>().text = "Created " + userInfo.Stats.LevelPublishes + " levels";
//            PlayerStats.transform.FindChild("LevelPlays").GetComponent<Text>().text = "Level plays: " + userInfo.Stats.LevelPlayes;
//            PlayerStats.transform.FindChild("LevelLikes").GetComponent<Text>().text = "Level likes: " + userInfo.Stats.LevelLikes;
//            PlayerStats.transform.FindChild("LevelPassed").GetComponent<Text>().text = "Passed " + userInfo.Stats.PassedLevels + " levels";
//            PlayerStats.transform.FindChild("BestLevelRate").GetComponent<Text>().text = "PassBest level rate: " + userInfo.Stats.BestLevelPasses + "/" + userInfo.Stats.BestLevelPlays;
//            PlayerStats.transform.FindChild("BronzeCups").GetComponent<Text>().text = userInfo.Stats.BronzeCups.ToString();
//            PlayerStats.transform.FindChild("SilverCups").GetComponent<Text>().text = userInfo.Stats.SilverCups.ToString();
//            PlayerStats.transform.FindChild("GoldenCups").GetComponent<Text>().text = userInfo.Stats.GoldenCups.ToString();

            //new
            PlayerStats.transform.FindChild("Created").FindChild("LevelsCreated").GetComponent<Text>().text = userInfo.Stats.LevelPublishes + " levels created";
            PlayerStats.transform.FindChild("Plays").FindChild("LevelPlays").GetComponent<Text>().text = userInfo.Stats.LevelPlayes + " levels plays";
            PlayerStats.transform.FindChild("Likes").FindChild("LevelLikes").GetComponent<Text>().text = userInfo.Stats.LevelLikes + " levels likes";
//            var tmpArray = userInfo.PublishedLevels.ToArray();
//            GetLevelsById(tmpArray);
        }
        

        private void WebAsyncOnOnGetLevelByIdTrue(object sender, EventArgs eventArgs)
        {
            StartCoroutine(NewLevelTrue());
        }

        private IEnumerator NewLevelTrue()
        {
            string str = WebAsync.WebResponseString;
            GetFeedLevelObject[] getFeedLevel = JsonHelper.GetJsonArray<GetFeedLevelObject>(str);
            for (int i = 0; i < getFeedLevel.Length; i++)
            {
                var newLevelPost =
                    Instantiate(LevelPost, LevelPost.transform.position, LevelPost.transform.rotation) as GameObject;

                newLevelPost.transform.SetParent(Content.transform);
                newLevelPost.transform.localScale = Vector2.one;
                newLevelPost.GetComponent<LevelPostController>()
					.SetData(getFeedLevel[i].Description, getFeedLevel[i].Stats.AllPlays,
                        getFeedLevel[i].Link, getFeedLevel[i].LikesCount, getFeedLevel[i].AuthorId,
                        getFeedLevel[i].FirstLikers, getFeedLevel[i].LastComments, getFeedLevel[i].Comments,
                        getFeedLevel[i].Id, getFeedLevel[i].IsLiked, getFeedLevel[i].PlayedType);

                newLevelPost.GetComponent<LevelPostController>().AuthorAvatar.sprite = Avatar.sprite;
                newLevelPost.GetComponent<LevelPostController>().AuthorName.text = UserName.text;
                newLevelPost.GetComponent<LevelPostController>().FollowingTypeInt = (int)FollowingTypeInt;
                newLevelPost.GetComponent<LevelPostController>().ContentGameobjectName = "friendsLevels";
                yield return new WaitForSeconds(0.1f);
            }

            for (int i = 3; i < Content.transform.childCount; i++)
            {
                Content.transform.GetChild(i).gameObject.SetActive(false);
            }
            if (getFeedLevel.Length == 2)
            {
                Content.transform.GetChild(2).FindChild("Accordion").FindChild("ViewMoreLevelPosts").gameObject.SetActive(true);
            }
        }

        public void OnCloseButtonClick()
        {
            ClearFrinedProfileInfo();
            gameObject.SetActive(false);
        }

        private void ClearFrinedProfileInfo()
        {
            Avatar.sprite = null;
            UserName.text = "";
//            TopUserName.text = "";
//            for (int i = 1; i < Content.transform.childCount; i++)
//            {
//                Destroy(Content.transform.GetChild(i).gameObject);
//            }
        }

        public void ViewAllFriendLevels()
        {
            for (int i = 3; i < Content.transform.childCount; i++)
            {
                Content.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }
}