﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class IosMoreBtnPanel : BaseIOSPopup
{
    public string CopyLink;
    public string Flag;
    public string ShareFacebook;
    public string ShareTwitter;
    public event Action<MorePanelEnum> OnComplete = delegate { };

    [DllImport("__Internal")]
    private static extern void _ISN_ShowRateUsPopUp(string flag, string shareFacebook, string shareTwitter, string copyLink);

    public static IosMoreBtnPanel Create(string flag, string shareFacebook, string shareTwitter, string copyLink)
    {
        var popup = new GameObject("IosMoreBtnPanel").AddComponent<IosMoreBtnPanel>();
        popup.Flag = flag;
        popup.ShareFacebook = shareFacebook;
        popup.ShareTwitter = shareTwitter;
        popup.CopyLink = copyLink;

        popup.Init();

        return popup;
    }

    public void Init()
    {
        ShowRateUsPopUp(Flag, ShareFacebook, ShareTwitter, CopyLink);
    }

    private static void ShowRateUsPopUp(string flag, string shareFacebook, string shareTwitter, string copyLink)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _ISN_ShowRateUsPopUp(flag, shareFacebook, shareTwitter, copyLink);
        }
    }

    public void OnPopUpCallBack(string buttonIndex)
    {
        int index = Convert.ToInt16(buttonIndex);
        switch (index)
        {
            case 0:
            {
                OnComplete(MorePanelEnum.Flag);
                break;
            }

            case 1:
            {
                OnComplete(MorePanelEnum.ShareFacebook);
                break;
            }

            case 2:
            {
                OnComplete(MorePanelEnum.ShareTwitter);
                break;
            }

            case 3:
            {
                OnComplete(MorePanelEnum.CopyLink);
                break;
            }
        }
        Destroy(gameObject);
    } 
}

public enum MorePanelEnum
{
    Flag,
    ShareFacebook,
    ShareTwitter,
    CopyLink
}