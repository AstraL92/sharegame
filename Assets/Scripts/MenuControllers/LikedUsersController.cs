﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers;
using Assets.Scripts.MenuControllers.CommentsController;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

//namespace Assets.Scripts.MenuControllers
//{
    [Serializable]
    public class LikedUsersController : MonoBehaviour
    {
        private readonly List<string> _userIdList = new List<string>();
        
        public RequestSendHandler RequestSendHandler;
        public GameObject Content;
        public GameObject LikedUsersPanel;

        private readonly List<Image> _avatarList = new List<Image>();
        private readonly List<string> _urlAvatarsList = new List<string>();

        public void LoadLikedUsers(long levelId, int pageNumber)
        {
            string requestUrl = string.Format(NetworkRequests.GetLevelLikesRequest + levelId,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            var newLevel = new NewLevel { PageNumber = pageNumber, DisplayRecods = 10 };

            RequestSendHandler.RequestTypeInt = 7;
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void OnEnable()
        {
            WebAsync.OnGetLevelLikedTrue += WebAsyncOnOnGetLevelLikedTrue;
            WebAsync.OnGetUsersLikedLevelTrue += WebAsyncOnOnGetUsersLikedLevelTrue;
            WebAsync.OnGetUserImageTrue += WebAsyncOnOnGetUserImageTrue;
        WebAsync.OnLikedLevelError += WebAsync_OnLikedLevelError;
        }

    private void WebAsync_OnLikedLevelError(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void OnDisable()
        {
            WebAsync.OnGetLevelLikedTrue -= WebAsyncOnOnGetLevelLikedTrue;
            WebAsync.OnGetUsersLikedLevelTrue -= WebAsyncOnOnGetUsersLikedLevelTrue;
            WebAsync.OnGetUserImageTrue += WebAsyncOnOnGetUserImageTrue;
        }

        private void WebAsyncOnOnGetUserImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.AvatarType == "LikedUsersAvatar")
            {
                string str = WebAsync.WebResponseString;
                if (_urlAvatarsList.Count > 0)
                {
                    var userAvatar = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(userAvatar.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    _avatarList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 1f));
                    _avatarList[0].sprite.name = _urlAvatarsList.Count.ToString();
                    if (_urlAvatarsList.Count > 1)
                    {
                        _urlAvatarsList.RemoveAt(0);
                        _avatarList.RemoveAt(0);
                        LoadUserImage();
                    }
                    else
                    {
                        GameObject[] userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");

                        for (int i = 0; i < userProfilesBtns.Length; i++)
                        {
                            userProfilesBtns[i].GetComponent<Button>().enabled = true;
                        }

                        _urlAvatarsList.Clear();
                        _avatarList.Clear();
                        LoginController.AvatarType = "";
                    }
                }
            }
        }
        private void LoadUserImage()
        {
            string requestUrl = string.Format(NetworkRequests.GetUserImageRequest + _urlAvatarsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 26;
            LoginController.AvatarType = "LikedUsersAvatar";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void WebAsyncOnOnGetUsersLikedLevelTrue(object sender, EventArgs eventArgs)
        {
            string str = WebAsync.WebResponseString;
            
            UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);

            for (int i = 0; i < userInfo.Length; i++)
            {
                var newlikedUsersPanel =
                       Instantiate(LikedUsersPanel, LikedUsersPanel.transform.position,
                           LikedUsersPanel.transform.rotation)
                           as GameObject;
                newlikedUsersPanel.transform.SetParent(Content.transform);
                newlikedUsersPanel.transform.localScale = Vector2.one;
                if (i == userInfo.Length - 1 && userInfo.Length == 10)
                {
                    newlikedUsersPanel.transform.FindChild("ViewMoreUsers").gameObject.SetActive(true);
                }
                newlikedUsersPanel.GetComponent<UsersController>().SetUserData(userInfo[i].Name, userInfo[i].FollowingType, userInfo[i].Id);
               
                string url = userInfo[i].AvatarUrl;
                if (url == "")
                {
                    newlikedUsersPanel.GetComponent<UsersController>().Avatar.gameObject.SetActive(false);
                }
                else
                {
                    _urlAvatarsList.Add(url);
                    _avatarList.Add(newlikedUsersPanel.GetComponent<UsersController>().Avatar);
                }
            }

            if (_urlAvatarsList.Count > 0)
            {
                LoadUserImage();
            }
        }

        private void WebAsyncOnOnGetLevelLikedTrue(object sender, EventArgs eventArgs)
        {
            string str = WebAsync.WebResponseString;

            LikedUsersId[] likedUsersId = JsonHelper.GetJsonArray<LikedUsersId>(str);
            for (int i = 0; i < likedUsersId.Length; i++)
            {
                _userIdList.Add("\"" + likedUsersId[i].UserId + "\"");
            }
            LoadUserInfo(_userIdList.ToArray());
        }

        private void LoadUserInfo(string[] id)
        {
            string requestUrl = string.Format(NetworkRequests.UserByIdRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            string str = "[";

            for (int i = 0; i < id.Length; i++)
            {
                if (id.Length == 1 || i == id.Length - 1)
                {
                    str += id[i];
                }
                else
                {
                    str += id[i] + ",";
                }
            }

            str += "]";

            RequestSendHandler.RequestTypeInt = 8;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        public void OnBackBtnClick()
        {
            ClearLikedUsers();
            gameObject.SetActive(false);
        }

        private void ClearLikedUsers()
        {
            for (int i = 0; i < Content.transform.childCount; i++)
            {
                Destroy(Content.transform.GetChild(i).gameObject);
            }
            _userIdList.Clear();
            _urlAvatarsList.Clear();
            _avatarList.Clear();
        }
    }
//}

[Serializable]
public class LikedUsersId
{
    public string Id;
    public string UserId;
}