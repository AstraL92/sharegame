﻿using System;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers;
using Assets.Scripts.MenuControllers.CommentsController;
using platformer.network;
using platformer.network.requests;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers.Login
{
    [Serializable]
    public class LoginController : MonoBehaviour
    {
        public static string UserToken;
        public static string TokenType;
        public static Texture UserAvatarStatic;
        public static string UserNameStatic;
        public static string AvatarType;
        public static string LevelType;

        public GameObject Alert;

        public GameObject Comment;
        public GameObject Connecting;
        public GameObject DescriptionBg;
        public GameObject FbLogin;
        public ContentController FeedContentController;

        public GameObject FriendProfile;
        public string GetUserById = "";
        public GameObject NickName;
        public GameObject PlayerStats;
        public GameObject PostMoreBtnPanel;

        public GameObject Reconnect;
        public RequestSendHandler RequestSendHandler;
        public Image UserAvatar;
        public Text UserName;

        private RequestLogin _rLogin;
        private string _url;

        private void Start()
        {
            Screen.orientation = ScreenOrientation.Portrait;
//            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(true);
            LoginTrue();
        }

        private void OnEnable()
        {
            RequestSendHandler = GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>();
            WebAsync.OnLoginTrue += WebAsync_OnLoginTrue;
            WebAsync.OnTokenTrue += WebAsync_OnOnTokenTrue;
            WebAsync.OnGetUserImageTrue += WebAsyncOnOnGetUserImageTrue;
        }

        private void OnLevelWasLoaded(int level)
        {
            Debug.LogError("Scene loaded : " + level);
        }

        private void WebAsync_OnOnTokenTrue(object sender, EventArgs eventArgs)
        {
            string str = WebAsync.WebResponseString.Replace(".", "");
            var token = JsonUtility.FromJson<TokenInfo>(str);
            UserToken = token.access_token;
            TokenType = token.token_type;
            if (_url == "")
            {
                UserAvatar.gameObject.SetActive(false);
                FeedContentController.FeedType = FeedType.All;
                FeedContentController.UpdateLevelsFeed(1);
            }
            else
            {
                LoadImage(_url);
            }
        }

        private void OnDisable()
        {
            WebAsync.OnTokenTrue -= WebAsync_OnOnTokenTrue;
            WebAsync.OnLoginTrue -= WebAsync_OnLoginTrue;
            WebAsync.OnGetUserImageTrue -= WebAsyncOnOnGetUserImageTrue;
        }

        private void WebAsyncOnOnGetUserImageTrue(object sender, EventArgs eventArgs)
        {
            if (AvatarType == "UserAvatar")
            {
                string str = WebAsync.WebResponseString;
                var userAvatar = JsonUtility.FromJson<JResult>(str);
                byte[] image = Convert.FromBase64String(userAvatar.Bytes);
                var texture = new Texture2D(1, 1);
                texture.LoadImage(QuickLZ.decompress(image));
                UserAvatar.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                    new Vector2(0f, 1f));
                UserAvatarStatic = texture;
                AvatarType = "";
                FeedContentController.FeedType = FeedType.All;
                FeedContentController.UpdateLevelsFeed(1);
            }
        }

        public void LoginTrue()
        {
            string requestUrl = string.Format(NetworkRequests.LoginRequest, RequestSendHandler.BaseServerUrl);

            var uri = new Uri(requestUrl);
            string deviceId = SystemInfo.deviceUniqueIdentifier;
            if (PlayerPrefs.HasKey("DeviceId"))
            {
                _rLogin = new RequestLogin {DeviceId = PlayerPrefs.GetString("DeviceId")};
                FbLogin.SetActive(false);
            }
            else
            {
                _rLogin = new RequestLogin {DeviceId = deviceId};
            }

            RequestSendHandler.RequestTypeInt = 0;
            RequestSendHandler.SendRequest(uri, _rLogin, HttpMethod.Post, ContentType.ApplicationJson);
        }

        private void WebAsync_OnLoginTrue(object sender, EventArgs e)
        {
            //Debug.Log(WebAsync.WebResponseString);
            string str = WebAsync.WebResponseString;
            var userInfo = JsonUtility.FromJson<UserInfo>(str);

            UserName.text = userInfo.Name;
            UserNameStatic = UserName.text;

//            _url = RequestSendHandler.BaseServerUrl + userInfo.AvatarUrl;
            _url = userInfo.AvatarUrl;

            //Old
//            PlayerStats.transform.FindChild("LevelsCreated").GetComponent<Text>().text = "Created " + userInfo.Stats.LevelPublishes + " levels";
//            PlayerStats.transform.FindChild("LevelPlays").GetComponent<Text>().text = "Level plays: " + userInfo.Stats.LevelPlayes;
//            PlayerStats.transform.FindChild("LevelLikes").GetComponent<Text>().text = "Level likes: " + userInfo.Stats.LevelLikes;
//            PlayerStats.transform.FindChild("LevelPassed").GetComponent<Text>().text = "Passed " + userInfo.Stats.PassedLevels + " levels";
//            PlayerStats.transform.FindChild("BestLevelRate").GetComponent<Text>().text = "PassBest level rate: " + userInfo.Stats.BestLevelPasses + "/" + userInfo.Stats.BestLevelPlays;
//            PlayerStats.transform.FindChild("BronzeCups").GetComponent<Text>().text = userInfo.Stats.BronzeCups.ToString();
//            PlayerStats.transform.FindChild("SilverCups").GetComponent<Text>().text = userInfo.Stats.SilverCups.ToString();
//            PlayerStats.transform.FindChild("GoldenCups").GetComponent<Text>().text = userInfo.Stats.GoldenCups.ToString();    
            //new
            PlayerStats.transform.FindChild("Created").FindChild("LevelsCreated").GetComponent<Text>().text =
                userInfo.Stats.LevelPublishes + " levels created";
            PlayerStats.transform.FindChild("Plays").FindChild("LevelPlays").GetComponent<Text>().text =
                userInfo.Stats.LevelPlayes + " levels plays";
            PlayerStats.transform.FindChild("Likes").FindChild("LevelLikes").GetComponent<Text>().text =
                userInfo.Stats.LevelLikes + " levels likes";

//            if (_url == "")
//            {
//                UserAvatar.gameObject.SetActive(false);
//            }

            string requestUrl = string.Format(NetworkRequests.TokenRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            string s =
                "grant_type=password&scope=offline_access profile email roles&resource=" + RequestSendHandler.BaseServerUrl + " &username=" +
                SystemInfo.deviceUniqueIdentifier + " &password=" + SystemInfo.deviceUniqueIdentifier;

            RequestSendHandler.RequestTypeInt = 1;
            RequestSendHandler.SendRequest(uri, s, HttpMethod.Post, ContentType.TextPlain);
            if (userInfo.Name == "")
            {
                NickName.SetActive(true);
            }
        }

//        private IEnumerator LoadImage(string url, Image userAvatar)
//        {
//            var www = new WWW(url);
//            yield return www;
//            userAvatar.sprite = Sprite.Create(www.texture,
//                new Rect(0, 0, www.texture.width, www.texture.height),
//                new Vector2(0f, 1f));
//            UserAvatarStatic = www.texture;
//        }

        private void LoadImage(string url)
        {
//            Debug.Log("Image server path: " + url);
            string requestUrl = string.Format(NetworkRequests.GetUserImageRequest + url,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 26;
            AvatarType = "UserAvatar";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                TokenType + " " + UserToken);
        }

        //void OnGUI()
        //{
        //    if (GUI.Button(new Rect(10, 10, 50, 30), "Clear"))
        //        PlayerPrefs.DeleteAll();

        //}
    }
}

public class TokenInfo
{
    public string access_token;
    public string expires;
    public string expires_in;
    public string issued;
    public string token_type;
    public string userName;
}

[Serializable]
public class UserInfo
{
    public string AvatarUrl;
    public string Coins;
    public string Crystals;
    public FollowingType FollowingType;
    public string Id;
    public string Level;
    public string Name;
    public List<long> PublishedLevels;
    public string RegistrationData;
    public string SkinUrl;
    public UserStats Stats;
}

[Serializable]
public class UserStats
{
    public long BestLevelPasses;
    public long BestLevelPlays;
    public long BronzeCups;
    public long GoldenCups;
    public long LevelLikes;
    public long LevelPlayes;
    public long LevelPublishes;
    public long PassedLevels;
    public long SilverCups;
}

[Serializable]
public class JResult
{
    public string Bytes;
}