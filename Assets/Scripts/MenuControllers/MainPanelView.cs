﻿using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Assets.Scripts.MenuControllers
{
    public enum PageType
    {
        None,
        Feed,
        Shop,
        //            Create,
        Library,
        Profile
    }

    public class MainPanelView : MonoBehaviour
    {
        public HorizontalScrollSnap _menuSnapScroll;

        public void SwitchPageTo(PageType type)
        {
            _menuSnapScroll.GoToScreen((int) type - 1);
        }
    }
}