﻿using Assets.Scripts.MenuControllers.CommentsController;
using UnityEngine;
using UnityEngine.UI;

public class NavigationPanelController : MonoBehaviour
{
    public string PanelValue;
    public ContentController FeedContentController;
    public GameObject PanelTop;
    public GameObject PanelLiked;
    public GameObject PanelAll;


    public void OnNavigationBtnClick()
    {
        switch (PanelValue)
        {
            case "All":
            {
                FeedContentController.DisableTopBtns();
                PanelAll.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 4);
                PanelLiked.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 5);
                PanelTop.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 6);

                SetActiveColorPanel("All");

                FeedContentController.ClearFeedContent();
                FeedContentController.FeedType = FeedType.All;
                FeedContentController.UpdateLevelsFeed(1);
                break;
            }

            case "Liked":
            {
                FeedContentController.DisableTopBtns();
                PanelLiked.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 4);
                PanelAll.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 5);
                PanelTop.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 6);

                SetActiveColorPanel("Liked");

                FeedContentController.ClearFeedContent();
                FeedContentController.FeedType = FeedType.Liked;
                FeedContentController.UpdateLevelsFeed(1);
                break;
            }

            case "Top":
            {
                FeedContentController.DisableTopBtns();
                PanelTop.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 4);
                PanelLiked.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 5);
                PanelAll.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 6);

                SetActiveColorPanel("Top");

                FeedContentController.ClearFeedContent();
                FeedContentController.FeedType = FeedType.Top;
                FeedContentController.UpdateLevelsFeed(1);
                break;
            }
        }
    }

    public void SetDefaultPosition()
    {
        PanelAll.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 4);
        PanelLiked.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 5);
        PanelTop.transform.SetSiblingIndex(gameObject.transform.parent.childCount - 6);
    }

    private void SetActiveColorPanel(string panelValue)
    {
        PanelLiked.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        PanelTop.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        PanelAll.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);

        switch (panelValue)
        {
            case "All":
            {
                PanelLiked.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                PanelTop.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                break;
            }

            case "Liked":
            {
                PanelAll.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                PanelTop.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                break;
            }

            case "Top":
            {
                PanelAll.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                PanelLiked.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                break;
            }
        }
    }
}