﻿namespace platformer.network
{
    public class NetworkRequests
    {
		public const string LoginRequest = "{0}/api/Account/Login";
		public const string UserProfileRequest = "{0}/api/Account/UserInfo";
        public const string ValuesRequest = "{0}/api/Values";
        public const string NickNameRequest = "{0}/api/User/changeUserName";
        public const string TokenRequest = "{0}/token";
        public const string ChangeAvatarRequest = "{0}/api/User/changeAvatar";
        public const string NewLevelsRequest = "{0}/api/Level/newLevels";
        public const string FeedLevelsRequest = "{0}/api/Level/feedLevels";
        public const string UserByIdRequest = "{0}/api/User/getUsersById";
        public const string CommentByIdRequest = "{0}/api/Comment/getComentsById";
        public const string GetLevelLikesRequest = "{0}api/Like/getLevelLikes?levelId=";
        public const string AddLikeRequest = "{0}api/Like/addLike?levelId=";
        public const string AddCommentRequest = "{0}api/Comment/addComment";
        public const string GetStartLevelRequest = "{0}api/Level/getStartLevel?idLevel=";
        public const string RemoveLikeRequest = "{0}api/Like/RemoveLike?levelId=";
        public const string UserLevelsRequest = "{0}api/Level/userLevels";
        public const string GetLevelByLinkRequest = "{0}api/Level/getLevelByLink?link=";
        public const string GetLevelByTagRequest = "{0}api/Level/getLevelsByTag?tag=";
        public const string GetLevelByIdRequest = "{0}api/Level/getLevelsById";
        public const string GetUserByFirstNameSymbolsRequest = "{0}api/User/getUsersByFirstNameSymbols?symbols=";
        public const string FaceBookSyncRequest = "{0}api/Account/FaceBookSync";
        public const string SendFileAnon = "{0}api/User/sendFilesTest";
        public const string FollowUSerRequest = "{0}api/Following/followUser";
        public const string UnFollowUSerRequest = "{0}api/Following/unfollowUser";
        public const string GetUserInfoRequest = "{0}api/User/getUserInfo";
        public const string GetFollowedAndFollowersRequest = "{0}api/Following/getFollowedAndFollowers";
        public const string FollowedFbUsersRequest = "{0}api/Following/followFbUsers";
        public const string GetLevelsThatUserLikeRequest = "{0}api/Level/getLevelsThatUserLike";
        public const string GetUserImageRequest = "{0}api/User/getUserImage?path=";
        public const string GetLevelImageRequest = "{0}api/Level/getLevelImage?path=";
    }
}
