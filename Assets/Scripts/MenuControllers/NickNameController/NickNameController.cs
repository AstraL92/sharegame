﻿using System;
using Assets.Scripts.MenuControllers.Login;
using HTTP;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers.NickNameController
{
    [Serializable]
    public class NickNameController : MonoBehaviour
    {
        public RequestSendHandler RequestSendHandler;
        public InputField InputField;
        public Text UserName;

        private string _nickName;

        private void OnEnable()
        {
            WebAsync.OnNickNameTrue += WebAsync_OnNickNameTrue;
            InputField.text = "";
        }

        void WebAsync_OnNickNameTrue(object sender, EventArgs e)
        {
            Debug.Log("NickName changed");
            UserName.text = _nickName;
            LoginController.UserNameStatic = UserName.text;
            gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            WebAsync.OnNickNameTrue -= WebAsync_OnNickNameTrue;
        }

        public void OnSendBtnClick()
        {
            string requestUrl = string.Format(NetworkRequests.NickNameRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 2;
            if (InputField.text != "")
            {
                string s = "\"" + InputField.text + "\"";
                _nickName = InputField.text;
                RequestSendHandler.SendRequest(uri, s, HttpMethod.Post, ContentType.ApplicationJson, LoginController.TokenType + " " + LoginController.UserToken);
              //  gameObject.SetActive(false);
            }
        }

        public void OnCancelBtnClick()
        {
            gameObject.SetActive(false);
        }
    }

    public class NickName
    {
        public string Name;
    }
}