﻿using Assets.Scripts.MenuControllers;
using Assets.Scripts.MenuControllers.CommentsController;
using UnityEngine;
using UnityEngine.UI;

namespace platformer.menu.view
{
	public class PageButtonView : MonoBehaviour
	{
	    public PageType PageType;
	    public NavigationPanelController NavigationPanelController;
	    public ContentController FeedContentController;
	    public Assets.Scripts.MenuControllers.UserLevelController.ContentController LibraryContentController;

        public void OnBtnClick()
		{
            switch (PageType)
            {
                case PageType.Feed:
                {
                    FeedContentController.ClearFeedContent();
                    FeedContentController.DisableTopBtns();
                    LibraryContentController.ClearFeedContent();
                    FeedContentController.FeedType = FeedType.All;
                    FeedContentController.UpdateLevelsFeed(1);
                    break;
                }
                    
                case PageType.Library:
                {
                    FeedContentController.ClearFeedContent();
                    FeedContentController.DisableTopBtns();
                    LibraryContentController.ClearFeedContent();
                    LibraryContentController.UpdateUserLevelFeed(1, 10);
                    break;
                }
            }
            NavigationPanelController.SetDefaultPosition();
            GameObject.Find("MainPanelView").GetComponent<MainPanelView>().SwitchPageTo(PageType);
		}
	}
}
