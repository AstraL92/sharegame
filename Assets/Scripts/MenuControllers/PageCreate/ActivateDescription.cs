﻿using Assets.Scripts.MenuControllers.Login;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActivateDescription : MonoBehaviour
{

    public string DescriptionText;
    public string Name;
    public int _id;
    public PanelType PanelType;

    public void OnElementClick()
    {
        var elementDescription = GameObject.Find("MenuContextView").GetComponent<LoginController>().DescriptionBg.GetComponent<DescriptionController>();
//        var elementDescription = gameObject.transform.parent.parent.parent.parent.FindChild("Description").GetComponent<DescriptionController>();
        elementDescription.gameObject.SetActive(true);
        elementDescription.UpdateInformation(gameObject.GetComponent<Image>().sprite, Name, DescriptionText);
        elementDescription.CurrentId = _id;
    }
}
