﻿using UnityEngine;
using UnityEngine.UI;

public class DescriptionController : MonoBehaviour
{

    public Image SelectedElement;
    public Text Description;
    public Text Name;
    public int CurrentId;

    public void UpdateInformation(Sprite selectedSprite, string nameText, string descriptionText)
    {
        SelectedElement.sprite = selectedSprite;
        Description.text = descriptionText;
        Name.text = nameText;
    }

//    public void OnLeftBtnClick()
//    {
//        if (CurrentId > 0)
//        {
//            CurrentId--;
//        }
//        else
//        {
//            CurrentId = DropDownFilter.ActivateElementsList.Count - 1;
//        }
//        DropDownFilter.ActivateElementsList[CurrentId].OnElementClick();
//    }
//
//    public void OnRightBtnClick()
//    {
//        if (CurrentId < DropDownFilter.ActivateElementsList.Count - 1)
//        {
//            CurrentId++;
//        }
//        else
//        {
//            CurrentId = 0;
//        }
//        DropDownFilter.ActivateElementsList[CurrentId].OnElementClick();
//    }
}