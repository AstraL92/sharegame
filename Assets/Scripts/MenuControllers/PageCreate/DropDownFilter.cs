﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownFilter : MonoBehaviour
{
    public GameObject Content;
    private List<ActivateDescription> _allElementsList;
//    public static List<ActivateDescription> ActivateElementsList;

    private void Start()
    {
        FillingElementsLists();
    }

    public void Filter()
    {
        int index = gameObject.GetComponent<Dropdown>().value;
        switch (index)
        {
            case 0:
            {
                EnableAllEments();
                break;
            }

            case 1:
            {
                EnableAllEments();
                DisableElements(PanelType.Block);
                break;
            }

            case 2:
            {
                EnableAllEments();
                DisableElements(PanelType.Trap);
                break;
            }

            case 3:
            {
                EnableAllEments();
                DisableElements(PanelType.Item);
                break;
            }

            case 4:
            {
                EnableAllEments();
                DisableElements(PanelType.Mechanism);
                break;
            }

            case 5:
            {
                EnableAllEments();
                DisableElements(PanelType.Main);
                break;
            }

            case 6:
            {
                EnableAllEments();
                DisableElements(PanelType.Monster);
                break;
            }
        }
    }

    private void DisableElements(PanelType panelType)
    {
        for (int i = 0; i < Content.transform.childCount; i++)
        {
            if (Content.transform.GetChild(i).GetComponent<ActivateDescription>() && Content.transform.GetChild(i).GetComponent<ActivateDescription>().PanelType !=
                panelType)
            {
                Content.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    private void EnableAllEments()
    {
        for (int i = 0; i < Content.transform.childCount; i++)
        {
            Content.transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    private void FillingElementsLists()
    {
        _allElementsList = new List<ActivateDescription>();
        for (int i = 0; i < Content.transform.childCount; i++)
        {
            _allElementsList.Add(Content.transform.GetChild(i).GetComponent<ActivateDescription>());
        }
    }

}
public enum PanelType
{
    Block,
    Trap,
    Item,
    Mechanism,
    Main,
    Monster
};