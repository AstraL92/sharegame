﻿using HedgehogTeam.EasyTouch;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PageCreateController : MonoBehaviour
{
    public GameObject ConstructorElementChoose;
    public GameObject ElementDescription;

    public void OnCreateLevelClick()
    {
		GridHandler.ActiveCell = null;
		GridHandler.LevelStructureList = null;
        LevelConstriuctorEnterPoint.CurrentLevel = new Level();
        SceneManager.LoadScene("LevelConstructor");
    }

    public void OnCreateElementClick()
    {
        ConstructorElementChoose.SetActive(true);
    }

    public void OnBackBtnClick()
    {
        gameObject.transform.FindChild("ConstructorElementChoose").gameObject.SetActive(false);
    }
}