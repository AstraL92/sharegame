﻿using UnityEngine;

namespace Assets.Scripts.MenuControllers
{
    public class PanelFriendsController : MonoBehaviour
    {
        public FriendsContentController FriendsContentController;

        public void UpdateFriends()
        {
            FriendsContentController.ClearFriends();
            FriendsContentController.UpdateFriends(1);
        }

        public void OnSearchBtnClick()
        {
            FriendsContentController.ClearFriends();
            FriendsContentController.Search();
        }
    }
}