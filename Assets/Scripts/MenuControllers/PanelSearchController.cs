﻿using Assets.Scripts.MenuControllers.SearchController;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class PanelSearchController : MonoBehaviour
    {
        public InputField SearchText;
        public SearchContentController SearchContentController;

        public void OnSearchBtnClick()
        {
            if (SearchText.text != "")
            {
                SearchContentController.ClearContent();
                var searchText = SearchText.text;
                if (searchText[0] == '#')
                {
                    var tmpString = searchText.Remove(0, 1);
                    SearchContentController.Search("LevelHashTag", tmpString.ToLower());
                }
                else if (searchText.Length == 32)
                {
                    SearchContentController.Search("LevelLink", searchText);
                }
                else
                {
                    SearchContentController.Search("User", searchText);
                }
            }
        }
    }
}