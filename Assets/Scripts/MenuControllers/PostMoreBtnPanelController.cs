﻿using UnityEngine;

namespace Assets.Scripts.MenuControllers
{
    public class PostMoreBtnPanelController : MonoBehaviour
    {
        public GameObject LevelReportPopUp;
        public string Link;

        public void OnbtnCopyLinkClick()
        {
            UniClipboard.SetText(Link);
        }

        public void OnbtnReportClick()
        {
            gameObject.SetActive(false);
            LevelReportPopUp.SetActive(true);
        }
    }
}