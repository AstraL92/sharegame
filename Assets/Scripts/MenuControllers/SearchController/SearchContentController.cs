﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MenuControllers.CommentsController;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers.SearchController
{
    [Serializable]
    public class SearchContentController : MonoBehaviour
    {
        private readonly string[] _authorId = new string[1];
        private readonly List<string> _authorIdList = new List<string>();
        private readonly List<LevelPostController> _levelPostControllerList = new List<LevelPostController>();
        private readonly List<Image> _avatarList = new List<Image>();
        private readonly List<string> _urlAvatarsList = new List<string>();
        private readonly List<string> _urlLevelsList = new List<string>();
        private readonly List<Image> _levelsList = new List<Image>();
       
        public GameObject LevelObject;
        public RequestSendHandler RequestSendHandler;
        public GameObject UserObject;
        public string SearchType;
        public GameObject LikedUsers;

        private string _searchType = "";
        private string _searchText = "";

        private void OnEnable()
        {
            WebAsync.OnGetLevelByLinkTrue += WebAsyncOnOnGetLevelByLinkTrue;
            WebAsync.OnGetUserByIdTrue += WebAsyncOnOnGetUserByIdTrue;
            WebAsync.OnGetLevelByTagTrue += WebAsyncOnOnGetLevelByTagTrue;
            WebAsync.OnGetUserByFirstNameSymbolsTrue += WebAsyncOnOnGetUserByFirstNameSymbolsTrue;
            WebAsync.OnGetUserImageTrue += WebAsyncOnOnGetUserImageTrue;
            WebAsync.OnGetLevelImageTrue += WebAsyncOnOnGetLevelImageTrue;
        }

        private void OnDisable()
        {
            WebAsync.OnGetLevelByLinkTrue -= WebAsyncOnOnGetLevelByLinkTrue;
            WebAsync.OnGetUserByIdTrue -= WebAsyncOnOnGetUserByIdTrue;
            WebAsync.OnGetLevelByTagTrue -= WebAsyncOnOnGetLevelByTagTrue;
            WebAsync.OnGetUserByFirstNameSymbolsTrue -= WebAsyncOnOnGetUserByFirstNameSymbolsTrue;
            WebAsync.OnGetUserImageTrue -= WebAsyncOnOnGetUserImageTrue;
            WebAsync.OnGetLevelImageTrue -= WebAsyncOnOnGetLevelImageTrue;
        }

        private void WebAsyncOnOnGetUserImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.AvatarType == "SearchAvatar")
            {
                string str = WebAsync.WebResponseString;
                if (_urlAvatarsList.Count > 0)
                {
                    var userAvatar = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(userAvatar.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    _avatarList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 1f));
                    if (_urlAvatarsList.Count > 1)
                    {
                        _urlAvatarsList.RemoveAt(0);
                        _avatarList.RemoveAt(0);
                        LoadUserImage();
                    }
                    else
                    {
                        GameObject[] userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");

                        for (int i = 0; i < userProfilesBtns.Length; i++)
                        {
                            userProfilesBtns[i].GetComponent<Button>().enabled = true;
                        }

                        _urlAvatarsList.Clear();
                        _avatarList.Clear();
                        LoginController.AvatarType = "";
                        if (_searchType != "User")
                        {
                            LoadLevelImage();    
                        }
                        
                    }
                }
            }
        }

        private void WebAsyncOnOnGetLevelImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.LevelType == "FeedLevels")
            {
                string str = WebAsync.WebResponseString;
                if (_urlLevelsList.Count > 0)
                {
                    var levelPreview = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(levelPreview.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    texture.LoadImage(decompressedImage);
                    _levelsList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 1f));

                    _levelPostControllerList[0].Connecting.SetActive(false);

                    if (_urlLevelsList.Count > 1)
                    {
                        _urlLevelsList.RemoveAt(0);
                        _levelsList.RemoveAt(0);
                        _levelPostControllerList.RemoveAt(0);
                        LoadLevelImage();
                    }
                    else
                    {
                        _urlLevelsList.Clear();
                        _levelPostControllerList.Clear();
                        _levelsList.Clear();
                    }
                }
            }
        }

        private void LoadUserImage()
        {
            string requestUrl = string.Format(NetworkRequests.GetUserImageRequest + _urlAvatarsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 26;
            LoginController.AvatarType = "SearchAvatar";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void LoadLevelImage()
        {
            string requestUrl = string.Format(NetworkRequests.GetLevelImageRequest + _urlLevelsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 27;
            LoginController.LevelType = "FeedLevels";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void WebAsyncOnOnGetUserByFirstNameSymbolsTrue(object sender, EventArgs eventArgs)
        {
            if (SearchType == "PanelSearch")
            {
                StartCoroutine(NewUserTrue());
            }
        }

        private IEnumerator NewUserTrue()
        {
            string str = WebAsync.WebResponseString;
            UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);
            for (int i = 0; i < userInfo.Length; i++)
            {
                var newUserInfo = Instantiate(UserObject, UserObject.transform.position, UserObject.transform.rotation) as GameObject;
                newUserInfo.transform.SetParent(gameObject.transform);
                newUserInfo.transform.localScale = Vector2.one;
                if (i == userInfo.Length - 1 && userInfo.Length == 10)
                {
                    newUserInfo.transform.FindChild("ViewMoreUsers").gameObject.SetActive(true);
                }
                newUserInfo.GetComponent<UsersController>().SetUserData(userInfo[i].Name, userInfo[i].FollowingType, userInfo[i].Id);
                string url = userInfo[i].AvatarUrl;
                if (url == "")
                {
                    newUserInfo.GetComponent<UsersController>().Avatar.gameObject.SetActive(false);
                }
                else
                {
                    _urlAvatarsList.Add(url);
                    _avatarList.Add(newUserInfo.GetComponent<UsersController>().Avatar);
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (_urlAvatarsList.Count > 0)
            {
                LoadUserImage();    
            }
            
        }

        private IEnumerator NewLevelTrue()
        {
            string str = WebAsync.WebResponseString;
            GetFeedLevelObject[] getFeedLevel = JsonHelper.GetJsonArray<GetFeedLevelObject>(str);
            for (int i = 0; i < getFeedLevel.Length; i++)
            {
                var newLevelPost =
                    Instantiate(LevelObject, LevelObject.transform.position, LevelObject.transform.rotation) as GameObject;

                newLevelPost.transform.SetParent(gameObject.transform);
                newLevelPost.transform.localScale = Vector2.one;
                if (i == getFeedLevel.Length - 1 && getFeedLevel.Length == 10)
                {
                    newLevelPost.transform.FindChild("Accordion").FindChild("ViewMoreLevelPosts").gameObject.SetActive(true);
                }
                newLevelPost.GetComponent<LevelPostController>()
					.SetData(getFeedLevel[i].Description, getFeedLevel[i].Stats.AllPlays,
                        getFeedLevel[i].Link, getFeedLevel[i].LikesCount, getFeedLevel[i].AuthorId,
                        getFeedLevel[i].FirstLikers, getFeedLevel[i].LastComments, getFeedLevel[i].Comments,
                        getFeedLevel[i].Id, getFeedLevel[i].IsLiked, getFeedLevel[i].PlayedType);

                _urlLevelsList.Add(getFeedLevel[i].LevelPreview);
                _levelsList.Add(newLevelPost.GetComponent<LevelPostController>().LevelPreview);

                _authorIdList.Add("\"" + newLevelPost.GetComponent<LevelPostController>().AuthorId + "\"");
                newLevelPost.GetComponent<LevelPostController>().ContentGameobjectName = "searchByTag";
                _levelPostControllerList.Add(newLevelPost.GetComponent<LevelPostController>());
                yield return new WaitForSeconds(0.1f);
            }

            List<string> distinct = _authorIdList.Distinct().ToList();
            string[] tmpArray = distinct.ToArray();
            RequestUserInfoById(tmpArray);
        }

        private void WebAsyncOnOnGetLevelByTagTrue(object sender, EventArgs eventArgs)
        {
            if (SearchType == "PanelSearch")
            {
                StartCoroutine(NewLevelTrue());
            }
        }

        private void WebAsyncOnOnGetUserByIdTrue(object sender, EventArgs eventArgs)
        {
            if (GameObject.Find("MenuContextView").GetComponent<LoginController>().GetUserById == "Search" && SearchType == "PanelSearch")
            {
                string str = WebAsync.WebResponseString;
                switch (_searchType)
                {
                    case "LevelLink":
                    {
                        UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);
                        _levelPostControllerList[0].AuthorName.text = userInfo[0].Name;
//                        string url = RequestSendHandler.BaseServerUrl + userInfo[0].AvatarUrl;
                        string url = userInfo[0].AvatarUrl;
                        
                        if (url == "")
                        {
                            _levelPostControllerList[0].AuthorAvatar.gameObject.SetActive(false);
                        }
                        else
                        {
                            _urlAvatarsList.Add(url);
                            _avatarList.Add(_levelPostControllerList[0].AuthorAvatar);
                            LoadUserImage();
                        }
//                        StartCoroutine(LoadImage(url));
                        break;
                    }

                    case "LevelHashTag":
                    {
                        UserInfo[] userInfo = JsonHelper.GetJsonArray<UserInfo>(str);
                        for (int i = 0; i < _levelPostControllerList.Count; i++)
                        {
                            for (int j = 0; j < userInfo.Length; j++)
                            {
                                if (_levelPostControllerList[i].AuthorId == userInfo[j].Id)
                                {
                                    _levelPostControllerList[i].AuthorName.text = userInfo[j].Name;
//                                    string url = RequestSendHandler.BaseServerUrl + userInfo[j].AvatarUrl;
                                    string url = userInfo[j].AvatarUrl;
                                    if (url == "")
                                    {
                                        _levelPostControllerList[i].AuthorAvatar.gameObject.SetActive(false);
                                    }
                                    else
                                    {
                                        _urlAvatarsList.Add(url);
                                        _avatarList.Add(_levelPostControllerList[i].AuthorAvatar);
                                    }
                                    
                                }
                            }
                        }
                        if (_levelPostControllerList.Count > 0)
                        {
                            LoadUserImage();    
                        }
                        
//                        StartCoroutine(LoadImage());
                        break;
                    }
                        
                }
            }
        }

//        private IEnumerator LoadImage()
//        {
//            for (int i = 0; i < _urlAvatarsList.Count; i++)
//            {
//                var www = new WWW(_urlAvatarsList[i]);
//                yield return www;
//                _avatarList[i].sprite = Sprite.Create(www.texture,
//                    new Rect(0, 0, www.texture.width, www.texture.height),
//                    new Vector2(0f, 1f));
//            }
//            _urlAvatarsList.Clear();
//            _avatarList.Clear();
//            _levelPostControllerList.Clear();
//        }
//
//        private IEnumerator LoadImage(string url)
//        {
//            var www = new WWW(url);
//            yield return www;
//            _levelPostControllerList[0].AuthorAvatar.sprite = Sprite.Create(www.texture,
//                new Rect(0, 0, www.texture.width, www.texture.height),
//                new Vector2(0f, 1f));
//            _levelPostControllerList.Clear();
//            _authorId[0] = null;
//        }

        private void WebAsyncOnOnGetLevelByLinkTrue(object sender, EventArgs eventArgs)
        {
            if (SearchType == "PanelSearch")
            {
                string str = WebAsync.WebResponseString;
                var levelObject = JsonUtility.FromJson<GetFeedLevelObject>(str);
                var newLevelPost =
                    Instantiate(LevelObject, LevelObject.transform.position, LevelObject.transform.rotation) as
                        GameObject;

                newLevelPost.transform.SetParent(gameObject.transform);
                newLevelPost.transform.localScale = Vector2.one;
                newLevelPost.GetComponent<LevelPostController>()
					.SetData(levelObject.Description, levelObject.Stats.AllPlays,
                        levelObject.Link, levelObject.LikesCount, levelObject.AuthorId,
                        levelObject.FirstLikers, levelObject.LastComments, levelObject.Comments,
                        levelObject.Id, levelObject.IsLiked, levelObject.PlayedType);

                _urlLevelsList.Add(levelObject.LevelPreview);
                _levelsList.Add(newLevelPost.GetComponent<LevelPostController>().LevelPreview);

                _authorId[0] = "\"" + levelObject.AuthorId + "\"";
                _levelPostControllerList.Add(newLevelPost.GetComponent<LevelPostController>());
                RequestUserInfoById(_authorId);
            }
        }

        private void RequestUserInfoById(string[] id)
        {
            string requestUrl = string.Format(NetworkRequests.UserByIdRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            string str = "[";

            for (int i = 0; i < id.Length; i++)
            {
                if (id.Length == 1 || i == id.Length - 1)
                {
                    str += id[i];
                }
                else
                {
                    str += id[i] + ",";
                }
            }

            str += "]";

            RequestSendHandler.RequestTypeInt = 5;
            RequestSendHandler.SendRequest(uri, str, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
            GameObject.Find("MenuContextView").GetComponent<LoginController>().GetUserById = "Search";
        }
        
        public void Search(string searchType, string searchText)
        {
            switch (searchType)
            {
                case "User":
                {
                    _searchType = "User";
                    _searchText = searchText;
                    UserSearch(1, searchText);
                    break;
                }

                case "LevelLink":
                {
                    _searchType = "LevelLink";
                    LevelLinkSearch(searchText);
                    break;
                }

                case "LevelHashTag":
                {
                    _searchType = "LevelHashTag";
                    _searchText = searchText;
                    LevelTagSearch(1, searchText);
                    break;
                }
            }
        }

        private void UserSearch(int pageNumber, string link)
        {
            string requestUrl = string.Format(NetworkRequests.GetUserByFirstNameSymbolsRequest + link,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);
            var newLevel = new NewLevel { PageNumber = pageNumber, DisplayRecods = 10 };
            RequestSendHandler.RequestTypeInt = 16;
            SearchType = "PanelSearch";
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }


        private void LevelLinkSearch(string link)
        {
            string requestUrl = string.Format(NetworkRequests.GetLevelByLinkRequest + link,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 14;
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void LevelTagSearch(int pageNumber, string link)
        {
            string requestUrl = string.Format(NetworkRequests.GetLevelByTagRequest + link,
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            var newLevel = new NewLevel { PageNumber = pageNumber, DisplayRecods = 10 };

            RequestSendHandler.RequestTypeInt = 15;
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        public void ClearContent()
        {
            for (int i = 1; i < gameObject.transform.childCount; i++)
            {
                Destroy(gameObject.transform.GetChild(i).gameObject);
            }
            _levelPostControllerList.Clear();
            _authorId[0] = null;
            _urlAvatarsList.Clear();
            _avatarList.Clear();
            _urlLevelsList.Clear();
            _levelsList.Clear();
        }

        public void UpdateSearchByTag(int pageNumber)
        {
            LevelTagSearch(pageNumber, _searchText);
        }

        public void UpdateUsersSerach(int pageNumber)
        {
            UserSearch(pageNumber, _searchText);
        }
    }
}

