﻿using UnityEngine;
using UnityEngine.UI;

public class SetOrangeBg : MonoBehaviour
{
    public string BgName;

    public Image FirstPanel;
    public Image SecondPanel;

    public void ChangeColor()
    {
        switch (BgName)
        {
            case "FirstPanel":
            {
                FirstPanel.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                SecondPanel.GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                break;
            }

            case "SecondPanel":
            {
                FirstPanel.GetComponent<Image>().color = new Color32(255, 220, 165, 255);
                SecondPanel.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                break;
            }
        }
    }
}