﻿using System.Runtime.InteropServices;
using UnityEngine;

public class UniClipboard
{
    private static IBoard _board;

    private static IBoard board
    {
        get
        {
            if (_board == null)
            {
                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    _board = new EditorBoard();
                }
                else if (Application.platform == RuntimePlatform.Android)
                {
                    _board = new AndroidBoard();
                }
                else if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    _board = new IOSBoard();
                }
            }
            return _board;
        }
    }

    public static void SetText(string str)
    {
        board.SetText(str);
    }

    public static string GetText()
    {
        return board.GetText();
    }
}

internal interface IBoard
{
    void SetText(string str);
    string GetText();
}

internal class EditorBoard : IBoard
{
    public void SetText(string str)
    {
        GUIUtility.systemCopyBuffer = str;
    }

    public string GetText()
    {
        return GUIUtility.systemCopyBuffer;
    }
}

internal class AndroidBoard : IBoard
{
    private readonly AndroidJavaClass cb = new AndroidJavaClass("jp.ne.donuts.uniclipboard.Clipboard");

    public void SetText(string str)
    {
        cb.CallStatic("setText", str);
    }

    public string GetText()
    {
        return cb.CallStatic<string>("getText");
    }
}

internal class IOSBoard : IBoard
{
    public void SetText(string str)
    {
        if (Application.platform != RuntimePlatform.OSXEditor)
        {
            SetText_(str);
        }
    }

    public string GetText()
    {
        return GetText_();
    }

    [DllImport("__Internal")]
    private static extern void SetText_(string str);

    [DllImport("__Internal")]
    private static extern string GetText_();
}