﻿using System;
using System.Collections;
using System.IO;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers
{
    public class UserInfoController : MonoBehaviour
    {
        public GameObject NickName;
        public RequestSendHandler RequestSendHandler;
        public Image UserAvatar;
        public GameObject EditProfileAvatarWindow;
        private byte[] _textureBuffer = new byte[0];

        private void OnEnable()
        {
            WebAsync.OnChangeAvatarTrue += WebAsyncOnOnChangeAvatarTrue;
        }

        private void OnDisable()
        {
            WebAsync.OnChangeAvatarTrue -= WebAsyncOnOnChangeAvatarTrue;
        }

        private void WebAsyncOnOnChangeAvatarTrue(object sender, EventArgs eventArgs)
        {
            string str = WebAsync.WebResponseString;
            var userAvatar = JsonUtility.FromJson<JResult>(str);
            byte[] image = Convert.FromBase64String(userAvatar.Bytes);
            var texture = new Texture2D(1, 1);
            byte[] decompressedImage = QuickLZ.decompress(image);
            texture.LoadImage(decompressedImage);
            UserAvatar.sprite = Sprite.Create(texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0f, 1f));
            /*
            var str = WebAsync.WebResponseString.Replace("\"", "");
            var url = RequestSendHandler.BaseServerUrl + str;
            StartCoroutine(LoadImage(url));
            */
        }

        private IEnumerator LoadImage(string url)
        {
            Debug.Log("MY AVATAR LOADIMAGE URL: " + url);
            WWW www = new WWW(url);
            yield return www;
            UserAvatar.sprite = Sprite.Create(www.texture,
                new Rect(0, 0, www.texture.width, www.texture.height),
                new Vector2(0f, 1f));
        }

        public void OnNickNameBtnClick()
        {
            NickName.SetActive(true);
        }

        public void OnEditAvatarBtnClick()
        {
            EditProfileAvatarWindow.SetActive(true);

         //   StreamManager.LoadFileDialog(FolderLocations.Pictures, 128, 128, new[] {".png", ".jpg", ".jpeg"},
        //        ImageLoadedCallback);
        }

        private void ImageLoadedCallback(Stream stream, bool succeeded)
        {
            if (!succeeded)
            {
                if (stream != null) stream.Dispose();
                return;
            }

            try
            {
                var data = new byte[stream.Length];
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    if (data.Length <= 51200)
                    {
                        stream.Read(data, 0, data.Length);
                        var newImage = new Texture2D(90, 90);
                        newImage.LoadImage(data);

                        newImage.Apply();
                        _textureBuffer = newImage.EncodeToPNG();
                    }
                    else
                    {
                        Debug.Log("Error Image Size");
                    }
                }
                else if (Application.platform == RuntimePlatform.Android)
                {
                    stream.Read(data, 0, data.Length);
                    var newImage = new Texture2D(90, 90);
                    newImage.LoadImage(data);

                    newImage.Apply();
                    _textureBuffer = newImage.EncodeToPNG();
                }
                else if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    stream.Read(data, 0, data.Length);
                    var newImage = new Texture2D(90, 90);
                    newImage.LoadImage(data);
                    newImage.Apply();
                    _textureBuffer = newImage.EncodeToPNG();

                    string requestUrl = string.Format(NetworkRequests.ChangeAvatarRequest,
                        RequestSendHandler.BaseServerUrl);
                    var uri = new Uri(requestUrl);

                    RequestSendHandler.RequestTypeInt = 3;
                    RequestSendHandler.SendRequest(uri, _textureBuffer, HttpMethod.Post, ContentType.ApplicationJson,
                        LoginController.TokenType + " " + LoginController.UserToken);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (stream != null) stream.Dispose();
            }
        }
    }

    
}