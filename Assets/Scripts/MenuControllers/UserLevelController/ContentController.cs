﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers.CommentsController;
using Assets.Scripts.MenuControllers.Login;
using platformer.network;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuControllers.UserLevelController
{
    public class ContentController : MonoBehaviour
    {
        public RequestSendHandler RequestSendHandler;
        public GameObject MyLevelPost;
        public Button LibraryButton;
        public GameObject LikedUsers;
        public GameObject NoLevelsPanel;

        private readonly List<string> _urlLevelsList = new List<string>();
        private readonly List<Image> _levelsList = new List<Image>();
        private readonly List<LevelPostController> _levelPostControllerList = new List<LevelPostController>();
        
        public void UpdateUserLevelFeed(int pageNumber, int displayRecods)
        {
            string requestUrl = string.Format(NetworkRequests.UserLevelsRequest, RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            var newLevel = new NewLevel { PageNumber = pageNumber, DisplayRecods = displayRecods };

            RequestSendHandler.RequestTypeInt = 13;
            RequestSendHandler.SendRequest(uri, newLevel, HttpMethod.Post, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void OnEnable()
        {
            WebAsync.OnUserLevelsTrue += WebAsyncOnOnUserLevelsTrue;
            WebAsync.OnGetLevelImageTrue += WebAsyncOnOnGetLevelImageTrue;
        }

        private void OnDisable()
        {
            WebAsync.OnUserLevelsTrue -= WebAsyncOnOnUserLevelsTrue;
            WebAsync.OnGetLevelImageTrue -= WebAsyncOnOnGetLevelImageTrue;
        }

        private void WebAsyncOnOnGetLevelImageTrue(object sender, EventArgs eventArgs)
        {
            if (LoginController.LevelType == "LibraryLevels")
            {
                string str = WebAsync.WebResponseString;
                if (_urlLevelsList.Count > 0)
                {
                    var levelPreview = JsonUtility.FromJson<JResult>(str);
                    byte[] image = Convert.FromBase64String(levelPreview.Bytes);
                    byte[] decompressedImage = QuickLZ.decompress(image);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(decompressedImage);
                    texture.LoadImage(decompressedImage);
                    _levelsList[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                        new Vector2(0f, 1f));

                    _levelPostControllerList[0].Connecting.SetActive(false);

                    if (_urlLevelsList.Count > 1)
                    {
                        _urlLevelsList.RemoveAt(0);
                        _levelsList.RemoveAt(0);
                        _levelPostControllerList.RemoveAt(0);
                        LoadLevelImage();
                    }
                    else
                    {
                        _urlLevelsList.Clear();
                        _levelPostControllerList.Clear();
                        _levelsList.Clear();
                    }
                }
            }
        }

        private void LoadLevelImage()
        {
            string requestUrl = string.Format(NetworkRequests.GetLevelImageRequest + _urlLevelsList[0],
                RequestSendHandler.BaseServerUrl);
            var uri = new Uri(requestUrl);

            RequestSendHandler.RequestTypeInt = 27;
            LoginController.LevelType = "LibraryLevels";
            RequestSendHandler.SendRequest(uri, "", HttpMethod.Get, ContentType.ApplicationJson,
                LoginController.TokenType + " " + LoginController.UserToken);
        }

        private void WebAsyncOnOnUserLevelsTrue(object sender, EventArgs eventArgs)
        {
            StartCoroutine(UserLevelTrue());
        }

        private IEnumerator UserLevelTrue()
        {
            string str = WebAsync.WebResponseString;
            GetFeedLevelObject[] getFeedLevel = JsonHelper.GetJsonArray<GetFeedLevelObject>(str);
            if (getFeedLevel.Length <= 0)
            {
                GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
            }
            for (int i = 0; i < getFeedLevel.Length; i++)
            {
                var newLevelPost =
                    Instantiate(MyLevelPost, MyLevelPost.transform.position, MyLevelPost.transform.rotation) as GameObject;

                newLevelPost.transform.SetParent(gameObject.transform);
                newLevelPost.transform.localScale = Vector2.one;
                if (i == getFeedLevel.Length - 1 && getFeedLevel.Length == 10)
                {
                    newLevelPost.transform.FindChild("Accordion").FindChild("ViewMoreLevelPosts").gameObject.SetActive(true);
                }
                newLevelPost.GetComponent<LevelPostController>()
					.SetData(getFeedLevel[i].Description, getFeedLevel[i].Stats.AllPlays,
                        getFeedLevel[i].Link, getFeedLevel[i].LikesCount, getFeedLevel[i].AuthorId,
                        getFeedLevel[i].FirstLikers, getFeedLevel[i].LastComments, getFeedLevel[i].Comments,
                        getFeedLevel[i].Id, getFeedLevel[i].IsLiked, getFeedLevel[i].PlayedType);

                var loginController = GameObject.Find("MenuContextView").GetComponent<LoginController>();

                _urlLevelsList.Add(getFeedLevel[i].LevelPreview);
                _levelsList.Add(newLevelPost.GetComponent<LevelPostController>().LevelPreview);
                _levelPostControllerList.Add(newLevelPost.GetComponent<LevelPostController>());

                var userAvatar = newLevelPost.GetComponent<LevelPostController>().AuthorAvatar;
                if (loginController.UserAvatar.sprite != null)
                {
                    userAvatar.sprite = loginController.UserAvatar.sprite;
                }
                else
                {
                    userAvatar.gameObject.SetActive(false);
                }
                
                newLevelPost.GetComponent<LevelPostController>().AuthorName.text = loginController.UserName.text;

                newLevelPost.GetComponent<LevelPostController>().ContentGameobjectName = "pageProfile";

                yield return new WaitForSeconds(0.1f);
            }
//            LibraryButton.enabled = true;
            var userProfilesBtns = GameObject.FindGameObjectsWithTag("UserProfile");
            for (int i = 0; i < userProfilesBtns.Length; i++)
            {
                userProfilesBtns[i].GetComponent<Button>().enabled = true;
            }
            if (getFeedLevel.Length > 0)
            {
                LoadLevelImage();
                NoLevelsPanel.SetActive(false);
            }
            else
            {
                NoLevelsPanel.SetActive(true);
            }
        }

        public void ClearFeedContent()
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                Destroy(gameObject.transform.GetChild(i).gameObject);
            }
        }
    }
}