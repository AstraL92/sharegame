﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.MenuControllers;
using Assets.Scripts.MenuControllers.Login;
using Assets.Scripts.MenuControllers.SearchController;
using platformer.menu.signals;
using platformer.menu.view;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class UsersController : MonoBehaviour
{
    public Image Avatar;
    public Text SocialButtonText;
    public string UserId;
    public Text UserName;
    public GameObject ViewMoreUsersGameObject;
    
    public int FollowingTypeInt;
    public Sprite AddFriendBtn;
    public Sprite FollowBtn;
    public Sprite FriendsBtn;
    public Sprite FollowingBtn;
    public Image SocialBtnImage;

    private PageButtonView _btnProfile;
    private LoginController _loginController;
    private HorizontalScrollSnap _menuSnapScroll;

    private void Start()
    {
        _menuSnapScroll = GameObject.Find("PagesScrollSnap").GetComponent<HorizontalScrollSnap>();
        _btnProfile = GameObject.Find("btnProfile").GetComponent<PageButtonView>();
    }

    public void SetUserData(string userName, FollowingType followingType, string id)
    {
//        string str = avatar.Replace("\"", "");
//        string url = RequestSendHandler.BaseServerUrl + str;
//        if (avatar == "")
//        {
//            Avatar.gameObject.SetActive(false);
//        }
//        StartCoroutine(LoadImage(url, Avatar));
        UserId = id;
        FollowingTypeInt = (int)followingType;
        UserName.text = userName;

        switch (followingType)
        {
            case FollowingType.Followed:
            {
                SocialButtonText.text = "Following";
                SocialBtnImage.sprite = FollowingBtn;
                SocialBtnImage.SetNativeSize();
                break;
            }
            case FollowingType.Follower:
            {
                SocialButtonText.text = "+Friend";
                SocialBtnImage.sprite = AddFriendBtn;
                SocialBtnImage.SetNativeSize();
                break;
            }
            case FollowingType.Friend:
            {
                SocialButtonText.text = "Friends";
                SocialBtnImage.sprite = FriendsBtn;
                SocialBtnImage.SetNativeSize();
                break;
            }
            case FollowingType.None:
            {
                SocialButtonText.text = "Follow";
                SocialBtnImage.sprite = FollowBtn;
                SocialBtnImage.SetNativeSize();
                break;
            }
        }

        _loginController = GameObject.Find("MenuContextView").GetComponent<LoginController>();
        if (userName == _loginController.UserName.text)
        {
            SocialButtonText.transform.parent.gameObject.SetActive(false);
        }
    }

//    private IEnumerator LoadImage(string url, Image avatarSprite)
//    {
//        var www = new WWW(url);
//        yield return www;
//        avatarSprite.sprite = Sprite.Create(www.texture,
//            new Rect(0, 0, www.texture.width, www.texture.height),
//            new Vector2(0f, 1f));
//    }

    public void ViewMoreUsers()
    {
        int pageNumber = (gameObject.transform.parent.childCount + 9)/10;
        gameObject.transform.parent.GetComponent<SearchContentController>().UpdateUsersSerach(pageNumber);
        ViewMoreUsersGameObject.SetActive(false);
    }

    public void ViewFriendsFrofile()
    {
//        _menuSnapScroll.GoToScreen((int) PageType.Profile - 1);

        var friendsProfile = GameObject.Find("MenuContextView").GetComponent<FollowingController>().FriendProfile;
            

        Transform userProfile =
            _menuSnapScroll.gameObject.transform.FindChild("Content")
                .FindChild("pageProfile")
                .FindChild("PanelProfile");
        Text authorName = GameObject.Find("MenuContextView").GetComponent<LoginController>().UserName;
        if (UserName.text == authorName.text)
        {
            _btnProfile.OnBtnClick();
            userProfile.SetAsLastSibling();
        }
        else
        {
            friendsProfile.SetActive(true);
//            friendsProfile.transform.SetAsLastSibling();
            friendsProfile.GetComponent<FriendsProfileController>().UsersController =
                gameObject.GetComponent<UsersController>();
            friendsProfile.GetComponent<FriendsProfileController>().SetFriendData(Avatar, UserName, (FollowingType)FollowingTypeInt, UserId);
        }

        var likedUsersController = gameObject.transform.parent.parent.parent.parent.GetComponent<LikedUsersController>();
        if (likedUsersController)
        {
            likedUsersController.OnBackBtnClick();
        }
    }


    public void OnSocialBtnClick()
    {
        var followingController = GameObject.Find("MenuContextView").GetComponent<FollowingController>();
        var alert = GameObject.Find("MenuContextView").GetComponent<LoginController>().Alert;
        switch (SocialButtonText.text)
        {
            case "Following":
            {
//                FollowingTypeInt = (int)FollowingType.None;
//                followingController.UnFollowUser(UserId, SocialButtonText, SocialBtnImage, FollowingType.None);
                alert.SetActive(true);
                alert.GetComponent<AlertController>().SetUserData(Avatar, UserName.text, gameObject.GetComponent<UsersController>(), followingController, SocialButtonText, UserId, SocialBtnImage);
                break;
            }

            case "+Friend":
            {
                FollowingTypeInt = (int)FollowingType.Friend;
                followingController.FollowUser(UserId, SocialButtonText, SocialBtnImage, FollowingType.Friend);
                break;
            }

            case "Friends":
            {
                alert.SetActive(true);
                alert.GetComponent<AlertController>().SetUserData(Avatar, UserName.text, gameObject.GetComponent<UsersController>(), followingController, SocialButtonText, UserId, SocialBtnImage);
//                FollowingTypeInt = (int)FollowingType.Follower;
//                followingController.UnFollowUser(UserId, SocialButtonText, SocialBtnImage, FollowingType.Follower);
                break;
            }

            case "Follow":
            {
                FollowingTypeInt = (int)FollowingType.Followed;
                followingController.FollowUser(UserId, SocialButtonText, SocialBtnImage, FollowingType.Followed);
                break;
            }
        }
    }
}