﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading;
using UnityEngine;

/// <summary>
///     The RequestState class passes data across async calls.
/// </summary>
public class RequestState
{
    public string errorMessage;
    public WebRequest webRequest;
    public WebResponse webResponse;

    public RequestState()
    {
        webRequest = null;
        webResponse = null;
        errorMessage = null;
    }
}

/// <summary>
///     Simplify getting web requests asynchronously
/// </summary>
public class WebAsync
{
    private const int TIMEOUT = 10; // seconds
    public static int ResponseCode;

    public static string WebResponseString;
    public static string WebResponseErrorString;

    public bool isResponseCompleted = false;
    public bool isURLcheckingCompleted = false;
    public bool isURLmissing = false;
    public RequestState requestState;

    public static event EventHandler OnLoginTrue;
    public static event EventHandler OnTokenTrue;
    public static event EventHandler OnNickNameTrue;
    public static event EventHandler OnChangeAvatarTrue;
    public static event EventHandler OnNewLevelsTrue;

    public static event EventHandler OnGetUserByIdTrue;
    public static event EventHandler OnGetCommentsByIdTrue;
    public static event EventHandler OnGetLevelLikedTrue;
    public static event EventHandler OnGetUsersLikedLevelTrue;
    public static event EventHandler OnAddLikeTrue;

    public static event EventHandler OnAddCommentTrue;
    public static event EventHandler OnGetStartLevelTrue;
    public static event EventHandler OnRemoveLikeTrue;
    public static event EventHandler OnUserLevelsTrue;
    public static event EventHandler OnGetLevelByLinkTrue;

    public static event EventHandler OnGetLevelByTagTrue;
    public static event EventHandler OnGetUserByFirstNameSymbolsTrue;
    public static event EventHandler OnFaceBookSyncTrue;
    public static event EventHandler OnGetLevelByIdTrue;
    public static event EventHandler OnFollowUserTrue;

    public static event EventHandler OnUnFollowUserTrue;
    public static event EventHandler OnGetUserInfoTrue;
    public static event EventHandler OnGetFollowedAndFollowersTrue;
    public static event EventHandler OnFollowedFbUsersTrue;
    public static event EventHandler OnGetLevelsThatUserLikeTrue;

    public static event EventHandler OnGetUserImageTrue;
    public static event EventHandler OnGetLevelImageTrue;
    public static event EventHandler OnNewLevelCreated;

    public static event EventHandler OnLikedLevelError;

    /// <summary>
    ///     Equivalent of webRequest.GetResponse, but using our own RequestState.
    ///     This can or should be used along with web async instance's isResponseCompleted parameter
    ///     inside a IEnumerator method capable of yield return for it, although it's mostly for clarity.
    ///     Here's an usage example:
    ///     WebAsync webAsync = new WebAsync(); StartCoroutine( webAsync.GetReseponse(webRequest) );
    ///     while (! webAsync.isResponseCompleted) yield return null;
    ///     RequestState result = webAsync.requestState;
    /// </summary>
    /// <param name='webRequest'>
    ///     A System.Net.WebRequest instanced var.
    /// </param>
    public IEnumerator GetResponse(WebRequest webRequest)
    {
        isResponseCompleted = false;
        requestState = new RequestState();

        // Put the request into the state object so it can be passed around
        requestState.webRequest = webRequest;

        // Do the actual async call here
        IAsyncResult asyncResult = webRequest.BeginGetResponse(
            RespCallback, requestState);

        // WebRequest timeout won't work in async calls, so we need this instead
        ThreadPool.RegisterWaitForSingleObject(
            asyncResult.AsyncWaitHandle,
            ScanTimeoutCallback,
            requestState,
            (TIMEOUT*1000), // obviously because this is in miliseconds
            true
            );

        // Wait until the the call is completed
        while (!asyncResult.IsCompleted)
        {
            yield return null;
        }

        // Help debugging possibly unpredictable results
        if (requestState != null)
        {
            if (requestState.errorMessage != null)
            {
                switch (RequestSendHandler.RequestTypeInt)
                {
                    case 0:
                    {
                        Debug.Log("login error");
                        Debug.LogError("Serwer response error : " + requestState.errorMessage);
                        break;
                    }

                    case 1:
                    {
                        Debug.Log("token error");
                        Debug.LogError("Serwer response error : " + requestState.errorMessage);
                        break;
                    }

                    case 2:
                    {
                        Debug.Log("nick name error");
                        Debug.LogError("Serwer response error : " + requestState.errorMessage);
                        break;
                    }

                    case 3:
                    {
                        Debug.Log("avatar error");
                        Debug.LogError("Serwer avatar error : " + requestState.errorMessage);

                        break;
                    }
                    case 4:
                    {
                        Debug.Log("new levels error");
                            if (OnLikedLevelError != null)
                            {
                                OnLikedLevelError(this, null);
                            }
                        Debug.LogError("Serwer new level error : " + requestState.errorMessage);
                        break;
                    }

                    case 5:
                    {
                        Debug.Log("get user by id error");
                        Debug.LogError("Serwer get user by id error : " + requestState.errorMessage);
                        break;
                    }

                    case 6:
                    {
                        Debug.Log("get comment by id error");
                        Debug.LogError("Serwer get comment by id error : " + requestState.errorMessage);
                        break;
                    }

                    case 7:
                    {
                        Debug.Log("get  levels liked  error");
                        Debug.LogError("Serwer get  levels liked error : " + requestState.errorMessage);
                        break;
                    }

                    case 8:
                    {
                        Debug.Log("get liked levels by user error");
                        Debug.LogError("Serwer get liked levels by user error : " + requestState.errorMessage);
                        break;
                    }

                    case 9:
                    {
                        Debug.Log("add like error");
                        Debug.LogError("Serwer add like error : " + requestState.errorMessage);
                        break;
                    }

                    case 10:
                    {
                        Debug.Log("add comment error");
                        Debug.LogError("Serwer add comment error : " + requestState.errorMessage);
                        break;
                    }

                    case 11:
                    {
                        Debug.Log("Get Start Level error");
                        Debug.LogError("Serwer Get Start Level error : " + requestState.errorMessage);
                        break;
                    }

                    case 12:
                    {
                        Debug.Log("Remove like error");
                        Debug.LogError("Serwer Remove like error : " + requestState.errorMessage);
                        break;
                    }

                    case 13:
                    {
                        Debug.Log("Users level error");
                        Debug.LogError("Serwer Users level error : " + requestState.errorMessage);
                        break;
                    }

                    case 14:
                    {
                        Debug.Log("Get level by link error");
                        Debug.LogError("Serwer Get level by link error : " + requestState.errorMessage);
                        break;
                    }

                    case 15:
                    {
                        Debug.Log("Get level by tag error");
                        Debug.LogError("Serwer Get level by tag error : " + requestState.errorMessage);
                        break;
                    }

                    case 16:
                    {
                        Debug.Log("Get User By First Name Symbols error");
                        Debug.LogError("Serwer Get User By First Name Symbols error : " + requestState.errorMessage);
                        break;
                    }

                    case 17:
                    {
                        Debug.Log("Get facebook sync error");
                        Debug.LogError("Serwer Get facebook sync error : " + requestState.errorMessage);
                        break;
                    }

                    case 18:
                    {
                        Debug.Log("Get level by id error");
                        Debug.LogError("Serwer level by id error : " + requestState.errorMessage);
                        break;
                    }

                    case 19:
                    {
                        Debug.Log("Follow user error");
                        Debug.LogError("Serwer Follow user error : " + requestState.errorMessage);
                        break;
                    }

                    case 20:
                    {
                        Debug.Log("UnFollow user error");
                        Debug.LogError("Serwer UnFollow user error : " + requestState.errorMessage);
                        break;
                    }

                    case 21:
                    {
                        Debug.Log("get user info error");
                        Debug.LogError("Serwer get user info error : " + requestState.errorMessage);
                        break;
                    }

                    case 22:
                    {
                        Debug.Log("get followed/followers error");
                        Debug.LogError("Serwer get followed/followers error : " + requestState.errorMessage);
                        break;
                    }

                    case 23:
                    {
                        Debug.LogError("Cerate new level error : " + requestState.errorMessage);
                        break;
                    }

                    case 24:
                    {
                        Debug.LogError("OnFollowedFbUsersTrue : " + requestState.errorMessage);
                        break;
                    }

                    case 25:
                    {
                        Debug.LogError("OnGetLevelsThatUserLikeTrue : " + requestState.errorMessage);
                        break;
                    }

                    case 26:
                    {
                        Debug.LogError("OnGetUserImageTrue : " + requestState.errorMessage);
                        break;
                    }

                    case 27:
                    {
                        Debug.LogError("OnGetLevelImageTrue : " + requestState.errorMessage);
                        break;
                    }

                    default:
                    {
                        Debug.Log("не обработанная ошибка");
                        Debug.LogError("error : " + requestState.errorMessage);
                        break;
                    }
                }
            }
            else
            {
                switch (RequestSendHandler.RequestTypeInt)
                {
                    case 0:
                    {
                        if (OnLoginTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnLoginTrue(this, null);
                        }
                        break;
                    }

                    case 1:
                    {
                        if (OnTokenTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnTokenTrue(this, null);
                        }
                        break;
                    }

                    case 2:
                    {
                        if (OnNickNameTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            OnNickNameTrue(this, null);
                        }
                        break;
                    }

                    case 3:
                    {
                        if (OnChangeAvatarTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnChangeAvatarTrue(this, null);
                        }
                        break;
                    }

                    case 4:
                    {
                        if (OnNewLevelsTrue != null)
                        {
//                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnNewLevelsTrue(this, null);
                        }
                        break;
                    }

                    case 5:
                    {
                        if (OnGetUserByIdTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetUserByIdTrue(this, null);
                        }
                        break;
                    }

                    case 6:
                    {
                        if (OnGetCommentsByIdTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetCommentsByIdTrue(this, null);
                        }
                        break;
                    }

                    case 7:
                    {
                        if (OnGetLevelLikedTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetLevelLikedTrue(this, null);
                        }
                        break;
                    }

                    case 8:
                    {
                        if (OnGetUsersLikedLevelTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetUsersLikedLevelTrue(this, null);
                        }
                        break;
                    }

                    case 9:
                    {
                        if (OnAddLikeTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnAddLikeTrue(this, null);
                        }
                        break;
                    }

                    case 10:
                    {
                        if (OnAddCommentTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnAddCommentTrue(this, null);
                        }
                        break;
                    }

                    case 11:
                    {
                        if (OnGetStartLevelTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetStartLevelTrue(this, null);
                        }
                        break;
                    }

                    case 12:
                    {
                        if (OnRemoveLikeTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnRemoveLikeTrue(this, null);
                        }
                        break;
                    }

                    case 13:
                    {
                        if (OnUserLevelsTrue != null)
                        {
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnUserLevelsTrue(this, null);
                        }
                        break;
                    }

                    case 14:
                    {
                        if (OnGetLevelByLinkTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetLevelByLinkTrue(this, null);
                        }
                        break;
                    }

                    case 15:
                    {
                        if (OnGetLevelByTagTrue != null)
                        {
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetLevelByTagTrue(this, null);
                        }
                        break;
                    }

                    case 16:
                    {
                        if (OnGetUserByFirstNameSymbolsTrue != null)
                        {
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetUserByFirstNameSymbolsTrue(this, null);
                        }
                        break;
                    }

                    case 17:
                    {
                        if (OnFaceBookSyncTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnFaceBookSyncTrue(this, null);
                        }
                        break;
                    }

                    case 18:
                    {
                        if (OnGetLevelByIdTrue != null)
                        {
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetLevelByIdTrue(this, null);
                        }
                        break;
                    }

                    case 19:
                    {
                        if (OnFollowUserTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnFollowUserTrue(this, null);
                        }
                        break;
                    }

                    case 20:
                    {
                        if (OnUnFollowUserTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnUnFollowUserTrue(this, null);
                        }
                        break;
                    }

                    case 21:
                    {
                        if (OnGetUserInfoTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetUserInfoTrue(this, null);
                        }
                        break;
                    }

                    case 22:
                    {
                        if (OnGetFollowedAndFollowersTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetFollowedAndFollowersTrue(this, null);
                        }
                        break;
                    }

                    case 23:
                    {
                        if (OnNewLevelCreated != null)
                        {
                            try
                            {
                                WebResponseString =
                                    new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                                OnNewLevelCreated(this, null);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        break;
                    }

                    case 24:
                    {
                        if (OnFollowedFbUsersTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnFollowedFbUsersTrue(this, null);
                        }
                        break;
                    }
                    case 25:
                    {
                        if (OnGetLevelsThatUserLikeTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetLevelsThatUserLikeTrue(this, null);
                        }
                        break;
                    }

                    case 26:
                    {
                        if (OnGetUserImageTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetUserImageTrue(this, null);
                        }
                        break;
                    }

                    case 27:
                    {
                        if (OnGetLevelImageTrue != null)
                        {
                            GameObject.Find("RequestSendHandler").GetComponent<RequestSendHandler>().Connecting.SetActive(false);
                            WebResponseString =
                                new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                            OnGetLevelImageTrue(this, null);
                        }
                        break;
                    }

                    default:
                    {
                        Debug.Log("не обработанный запрос");
                        WebResponseString =
                            new StreamReader(requestState.webResponse.GetResponseStream()).ReadToEnd();
                        Debug.LogWarning(WebResponseString);
                        break;
                    }
                }
            }
        }

        isResponseCompleted = true;
    }

    private void RespCallback(IAsyncResult asyncResult)
    {
        WebRequest webRequest = requestState.webRequest;

        try
        {
            requestState.webResponse = webRequest.EndGetResponse(asyncResult);
        }
        catch (WebException webException)
        {
            requestState.errorMessage = "From callback, " + webException.Message;
        }
    }

    public void ScanTimeoutCallback(object state, bool timedOut)
    {
        if (timedOut)
        {
            var requestState = (RequestState) state;
            if (requestState != null)
                requestState.webRequest.Abort();
        }
        else
        {
            var registeredWaitHandle = (RegisteredWaitHandle) state;
            if (registeredWaitHandle != null)
                registeredWaitHandle.Unregister(null);
        }
    }

    public void AbortActiveRequest()
    {
        if (requestState != null)
            requestState.webRequest.Abort();
    }
}

public class ForgotResponse
{
    public int code;
    public string message;
}

public class Index
{
    public static int SublingIndex;
}