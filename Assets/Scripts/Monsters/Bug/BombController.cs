﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BombController : MonoBehaviour
{
	public SpriteRenderer SelfImage;
	public GameObject ExplosionEffect;
	public GameObject ExplosionArea;


	[Header ("SerializeFields")]
	[SerializeField] private Sprite ActiveBombSprite;
	[SerializeField] private Sprite InActiveBombSprite;
	[SerializeField] private BombSoundHandler _bombSoundHandler;

    void OnCollisionEnter2D(Collision2D coll)
	{
		StartCoroutine (Explode());
	}

	IEnumerator Explode() 
	{
        yield return new WaitForSeconds(0.3f);
        _bombSoundHandler.Beep();
        SelfImage.sprite = ActiveBombSprite;
		yield return new WaitForSeconds(0.2f);
		SelfImage.sprite = InActiveBombSprite;
		yield return new WaitForSeconds(0.1f);
		SelfImage.sprite = ActiveBombSprite;
		yield return new WaitForSeconds(0.05f);
		SelfImage.sprite = InActiveBombSprite;
		yield return new WaitForSeconds(0.03f);
		SelfImage.sprite = ActiveBombSprite;
		yield return new WaitForSeconds(0.02f);

		Destroy (this.gameObject, 0.2f);
		ExplosionArea.GetComponent<BoxCollider2D> ().enabled = true;
		GameObject _explosionEffect = Instantiate (ExplosionEffect) as GameObject;

		_explosionEffect.transform.position = this.gameObject.transform.position;
	}
}