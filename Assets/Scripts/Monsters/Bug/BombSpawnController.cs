﻿using UnityEngine;

public class BombSpawnController : MonoBehaviour
{
	public GameObject bombPrefab;
	int counter = 0;
	public int timeInterval = 120;

	void FixedUpdate()
	{
		if (counter >= timeInterval)
		{
			counter = 0;
			GameObject bomb = Instantiate(bombPrefab);
			bomb.transform.SetParent (null);
			bomb.transform.position = transform.position;
		}
		counter++;
	}
}