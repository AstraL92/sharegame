﻿using UnityEngine;

public class BugController : MonoBehaviour
{
	public Transform BugTransform;
	[Range (1, 20)] public int BugSpeed;

	private int _levelLenght;

	void Start()
	{
		/*
		if (WinPopUp.IsLevelFronFeed) 
			_levelLenght = JsonUtility.FromJson<LevelConfig>(LevelConstriuctorEnterPoint.CurrentLevelFromFeed.GetConfig()).RowCount;
		else
			_levelLenght = JsonUtility.FromJson<LevelConfig>(LevelConstriuctorEnterPoint.CurrentLevel.GetConfig()).RowCount;

		Debug.Log ("Level lenght : " + _levelLenght);*/
	}

	void FixedUpdate()
	{
		Move ();
	}

	private void Move()
	{
		/*if ((BugTransform.position.x > _levelLenght + 5) || (BugTransform.position.x < -5)) 
		{
			BugTransform.localScale = new Vector3 (BugTransform.localScale.x * -1, BugTransform.localScale.y, BugTransform.localScale.z);
		}*/
		BugTransform.Translate(Vector3.left * Time.deltaTime * BugSpeed);
	}
}