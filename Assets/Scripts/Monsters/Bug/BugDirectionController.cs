﻿using UnityEngine;

public class BugDirectionController : MonoBehaviour
{
	public GameObject BugBody;
	public LayerMask LM;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (!col.isTrigger) 
		{
			BugBody.transform.localScale = new Vector3 (BugBody.transform.localScale.x * -1, BugBody.transform.localScale.y, BugBody.transform.localScale.z);
		}
	}
}