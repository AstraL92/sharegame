﻿using UnityEngine;
using platformer.common;

public class GlitchCotroller : MonoBehaviour
{
	[SerializeField] [Range (0f,0.1f)] private float _speed;
	
	[SerializeField] private bool _isMoveLeft = true;

	public GameObject GlitchSprite;

	public LayerMask LMask;

	[SerializeField]private float _startPosLineCast = 0.55f;
	[SerializeField]private float _endPosLineCast = 0.6f;

	void FixedUpdate()
	{
		// refactor
		// Ground check
//		if (!Physics2D.Linecast (new Vector3 (transform.position.x, transform.position.y - 0.55f, transform.position.z), new Vector3 (transform.position.x, transform.position.y - 0.6f, transform.position.z), LMask))
//			return;
//
//		if (!Physics2D.Linecast (new Vector3 (transform.position.x - _startPosLineCast, transform.position.y - 0.6f, transform.position.z), 
//			new Vector3 (transform.position.x - _startPosLineCast, transform.position.y - 0.6f, transform.position.z), LMask)) 
//		{
//			_startPosLineCast = _startPosLineCast * -1;
//			_endPosLineCast = _endPosLineCast * -1;
//			_isMoveLeft = !_isMoveLeft;
//			GlitchSprite.transform.localScale = new Vector3 (GlitchSprite.transform.localScale.x * -1f, GlitchSprite.transform.localScale.y, GlitchSprite.transform.localScale.z);
//			return;
//		}

		WallCheck ();
		CheckMoveVector ();
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector3(transform.position.x - _startPosLineCast, transform.position.y - 0.2f, transform.position.z),
            new Vector3(transform.position.x - _endPosLineCast, transform.position.y - 0.2f, transform.position.z));
    }

    void WallCheck ()
	{
		if (Physics2D.Linecast (new Vector3 (transform.position.x - _startPosLineCast, transform.position.y, transform.position.z), 
			new Vector3 (transform.position.x - _endPosLineCast, transform.position.y, transform.position.z), LMask))
        {
            SwitchGlitchScale();
        }
    }

    public void SwitchGlitchScale()
    {
        _startPosLineCast = _startPosLineCast * -1;
        _endPosLineCast = _endPosLineCast * -1;
        _isMoveLeft = !_isMoveLeft;
        GlitchSprite.transform.localScale = new Vector3(GlitchSprite.transform.localScale.x * -1f, GlitchSprite.transform.localScale.y, GlitchSprite.transform.localScale.z);
    }

    void CheckMoveVector ()
	{
		if (_isMoveLeft) 
		{
			Move (1);
		}
		else 
		{
			Move (-1);
		}
	}

	void Move (int i)
	{
		gameObject.transform.Translate (i * (Vector3.left * _speed));
	}
}