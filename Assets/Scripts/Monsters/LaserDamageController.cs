﻿using UnityEngine;
using System.Collections;
using platformer.common.api;

public class LaserDamageController : MonoBehaviour
{
    [SerializeField]
    private int _damage = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            IDamageReviever affectedObject = collision.GetComponent<IDamageReviever>();

            if (affectedObject != null)
            {
                affectedObject.OnDamage(_damage);
            }
        }
    }

}
