﻿using System.Collections;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public bool CheckGround;
    public bool EnableShooting;
    public GameObject GlitchSprite;
    public LayerMask LMask;
    public float TimeLeft = 3f;

    [SerializeField] private float _startPosLineCast = 0.55f;
    [SerializeField] private float _endPosLineCast = 0.6f;
    [SerializeField] private bool _isMoveLeft = true;
    [SerializeField] [Range(0f, 0.3f)] private float _speed;

    private bool _enableMoving;

    private void Awake()
    {
        if (EnableShooting)
        {
            StartCoroutine(Timer());
        }
    }

private void FixedUpdate()
    {
        // refactor
        // Ground check
        if (CheckGround)
        {
            if (
                !Physics2D.Linecast(
                    new Vector3(transform.position.x, transform.position.y - 0.55f, transform.position.z),
                    new Vector3(transform.position.x, transform.position.y - 0.6f, transform.position.z), LMask))
                return;

            if (
                !Physics2D.Linecast(
                    new Vector3(transform.position.x - _startPosLineCast, transform.position.y - 0.6f,
                        transform.position.z),
                    new Vector3(transform.position.x - _startPosLineCast, transform.position.y - 0.6f,
                        transform.position.z),
                    LMask))
            {
                _startPosLineCast = _startPosLineCast * -1;
                _endPosLineCast = _endPosLineCast * -1;
                _isMoveLeft = !_isMoveLeft;
                GlitchSprite.transform.localScale = new Vector3(GlitchSprite.transform.localScale.x * -1f,
                    GlitchSprite.transform.localScale.y, GlitchSprite.transform.localScale.z);
                return;
            }
        }

        WallCheck();
        CheckMoveVector();
       
    }

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(TimeLeft);
        _enableMoving = true;
        gameObject.GetComponent<Animator>().SetTrigger("Shoot");
        yield return new WaitForSeconds(2f);
        _enableMoving = false;
        gameObject.GetComponent<Animator>().SetTrigger("Move");
        StartCoroutine(Timer());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(
            new Vector3(transform.position.x - _startPosLineCast, transform.position.y - 0.2f, transform.position.z),
            new Vector3(transform.position.x - _endPosLineCast, transform.position.y - 0.2f, transform.position.z));
    }

    private void WallCheck()
    {
        if (
            Physics2D.Linecast(
                new Vector3(transform.position.x - _startPosLineCast, transform.position.y - 0.2f, transform.position.z),
                new Vector3(transform.position.x - _endPosLineCast, transform.position.y - 0.2f, transform.position.z), LMask))
        {
            SwitchScale();
        }
    }

    public void SwitchScale()
    {
        _startPosLineCast = _startPosLineCast*-1;
        _endPosLineCast = _endPosLineCast*-1;
        _isMoveLeft = !_isMoveLeft;
        GlitchSprite.transform.localScale = new Vector3(GlitchSprite.transform.localScale.x*-1f,
            GlitchSprite.transform.localScale.y, GlitchSprite.transform.localScale.z);
    }

    private void CheckMoveVector()
    {
        if (_enableMoving)
        {
            return;
        }

        if (_isMoveLeft)
        {
            Move(1);
        }
        else
        {
            Move(-1);
        }
    }

    private void Move(int i)
    {
        gameObject.transform.Translate(i*(Vector3.left*_speed));
    }
}