﻿using platformer.common.api;
using UnityEngine;

public class TrojaniumController : MonoBehaviour
{
    [SerializeField]
    private GameObject _body;

    [SerializeField]
    private LayerMask _lm;

    private TrojaniumJumpController _tjc;
    private Rigidbody2D _rb2d;
    private bool _isLeftJump = false;

    private void Start()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _tjc = GetComponent<TrojaniumJumpController>();
    }

    private void FixedUpdate()
    {
        if (Physics2D.Linecast(new Vector3(transform.position.x - 0.3f, transform.position.y, transform.position.z),
                   new Vector3(transform.position.x - 0.4f, transform.position.y, transform.position.z), _lm))
        {
            _body.transform.localScale = new Vector3(1f, 1f, 1f);
            _rb2d.velocity = new Vector2(-_rb2d.velocity.x, _rb2d.velocity.y);
            _isLeftJump = false;
        }

        if (Physics2D.Linecast(new Vector3(transform.position.x + 0.4f, transform.position.y, transform.position.z),
            new Vector3(transform.position.x + 0.4f, transform.position.y, transform.position.z), _lm))
        {
            _body.transform.localScale = new Vector3(-1f, 1f, 1f);
            _rb2d.velocity = new Vector2(-_rb2d.velocity.x, _rb2d.velocity.y);
            _isLeftJump = true;
        }

        //horse jump once in 3 sec
        if ((Time.time % 3) == 0)
        {
            if (Physics2D.Linecast (new Vector3(transform.position.x - 0.2f, transform.position.y - 0.6f, transform.position.z),
            new Vector3(transform.position.x + 0.2f, transform.position.y - 0.6f, transform.position.z), _lm))
            {
                _tjc.Jump(_isLeftJump);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            IDamageReviever affectedObject = collision.GetComponent<IDamageReviever>();

            if (affectedObject != null)
            {
                _rb2d.velocity = new Vector2(-_rb2d.velocity.x, -_rb2d.velocity.y);
                gameObject.transform.position = new Vector3(gameObject.transform.position.x - 0.5f, gameObject.transform.position.y, gameObject.transform.position.z);
                //affectedObject.OnDamage(1);
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector3(transform.position.x - 0.2f, transform.position.y - 0.6f, transform.position.z),
            new Vector3(transform.position.x + 0.2f, transform.position.y - 0.6f, transform.position.z));

        Gizmos.DrawLine(new Vector3(transform.position.x - 0.3f, transform.position.y, transform.position.z),
            new Vector3(transform.position.x - 0.4f, transform.position.y, transform.position.z));

        Gizmos.DrawLine(new Vector3(transform.position.x + 0.3f, transform.position.y, transform.position.z),
            new Vector3(transform.position.x + 0.4f, transform.position.y, transform.position.z));
    }
}
