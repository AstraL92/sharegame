﻿using UnityEngine;

public class TrojaniumJumpController : MonoBehaviour
{
    [SerializeField]    private float _xForce = 80f;
    [SerializeField]    private float _yForce = 300f;

    private Rigidbody2D _rb2d;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _rb2d = GetComponent<Rigidbody2D>();
    }

    public void Jump(bool isLeftJump)
    {
        if (isLeftJump)
            _rb2d.AddForce(new Vector2(-_xForce, _yForce));
        else
            _rb2d.AddForce(new Vector2(_xForce, _yForce));

        _animator.SetTrigger("Jump");
    }
}
