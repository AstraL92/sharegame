﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PauseHandler : MonoBehaviour
{
    public Image EditButton;
    public Text EditButtonText;

	void OnEnable()
	{
  //      if (WinPopUp.IsLevelFronFeed)
  //      {
  //          SwitchOffEditButton();
  //      }

		//Time.timeScale = 0;
	}

    public void Pause()
    {
        if (WinPopUp.IsLevelFronFeed)
        {
            SwitchOffEditButton();
        }

        SetTimeStaleToZero();
    }

    public void SetTimeStaleToZero()
    {
        Debug.Log("timescali = 0");
        Time.timeScale = 0;
    }

    private void SwitchOffEditButton()
    {
        try
        {
            EditButton.GetComponent<Button>().enabled = false;
            EditButton.color = Color.gray;

            EditButtonText.color = Color.gray;
        }
        catch (Exception ex)
        {
            Debug.LogWarning(ex.Message);
        }
        
    }

    void OnDisable()
	{
		Time.timeScale = 1;
	}
}