﻿using UnityEngine;

public class PopUpEffect : MonoBehaviour
{
	public GameObject PopUpObject;

	[SerializeField]private float _zoomSpeed = 0.1f;
	[SerializeField]private float _finalScale = 1.0f;

    private bool _startZoom = true;

	void OnEnable()
	{
		_startZoom = true;
        _zoomSpeed = 0.15f;

        PopUpObject.transform.localScale = Vector3.zero;
	}

	void OnDisable()
	{
		PopUpObject.transform.localScale = Vector3.zero;
	}

	void Update()
	{
		if (PopUpObject.transform.localScale.x < 1.05f && _startZoom == true) 
			PopUpObject.transform.localScale = new Vector3 (PopUpObject.transform.localScale.x + _zoomSpeed, PopUpObject.transform.localScale.y + _zoomSpeed, 1);
		else
			_startZoom = false;

		if (_startZoom == false && PopUpObject.transform.localScale.x > _finalScale) 
			PopUpObject.transform.localScale = new Vector3 (PopUpObject.transform.localScale.x - _zoomSpeed / 3, PopUpObject.transform.localScale.y - _zoomSpeed / 3, 1);
	}
}