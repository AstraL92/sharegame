﻿using platformer.levelEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoaderTemp : MonoBehaviour 
{
	public void LoadGameScene()
	{
		SceneManager.LoadScene ("Game");
	}

	public void LoadLevelConstructor(bool b)
	{
        if (b)
        {
            LevelConstriuctorEnterPoint.IsEditLevel = b;
        }

		SceneManager.LoadScene ("LevelConstructor");
	}

	public void LoadMenu()
	{
		//govno
	    LevelGrid.IsScreenBlockerShow = true;

        LevelImagesLoader._bg = null;
		LevelImagesLoader._win = null;
		LevelImagesLoader._lose = null;
		
		SceneManager.LoadScene ("Menu");
	}
}
