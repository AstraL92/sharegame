﻿using UnityEngine;
using System.Collections.Generic;

public class SelectLevelSizeHandler : MonoBehaviour
{
	public int GridSize = 0;

	public List<GameObject> Buttons = new List<GameObject> ();

	public void Click(int i)
	{
		foreach (var item in Buttons) 
		{
			item.transform.localScale = Vector3.one;
		}

		Buttons [i].transform.localScale = new Vector3 (1.2f, 1.2f, 1f);
	}
}