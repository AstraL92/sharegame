﻿using UnityEngine;
using System.Collections;

public class SimpleSpriteAnimator : MonoBehaviour
{
	public SpriteRenderer SelfSpriteRenderer;
	
    public Sprite[] spriteFrames;
    public float frameRate = 24.0f;
    public bool loop;
    public bool destroyOnComplete;
    public bool playOnStart;

    private float time;
    private bool isPlaying;
    
    void Start()
    {
        if (playOnStart)
        {
            Play();
        }
    }
    
    void Update()
    {
        if (!isPlaying)
            return;

        time += Time.deltaTime*frameRate;
        if (time >= (float)spriteFrames.Length)
        {
            if (loop)
                time = _.fmod(time, (float)spriteFrames.Length);
            else
            {
                time = 0.0f;
                isPlaying = false;
                if(destroyOnComplete)
                {
                    Destroy(gameObject);
                }
            }
        }

        int frameIndex = Mathf.FloorToInt(time);

		if (SelfSpriteRenderer != null)
			SelfSpriteRenderer.sprite = spriteFrames[frameIndex];
    }

    public void Play()
    {
        if (!isPlaying)
        {
            isPlaying = true;
            time = 0.0f;
        }
    }
}
