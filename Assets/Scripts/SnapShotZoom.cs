﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SnapShotZoom : MonoBehaviour
{
	public ScrollRect GridSctollRect;
	
	[SerializeField] private float MaxSize = 1.2f;
	[SerializeField] private float MinSize = 0.78f;
	[SerializeField] private float Speed = 0.04f;
	private Vector3 _curDist;
	private Vector3 _prevDist;
	private float _touchDelta;

	private void Update()
	{
		ZoomAvatar();
	}

	private void ZoomAvatar()
	{
		GridSctollRect.enabled = Input.touchCount != 2;

		if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved &&
			Input.GetTouch(1).phase == TouchPhase.Moved)
		{

			_curDist = Input.GetTouch(0).position - Input.GetTouch(1).position;
			_prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) -
				(Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition));

			_touchDelta = _curDist.magnitude - _prevDist.magnitude;

			gameObject.transform.localScale =
				new Vector3(
					Mathf.Clamp(gameObject.transform.localScale.x + Speed * Math.Sign(_touchDelta), MinSize,
						MaxSize),
					Mathf.Clamp(gameObject.transform.localScale.y + Speed * Math.Sign(_touchDelta), MinSize,
						MaxSize), 1);
		}
	}
}