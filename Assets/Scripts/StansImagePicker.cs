﻿using UnityEngine;
using UnityEngine.UI;

public class StansImagePicker : MonoBehaviour
{
    public static int SpriteType;

    [Header ("Constructor GameObjects")]
    public GameObject ImageCropPopUpGameObject;
    public Image TargetImage;

    [Header("Images")]
    public Image Avatar;

    [Header("Avatar Obj")]
    public GameObject AvatarPanel;

    void OnEnable()
    {
        IOSCamera.OnImagePicked += OnImage;
    }

    void OnDisable()
    {
        IOSCamera.OnImagePicked -= OnImage;
    }

    private void OnImage(IOSImagePickResult obj)
    {
        if (obj.IsSucceeded)
        {
            switch ((ImageType)SpriteType)
            {
                case ImageType.Avatar:
                    Avatar.sprite = Sprite.Create(obj.Image, new Rect(0, 0, obj.Image.width, obj.Image.height), new Vector2(0.5f, 0.5f));
                    AvatarPanel.SetActive(true);
                    break;

                case ImageType.Background:
                    SendImageToCropTool(obj);
                    break;

                case ImageType.Win:
                    SendImageToCropTool(obj);
                    break;

                case ImageType.Lose:
                    SendImageToCropTool(obj);
                    break;

                default:
                    Debug.LogWarning("NoImageType");
                    break;
            }
        }
    }

    private void SendImageToCropTool(IOSImagePickResult obj)
    {
        TargetImage.sprite = Sprite.Create(obj.Image, new Rect(0, 0, obj.Image.width, obj.Image.height), new Vector2(0.5f, 0.5f));

        ImageCropPopUpGameObject.SetActive(true);
    }
}

public enum ImageType : int
{
    Avatar,
    Background,
    Win,
    Lose
}