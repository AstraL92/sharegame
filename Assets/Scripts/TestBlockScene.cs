﻿using UnityEngine;
using UnityEngine.UI;

public class TestBlockScene : MonoBehaviour
{
    public GameObject StartPopUp;
	public Text AccelerometerDegree;

    private void Start()
    {
        if (LevelConstriuctorEnterPoint.IsSnapshot || LevelConstriuctorEnterPoint.IsEditLevel)
        {
            gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
	{
		if (Input.acceleration.y < -1.0f) 
		{
            StartPopUp.SetActive(true);
            gameObject.SetActive (false);
		}
	}
}