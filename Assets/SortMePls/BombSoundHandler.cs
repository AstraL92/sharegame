﻿using UnityEngine;

public class BombSoundHandler : MonoBehaviour
{
    [SerializeField]
    private AudioSource _beepSound;

    public void Beep()
    {
        _beepSound.Play();
    }
}
