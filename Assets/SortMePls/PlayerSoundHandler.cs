﻿using UnityEngine;

public class PlayerSoundHandler : MonoBehaviour
{
    [SerializeField]
    private AudioSource _jumpSound;
    [SerializeField]
    private AudioSource _run1Sound;
    [SerializeField]
    private AudioSource _run2Sound;
    [SerializeField]
    private AudioSource _landingSound;
    [SerializeField]
    private AudioSource _killSound;

    public AudioClip cyborgStepSound1;
    public AudioClip cyborgStepSound2;
    public AudioClip stepSound1;
    public AudioClip stepSound2;

    public bool IsRun = false;
    private int lastStepSound = 2;

    public void Jump()
    {
        _jumpSound.Play();
    }

    public void Run()
    {
        IsRun = true;
    }

    public void StopRun()
    {
        IsRun = false;
    }

    public void Landing()
    {
        if (!_landingSound.isPlaying)
        {
            _landingSound.Play();
        }
    }

    public void Kill()
    {
        _killSound.Play();
    }

    private void FixedUpdate()
    {
        if (IsRun)
        {
            if (!_run1Sound.isPlaying && !_run2Sound.isPlaying)
            {
                _run1Sound.clip = lastStepSound == 2 ? stepSound1 : stepSound2;
                _run1Sound.Play();
                _run2Sound.clip = lastStepSound == 2 ? cyborgStepSound1 : cyborgStepSound2;
                _run2Sound.Play();
                lastStepSound = lastStepSound == 2 ? 1 : 2;
            }
        }
    }
}
