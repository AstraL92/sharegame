using UnityEngine;
using HedgehogTeam.EasyTouch;
using System;

public class DoubleTapMe : MonoBehaviour
{
    public static Action DoubleTap;

    private CellHandler cellHandler;

    void Start()
    {
        cellHandler = GetComponent<CellHandler>();
    }

    // Subscribe to events
    void OnEnable()
    {
		EasyTouch.On_DoubleTap += On_DoubleTap;	
	}

	void OnDisable()
    {
		UnsubscribeEvent();
	}
	
	void OnDestroy()
    {
		UnsubscribeEvent();
	}
	
	void UnsubscribeEvent()
    {
		EasyTouch.On_DoubleTap -= On_DoubleTap;	
	}	
	
	// Double tap  
	private void On_DoubleTap( Gesture gesture)
    {
        if (gesture.pickedObject == gameObject && cellHandler.CellType != CellType.Empty)
        {
            if (GridHandler.ActiveCell != null)
            {
                GridHandler.ActiveCell.GridRef.CellToolsPanel.MoveBtn.isOn = true;
            }

            GridHandler.ActiveCell = cellHandler;
            GridHandler.ActiveCell.GridRef.CellToolsPanel.DuplicateBtn.isOn = false;

            if (DoubleTap != null)
                DoubleTap.Invoke(); 
        }
    }
}
