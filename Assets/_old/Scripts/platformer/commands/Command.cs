﻿using UnityEngine;

namespace platformer.commands
{
	public class Command : MonoBehaviour 
	{
		private object[] _args;

		public static T ExecuteOn<T>(GameObject target, params object[] args)
			where T : Command
		{
			T result = target.AddComponent <T>();
			result._args = args;
			return result;
		}

		private void Start()
		{
			OnStart (_args);
		}

		private void Update()
		{
			OnUpdate ();
		}

		private void FixedUpdate()
		{
			OnFixedUpdate ();
		}

		protected virtual void OnStart(object[] args)
		{}

		protected virtual void OnUpdate()
		{}

		protected virtual void OnFixedUpdate()
		{}
	}
}
