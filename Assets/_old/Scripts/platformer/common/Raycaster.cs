﻿using UnityEngine;
using platformer.domain;
using System;

namespace platformer.common
{
    public class Raycaster : MonoBehaviour
    {
        public const float kSkinWidth = .015f;

        public LayerMask CollisionMask;

        public int HorRayCount = 4;
        public int VertRayCount = 4;

        [HideInInspector] public float HorRaySpacing;
        [HideInInspector] public float VertRaySpacing;
        [HideInInspector] public RaycastOrigins Origins;

        private BoxCollider2D _collider;

        void OnEnable()
        {
            _collider = GetComponent<BoxCollider2D>();

            CalculateSpacing();
        }

		public void UpdateRaycastOrigins ( )
		{
			Bounds bounds = _collider.bounds;
			bounds.Expand (kSkinWidth * -2);

			Origins.bottomLeft = new Vector2 (bounds.min.x, bounds.min.y);
			Origins.bottomRight = new Vector2 (bounds.max.x, bounds.min.y);
			Origins.topLeft = new Vector2 (bounds.min.x, bounds.max.y);
			Origins.topRight = new Vector2 (bounds.max.x, bounds.max.y);
		}

		public void CalculateSpacing ()
		{
			Bounds bounds = _collider.bounds;
			bounds.Expand (kSkinWidth * -2);

			HorRayCount = Mathf.Clamp (HorRayCount, 2, int.MaxValue);
			VertRayCount = Mathf.Clamp (VertRayCount, 2, int.MaxValue);

			HorRaySpacing = bounds.size.y / (HorRayCount - 1);
			VertRaySpacing = bounds.size.x / (VertRayCount - 1);
		}
	}
}
