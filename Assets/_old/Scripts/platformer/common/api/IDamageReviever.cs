﻿namespace platformer.common.api
{
	public interface IDamageReviever 
	{
		void OnDamage ( int damage );
	}
}