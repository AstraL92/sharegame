﻿using UnityEngine;
using System.Collections;

namespace platformer.common.api
{
	public interface IGameEventsHandler
	{
		void OnGamePaused ( bool pause );
		void OnGameRestarted ();
	}
}