﻿using UnityEngine;

namespace platformer.common.api
{
	public interface IVelocityImpact 
	{
		void ImpactVelocity (Vector3 velocity);
	}
}
