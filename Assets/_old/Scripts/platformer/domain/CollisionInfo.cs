﻿using UnityEngine;

namespace platformer.domain
{
	public struct CollisionInfo 
	{
		public bool IsAbove;
		public bool IsBelow;
		public bool IsLeft;
		public bool IsRight;

		public void Reset ()
		{
			IsAbove = IsBelow = IsLeft = IsRight = false;
		}
	}
}
