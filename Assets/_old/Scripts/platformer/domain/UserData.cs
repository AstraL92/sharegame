﻿using platformer.domain.level;
using UnityEngine;
using System;

namespace platformer.domain
{
	public class UserData 
	{
		public Guid id;
		public Int32 RegistrationDate; // unix format
		public string Name;
	    	    
        public string AvatarURL;
		public Guid[] CreatedLevelsIds;

        public int Level;
        public int Coins;
        public int Сrystals;

        public UserData ()
		{
		}

		public static UserData GetMockObject ()
		{
			UserData mock = new UserData ();
			mock.id = Guid.NewGuid ();
			mock.RegistrationDate = 145264;
			mock.Name = "Adam Smith";
			mock.AvatarURL = "https://dl.dropboxusercontent.com/u/6205925/vr_av.jpg";
			mock.CreatedLevelsIds = new Guid[1]{ Guid.Empty };
		    mock.Level = 1;
		    mock.Coins = 32;
		    mock.Сrystals = 54;

            return mock;
		}
	}
}
