﻿using UnityEngine;
using System;

namespace platformer.domain.level
{
	public class CommentData  
	{
		public Guid AuthorId;
		public Int32 CreationDate; // unix format

		public string Text;

		public CommentData ()
		{
		}
	}
}