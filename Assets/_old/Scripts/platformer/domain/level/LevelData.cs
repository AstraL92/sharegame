﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Text;

namespace platformer.domain.level
{
	public class LevelData 
	{
		public Guid id;
		public string Name; // unused by design
		public string Description;

		public Int32 TimeLimit;
		public LevelType Type = LevelType.None;

		public Int32 CreationDate; 
		public Guid AuthorId;
		public List<CommentData> Comments;

		public LevelConfig Config;
		public Guid[] likedUsersIds;

		public string LevelPreviewURL;

		public LevelData ()
		{
			
		}

		public static LevelData GetMockObject ()
		{
			LevelData mock = new LevelData ();
			mock.id = Guid.NewGuid ();
			mock.Name = "Test level 001";
			mock.Description = "can you beat this level?";
			mock.TimeLimit = 500;
			mock.Type = LevelType.Public;
			mock.CreationDate = 74422522;
			mock.AuthorId = Guid.Empty;
			mock.Comments = new List<CommentData> ();
			mock.LevelPreviewURL = "https://dl.dropboxusercontent.com/u/6205925/sharegame/lvlPreviewTest.png";
			mock.Config = null;
			mock.likedUsersIds = new Guid[]{ Guid.Empty, Guid.Empty };

			return mock;
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder ();
			sb.AppendFormat ("Id: {0}\n", id);
			sb.AppendFormat ("Name: {0}\n", Name);
			sb.AppendFormat ("Time Limit: {0}\n", TimeLimit);
			sb.AppendFormat ("Type: {0}\n", Type);
			sb.AppendFormat ("Creation Date: {0}\n", CreationDate);
			sb.AppendFormat ("Author id: {0}\n", AuthorId);

			return sb.ToString ();
		}
	}
}
