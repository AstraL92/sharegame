﻿namespace platformer.domain.level
{
	public enum LevelType : byte
	{
		None = 0,
		Private = 1,
		Public = 2
	}
}
