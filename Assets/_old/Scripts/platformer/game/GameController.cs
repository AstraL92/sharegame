﻿using UnityEngine;
using System;

namespace platformer.game
{
	[RequireComponent(typeof(GameView))]
	public class GameController : MonoBehaviour 
	{
		private GameView _view;
		private GameModel _model;

		public static event Action OnGameRestarted; //TODO: call methods only in observers?
		public static event Action<bool> OnGamePaused;

		void Awake ()
		{
			_model = new GameModel ();
			_view = this.GetComponent<GameView> ();
		}

		void Start ()
		{
			LevelStart ();
		}
			
		void OnEnable ()
		{
			//_view.Player.OnPlayerDied += OnPlayerDiedEvent;
		}

		void OnDisable ()
		{
//			if (_view == null)
//				Debug.Log ("sfdgf");
//
//			if (_view.Player == null)
//				Debug.Log ("1111");

			//_view.Player.OnPlayerDied -= OnPlayerDiedEvent;
		}

		public void LevelStart ()
		{
			_view.GameUIController.StartGameTimer (_model.Level.TimeLimit);
		}

		public void LevelRestart ()
		{
			if (OnGameRestarted != null)
				OnGameRestarted ();

			_view.GameUIController.RestartGameTimer ();
		}

		public void PauseGame ()
		{
			if (OnGamePaused != null)
				OnGamePaused (true);

			_view.PauseController.ShowPause ();
			_view.GameUIController.PauseGameTimer ();
		}

		public void ResumeGame ()
		{
			if (OnGamePaused != null)
				OnGamePaused (false);

			_view.GameUIController.ResumeGameTimer ();
		}

		public void OnGameTimerCompleted ()
		{
			//_view.Player.OnDie ();
		}

		private void OnPlayerDiedEvent ()
		{
			LevelRestart ();
		}
	}
}
