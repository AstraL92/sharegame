﻿using UnityEngine;
using platformer.domain.level;

namespace platformer.game
{
	public class GameModel 
	{
		private LevelData _level;
		public LevelData Level
		{
			get { return _level; }
		}

		public GameModel ()
		{
			_level = LevelData.GetMockObject ();
		}
	}
}
