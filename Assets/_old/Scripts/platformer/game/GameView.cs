﻿using UnityEngine;
using System.Collections.Generic;
using platformer.levelObjects;
using platformer.game.pause;
using platformer.player;
using platformer.game.gameui;

namespace platformer.game
{
	[RequireComponent(typeof(GameController))]
	public class GameView : MonoBehaviour 
	{
		private PlayerController _player;
		public PlayerController Player
		{
			get 
			{ 
				if (_player == null)
					_player = FindObjectOfType<PlayerController> ();
				
				return _player; 
			}
		}

		private PauseController _pauseController;
		public PauseController PauseController
		{
			get 
			{ 
				if (_pauseController == null)
					_pauseController = FindObjectOfType<PauseController>();
				
				return _pauseController; 
			}
		}

		private GameUIController _gameUIController;
		public GameUIController GameUIController
		{
			get 
			{ 
				if (_gameUIController == null)
					_gameUIController = FindObjectOfType<GameUIController> ();
				
				return _gameUIController; 
			}
		}

		private GameController _controller;

		private UnityCloudBuildManifest _manifest;

		void Awake ()
		{
		}

		void Start ()
		{
			_controller = this.GetComponent<GameController> ();

			_manifest = UnityCloudBuildManifest.Load ();
		}

		void OnGUI ()
		{
			if (_manifest != null)
			{
				GUI.Label(new Rect(Screen.width - 100, 0, 100, 20), string.Format("Build #: {0}", _manifest.buildNumber));
			}
		}

		public void OnBtnPauseClick ()
		{
			_controller.PauseGame ();
		}
	}
}