﻿using UnityEngine;
using System.Collections;

namespace platformer.game.gameui
{
	[RequireComponent(typeof(GameUIView))]
	public class GameUIController : MonoBehaviour
	{
		private GameUIView _view;
		private GameController _gameController;

		void Start()
		{
			_view = this.GetComponent<GameUIView> ();

			_gameController = FindObjectOfType<GameController> ();
			if (_gameController == null)
				Debug.LogError ("GameController can't be found on scene");

		}

		public void StartGameTimer ( int seconds )
		{
			//_view.ShowGameTimer ();
			//_view.StartGameTimer (seconds);
		}

		public void RestartGameTimer ()
		{
			_view.RestartGameTimer ();
		}

		public void PauseGameTimer ()
		{
			_view.PauseGameTimer (true);
		}

		public void ResumeGameTimer ()
		{
			_view.PauseGameTimer (false);
		}

		public void StopGameTimer ()
		{
			_view.StopGameTimer ();
			_view.HideGameTimer ();
		}

		public void OnUIGameTimerComplete ()
		{
			_gameController.OnGameTimerCompleted ();
		}
	}
}