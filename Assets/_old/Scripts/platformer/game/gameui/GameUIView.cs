﻿using UnityEngine;
using System.Collections;

namespace platformer.game.gameui
{
	[RequireComponent(typeof(GameUIController))]
	public class GameUIView : MonoBehaviour
	{
		[SerializeField] private GameObject _topLeftPanel;
		[SerializeField] private GameObject _topPanel;
		[SerializeField] private UIGameTimer _gameTimer;

		private GameUIController _controller;

		void Start ()
		{
			_controller = this.GetComponent<GameUIController> ();
		}

		public void ShowGameTimer ()
		{
			_topPanel.gameObject.SetActive (true);
		}

		public void HideGameTimer ()
		{
			_topPanel.gameObject.SetActive (false);
		}

		public void StartGameTimer ( int seconds )
		{
			_gameTimer.OnStart (seconds);
			_gameTimer.OnTimerCompleteEvent += OnTimerCompleteEventHandler;
		}

		public void RestartGameTimer ()
		{
			_gameTimer.OnReset ();
		}

		public void PauseGameTimer ( bool pause )
		{
			//_gameTimer.OnPause (pause);
		}

		public void StopGameTimer ()
		{
			_gameTimer.OnTimerCompleteEvent -= OnTimerCompleteEventHandler;
		}

		void OnTimerCompleteEventHandler ()
		{
			_controller.OnUIGameTimerComplete ();
		}

	}
}