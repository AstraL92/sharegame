﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;
using platformer.utils;

namespace platformer.game.gameui
{
	public class UIGameTimer : MonoBehaviour
	{
		[SerializeField] private Text _lblTimer;

		private int _timeOriginal;
		private Tweener _timerTween;

		private const string _normalColor = "<color=#323232FF>";
		private const string _warningColor = "<color=#ff0000ff>";

		public event Action OnTimerCompleteEvent;

		public void OnStart ( int timeLimitInSeconds )
		{
			_timeOriginal = timeLimitInSeconds;

			_lblTimer.text = _timeOriginal.ToString();

			StartTimerTween ();
		}

		public void OnPause ( bool pause )
		{
			if (pause)
				_timerTween.Pause ();
			else
				_timerTween.Play ();
		}

		public void OnReset ()
		{
			_timerTween.Kill ();

			OnStart (_timeOriginal);
		}

		private void StartTimerTween ()
		{
			_timerTween = DOVirtual.Float ((float)_timeOriginal, 0, _timeOriginal, UpdateUI);
			_timerTween.SetEase (Ease.Linear);
			_timerTween.OnComplete (OnTimerComplete);
		}

		private void UpdateUI ( float timerValue )
		{
			string timeLeft = Convert.ToInt32(timerValue).ToString();

			_lblTimer.text = String.Format ("Time left: {0}{1}</color>", timerValue > 5f ? _normalColor : _warningColor, timeLeft);
		}

		private void OnTimerComplete ()
		{
			if (OnTimerCompleteEvent != null)
				OnTimerCompleteEvent ();
		}
	}
}