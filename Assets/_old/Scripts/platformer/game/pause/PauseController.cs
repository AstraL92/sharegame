﻿using UnityEngine;
using System.Collections;
using System;

namespace platformer.game.pause
{
	[RequireComponent(typeof(PauseView))]
	public class PauseController : MonoBehaviour
	{
		private PauseView _view;
		private GameController _gameController;

		public static event Action<bool> OnGamePaused; //TODO: change to signal

		void Awake ()
		{
			_gameController = FindObjectOfType<GameController> ();
			if (_gameController == null)
				Debug.LogError ("GameController can't be found on scene");
		}

		void Start()
		{
			_view = this.GetComponent<PauseView> ();
		}

		public void ShowPause ()
		{
			_view.ShowPause ();

			if (OnGamePaused != null)
				OnGamePaused (true);
		}

		public void ResumeGame ()
		{
			_view.HidePause ();
			_gameController.ResumeGame ();

			if (OnGamePaused != null)
				OnGamePaused (false);
		}
	}
}