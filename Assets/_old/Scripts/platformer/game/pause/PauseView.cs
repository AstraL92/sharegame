﻿using UnityEngine;
using System.Collections;

namespace platformer.game.pause
{
	[RequireComponent(typeof(PauseController))]
	public class PauseView : MonoBehaviour
	{
		[SerializeField] private GameObject _panelPause;

		private PauseController _controller;

		void Start ()
		{
			_controller = this.GetComponent<PauseController> ();
		}

		public void ShowPause()
		{
			_panelPause.SetActive (true);
		}

		public void HidePause ()
		{
			_panelPause.SetActive (false);
		}

		public void OnBtnCloseClick()
		{
			_controller.ResumeGame ();
		}

		public void OnBtnResumeClick ()
		{
			_controller.ResumeGame ();
		}

		public void OnToggleGenerateGridChanged ( bool value )
		{
			platformer.levelEditor.LevelEditorController gridController = FindObjectOfType<platformer.levelEditor.LevelEditorController> ();

			if (value)
				gridController.ShowGrid ();
			else
				gridController.HideGrid ();
		}
	}
}