﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

namespace platformer.game.userRegistration
{
	[RequireComponent(typeof(UserRegistrationView))]
	public class UserRegistrationController : MonoBehaviour
	{
		//private UserRegistrationView _view;
		private GameController _gameController;

		void Awake ()
		{
			_gameController = FindObjectOfType<GameController> ();
			if (_gameController == null)
				Debug.LogError ("GameController can't be found on scene");
		}

		void Start()
		{
			//_view = this.GetComponent<UserRegistrationView> ();
		}

		public void ValidateNickname ( string nicknameToValidate )
		{
			string pattern = @"^[^0-9]\w+$";

			if (Regex.IsMatch (nicknameToValidate, pattern))
			{
				Debug.Log ("match");
			} 
			else
			{
				Debug.Log ("dont match");
			}
		}

	}
}