﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace platformer.game.userRegistration
{
	[RequireComponent(typeof(UserRegistrationController))]
	public class UserRegistrationView : MonoBehaviour
	{
		[SerializeField] private GameObject _panelRegistration;
		[SerializeField] private InputField _fieldNickname;


		private UserRegistrationController _controller;

		void Start ()
		{
			_controller = this.GetComponent<UserRegistrationController> ();
		}

		public void OnNicknameEndEditEvent (string currentText)
		{
			//temp do nothing
		}

		public void OnNicknameValueChangedEvent (string currentText)
		{
			//temp do nothing
		}

		public void OnBtnRegisterUserClick ()
		{
			_controller.ValidateNickname (_fieldNickname.text);
		}


	}
}