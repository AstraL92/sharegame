﻿using UnityEngine;
using System.Collections;

namespace platformer.levelEditor
{
	public class GridTileController : MonoBehaviour
	{
		[SerializeField] private GameObject _prefab;
		[SerializeField] private BoxCollider2D _collider;
		[SerializeField] private Material _material;

		[SerializeField] private GameObject _attachedObject;

		void Start ()
		{
		}

		public void SetObject ( GameObject obj )
		{
			_attachedObject = obj;
		}

		public GameObject PickObject ()
		{
			GameObject obj = _attachedObject;
			_attachedObject = null;

			return obj;
		}

		public bool IsEmpty ()
		{
			return _attachedObject == null;
		}

		public void OnHover ( Transform withObject )
		{
		}
	}
}