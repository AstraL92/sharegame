﻿using UnityEngine;
using platformer.domain.level;
using platformer.domain;
using platformer.utils;
using System;

namespace platformer.levelEditor
{
	[RequireComponent(typeof(LevelEditorView))]
	public class LevelEditorController : MonoBehaviour 
	{
		private LevelEditorView _view;
		private LevelEditorModel _model;

		void Start()
		{
			//_model = new LevelEditorModel ();
			//_view = this.GetComponent<LevelEditorView> ();

			//_view.ScrollController.FillScrollWithObjects ( _model.AvailableObjects );

			//_view.PanelCreateLevel.Show ();
		}

		public void CreateLevel ( string name, int timeLimit, int type )
		{
			LevelData data = new LevelData ();
			data.id = Guid.NewGuid ();

			data.Name = name;
			data.TimeLimit = timeLimit;
			data.Type = (LevelType)type;
			data.CreationDate = DateTimeUtils.ConvertToUnixTime (DateTime.UtcNow);

			data.AuthorId = Guid.NewGuid (); //TODO: change to *.currentUser.id or smth like that

			Debug.Log (data.ToString ());
		}

		public void ShowGrid ()
		{
			_view.Grid.Show ();
		}

		public void HideGrid ()
		{
			_view.Grid.Hide ();
		}
	}
}
