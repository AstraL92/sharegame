﻿using UnityEngine;
using System.Collections.Generic;
using platformer.levelEditor.domain;

namespace platformer.levelEditor
{
	public class LevelEditorModel 
	{
		private GridObject[] _availableObjects;
		public GridObject[] AvailableObjects
		{
			get { return _availableObjects; }
		}

		private const string kGridObjectsPath = "Prefabs/LevelObjects/";

		public LevelEditorModel ()
		{
			_availableObjects = new GridObject[5];
			_availableObjects[0] = new GridObject( GridObjectType.BlockStandart, Resources.Load<GameObject>( string.Format("{0}BlockStandart", kGridObjectsPath )));
			_availableObjects[1] = new GridObject( GridObjectType.MovablePlatform, Resources.Load<GameObject>( string.Format("{0}Movable Platform", kGridObjectsPath )));
			_availableObjects[2] = new GridObject( GridObjectType.BlockJumper, Resources.Load<GameObject>( string.Format("{0}Block Jumper", kGridObjectsPath )));
			_availableObjects[3] = new GridObject( GridObjectType.Checkpoint, Resources.Load<GameObject>( string.Format("{0}Checkpoint", kGridObjectsPath )));
			_availableObjects[4] = new GridObject( GridObjectType.LevelExit, Resources.Load<GameObject>( string.Format("{0}Level Exit", kGridObjectsPath )));
		}

	}
}
