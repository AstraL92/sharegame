﻿using UnityEngine;
using HedgehogTeam.EasyTouch;
using platformer.levelEditor.ui;

namespace platformer.levelEditor
{
	[RequireComponent(typeof(LevelEditorController))]
	public class LevelEditorView : MonoBehaviour 
	{
		private LevelGrid _grid;
		public LevelGrid Grid
		{
			get { return _grid; }
		}

		private LevelEditorScrollController _scrollController;
		public LevelEditorScrollController ScrollController
		{
			get { return _scrollController; }
		}

		private UIPanelCreateLevel _panelCreateLevel;
		public UIPanelCreateLevel PanelCreateLevel
		{
			get { return _panelCreateLevel; }
		}

		//private LevelEditorController _controller;

		void Awake ()
		{
			_grid = FindObjectOfType<LevelGrid> ();

			_scrollController = FindObjectOfType<LevelEditorScrollController> ();
			_panelCreateLevel = FindObjectOfType<UIPanelCreateLevel> ();
		}

		void Start ()
		{
			//_controller = this.GetComponent<LevelEditorController> ();
		}

		public void ShowPanelCreateLevel ()
		{
		}

		public void fff ()
		{
		}
	}
}
