﻿using Assets.Scripts.MenuControllers.CommentsController;
using UnityEngine;
using platformer.levelEditor.domain;
using platformer.utils;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.MenuControllers.Login;
using DG.Tweening;

namespace platformer.levelEditor
{
	public class LevelGrid : MonoBehaviour 
	{
        public GameObject ScreenCheker;

		[Header ("Options")]
		public RequestSendHandler RequestSendHandler;
        public static int PlayedType;

		[SerializeField] [Range(10, 100)] private int _horTileCount = 10;
		[SerializeField] [Range(10, 100)] private int _vertTileCount = 10;
		[SerializeField] [Range(0, 60)] private int _generationTimeoutFrames;

        [Serializable]
        public struct GameObjType
        {
            public CellType type;
            public GameObject obj;
        }

        public List<GameObjType> objectsType;

        private Dictionary<CellType, GameObject> objType;

		private GridTile[,] grid;

		private int _tileWidth = 1;
		private int _tileHeight = 1;
		private Vector3 _origin = Vector3.zero;

		private bool _IsGridGenerated;
        private LevelConfig _lc;

		private string _firstLaunchLevelUrl = "api/Stats/UserPlaysLevel?levelId={0}";
		private string _isPassed = "api/Stats/UserPassedLevel?levelId={0}";

        const float SCREEN_CHECKER_REPEAT_RATE = 1;
        bool _isRotated = false;
        public bool IsScreenRotated = false;
        public static bool IsScreenBlockerShow = true;

        public void ShowScreenBlocker(bool b)
        {
            IsScreenBlockerShow = b;
        }

        void Start ()
		{
            Screen.orientation = ScreenOrientation.Landscape;

            Initialization();
			GenerateGrid ();
            
            Debug.Log("IsScreenBlockerShow" + IsScreenBlockerShow);
            
            if (!IsScreenBlockerShow)
            {
                ScreenCheker.SetActive(false);
                return;
            }

            if (WinPopUp.IsLevelFronFeed)
            {
                IsScreenBlockerShow = true;
                Time.timeScale = 0;
                ScreenChecker();
            }
            else
            {
                ScreenCheker.SetActive(false);
            }
		}

        void ScreenChecker()
        {
            Debug.Log("tik tak");
            if (Input.acceleration.y < -0.95f || IsScreenRotated) // if screen is rotated
            {
                _isRotated = true;
                ScreenCheker.SetActive(false);
                Time.timeScale = 1;
            }

            if (!_isRotated)
                DOVirtual.DelayedCall(SCREEN_CHECKER_REPEAT_RATE, ScreenChecker, true);
        }

        void Initialization()
        {
            if (WinPopUp.IsLevelFronFeed)
            {
                _lc = JsonUtility.FromJson<LevelConfig>(LevelConstriuctorEnterPoint.CurrentLevelFromFeed.GetConfig());
				switch ((LevelPlayedType)PlayedType) 
				{
				case LevelPlayedType.NotPlayed:
					try 
					{
						Uri uri = new Uri(RequestSendHandler.BaseServerUrl + string.Format(_firstLaunchLevelUrl, LevelConstriuctorEnterPoint.CurrentLevelFromFeed.Id));
						RequestSendHandler.SendRequest(uri, "", HttpMethod.Post, ContentType.ApplicationJson, LoginController.TokenType + " " + LoginController.UserToken);
					} catch (Exception ex) 
					{
						Debug.LogError (ex);
					}

					break;

				default:
					break;
				}
            }
            else
            {
                _lc = JsonUtility.FromJson<LevelConfig>(LevelConstriuctorEnterPoint.CurrentLevel.GetConfig());
            }

            objType = new Dictionary<CellType, GameObject>();

            for (int i = 0; i < objectsType.Count; i++)
            {
                objType.Add(objectsType[i].type, objectsType[i].obj);
            }

            _horTileCount = _lc.ColumnCount;
            _vertTileCount = _lc.RowCount;
        }


		void OnDrawGizmos ()
		{
			if (Application.isPlaying)
				return;

			for (float y = 0; y < _origin.y + _tileHeight * _vertTileCount; y += _tileHeight)
			{
				for (float x = 0; x < _origin.x + _tileWidth * _horTileCount; x += _tileWidth)
				{
					//Gizmos.DrawWireCube ( new Vector3(x, y) , Vector3.one);
				}
			}
		}

		public void Show()
		{
			GenerateGrid ();
		}

		public void Hide ()
		{
			for (int x = 0; x <= grid.GetUpperBound (0); x++)
			{
				for (int y = 0; y <= grid.GetUpperBound (1); y++)
				{
					Destroy (grid [x, y].Controller.gameObject);
				}
			}
		}

		public int ElementCount = 0;
		GameObject go;

		void GenerateGrid ()
		{
			grid = new GridTile[ _horTileCount, _vertTileCount ];

			for (int y = 0; y < _horTileCount; y++)
			{
				float yVal = _origin.y - _tileHeight * y;

				for (int x = 0; x < _vertTileCount; x++) {

					if (_lc.LevelSeed [ElementCount] != CellType.Empty) {
					
						float xVal = _origin.x + _tileWidth * x;
						Vector3 tileCenter = new Vector2 (xVal, yVal);

						go = Instantiate (objType [_lc.LevelSeed [ElementCount]], tileCenter, Quaternion.identity) as GameObject;


						go.transform.parent = this.transform;
						go.name = string.Format ("{0},{1}", x, y);

						GridTile tile = new GridTile (x, y, tileCenter, go.AddComponent<GridTileController> ());
						grid [y, x] = tile;

					}
					ElementCount++;
				}
			}
		}
	}
}
