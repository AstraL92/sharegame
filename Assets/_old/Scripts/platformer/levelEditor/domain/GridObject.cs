﻿using UnityEngine;
using System.Collections;

namespace platformer.levelEditor.domain
{
	public class GridObject
	{
		public GridObjectType Type;
		public GameObject Prefab;

		public GridObject ( GridObjectType type, GameObject prefab )
		{
			this.Type = type;
			this.Prefab = prefab;
		}

		public static GridObject GetMockObject ()
		{
			GridObject mock = new GridObject (GridObjectType.BlockStandart, null);
			return mock;
		}
	}
}