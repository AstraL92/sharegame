﻿namespace platformer.levelEditor.domain
{
	public enum GridObjectType
	{
		None,
		BlockStandart,
		BlockJumper,
		MovablePlatform,
		Checkpoint,
		LevelExit
	}
}