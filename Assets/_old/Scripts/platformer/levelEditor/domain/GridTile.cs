﻿using UnityEngine;
using UnityEngine.UI;

namespace platformer.levelEditor.domain
{
	public struct GridTile
	{
		public readonly int X;
		public readonly int Y;
		public readonly Vector2 Center;

		public GridTileController Controller;

		public GridTile ( int x, int y, Vector2 center, GridTileController controller)
		{
			this.X = x;
			this.Y = y;
			this.Center = center;
			this.Controller = controller;
		}

		public override string ToString ()
		{
			return string.Format ("[GridTile]: {0},{1}", X, Y);
		}
	}
}
