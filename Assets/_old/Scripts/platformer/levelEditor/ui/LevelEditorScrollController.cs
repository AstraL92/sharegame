﻿using UnityEngine;
using System.Collections;
using platformer.levelEditor.domain;
using UnityEngine.UI;

namespace platformer.levelEditor.ui
{
	//TODO: rename class
	public class LevelEditorScrollController : MonoBehaviour
	{
		[SerializeField] private ScrollRect _scrollRect;
		[SerializeField] private GameObject _tileObjectPrefab;

		private LevelEditorController _levelEditorController;

		void Awake ()
		{
			_levelEditorController = FindObjectOfType<LevelEditorController> ();

			if (_levelEditorController == null)
				Debug.LogError ("LevelEditorController can't be found on scene");
			
		}

		void Start ()
		{
		}

		public void FillScrollWithObjects ( GridObject[] objects )
		{
			for (int i = 0; i < objects.Length; i++)
			{
				UITileObject tile = GameObject.Instantiate (_tileObjectPrefab).GetComponent<UITileObject>();
				tile.transform.SetParent (_scrollRect.content);
				tile.gameObject.name = objects [i].Type.ToString ();

				tile.Init (objects [i]);
			}
		}
	}
}