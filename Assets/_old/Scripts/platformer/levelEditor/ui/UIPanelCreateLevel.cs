﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace platformer.levelEditor.ui
{
	public class UIPanelCreateLevel : MonoBehaviour
	{
		[SerializeField] private GameObject _panel;

		[SerializeField] private InputField _fieldLevelName;
		[SerializeField] private InputField _fieldTimeLimit;
		[SerializeField] private Dropdown _dropdownLevelType;

		private LevelEditorController _controller;

		void Awake ()
		{
			_controller = FindObjectOfType<LevelEditorController> ();
		}

		public void Show ()
		{
			_panel.SetActive (true);
		}

		public void Hide ()
		{
			_panel.SetActive (false);
		}

		public void OnLevelNameEndEditEvent (string currentText)
		{
			//temp do nothing
		}

		public void OnTimeLimitEndEditEvent (string currentText)
		{
			//temp do nothing
		}

		public void OnBtnCreateLevelClick ()
		{
			String levelName = String.IsNullOrEmpty(_fieldLevelName.text) ? "My new cool level" : _fieldLevelName.text;
			int levelTimeLimit = Convert.ToInt32( String.IsNullOrEmpty(_fieldTimeLimit.text) ? "500" : _fieldTimeLimit.text);
			int levelType = _dropdownLevelType.value; //TODO: check this

			_controller.CreateLevel (levelName, levelTimeLimit, levelType);

			Hide ();
		}

		public void OnBtnCloseClick() //temp disabled
		{
			Hide (); 
		}
	}
}