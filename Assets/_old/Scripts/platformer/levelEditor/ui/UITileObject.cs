﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using platformer.levelEditor.domain;
using HedgehogTeam.EasyTouch;

namespace platformer.levelEditor.ui
{
	public class UITileObject : MonoBehaviour
	{
		[SerializeField] Image _img;
		[SerializeField] Text _description;
		[SerializeField] Button _btnActivate;

		private GridObject _obj;
		private GameObject _pickedObject;

		public void Init ( GridObject obj )
		{
			_obj = obj;

			_description.text = _obj.Type.ToString ();
		}

		public void OnTouchStart ( Gesture gesture )
		{
			Debug.Log ("OnTouchStart " + gesture.position);
			GameObject go = GameObject.Instantiate (_obj.Prefab, gesture.GetTouchToWorldPoint(gesture.pickedUIElement.transform.position), Quaternion.identity) as GameObject;
			Debug.Log ("Instantiated gameobject: " + go.name);
		}
			

		public void OnBtnActivateClick( Gesture gesture )
		{
			Debug.Log ("OnBtnActivateClick " + _obj.Type);
		}
	}
}