﻿using UnityEngine;
using platformer.utils;
using platformer.common.api;
using platformer.game;
using System;

namespace platformer.levelObjects
{
	public class BlockHittable : MonoBehaviour, IGameEventsHandler, IExplosion
	{
        public bool IsPollout;

		[SerializeField] private int _damage = 1;
		[SerializeField] private Collider2D _collider;
		[SerializeField] private GameObject _visual;

        //void OnEnable ()
        //{
        //	GameController.OnGamePaused += OnGamePaused;
        //	GameController.OnGameRestarted += OnGameRestarted;
        //}

        //void OnDisable ()
        //{
        //	GameController.OnGamePaused -= OnGamePaused;
        //	GameController.OnGameRestarted -= OnGameRestarted;
        //}

        #region IGameEventsHandler implementation

        public void OnGamePaused(bool pause)
        {
            //stop moving or smth else... 
        }

        public void OnGameRestarted()
        {
            //Show();
        }

        #endregion

        void OnTriggerEnter2D ( Collider2D other )
		{
			//MonoLog.Log (MonoChannel.LevelEvents, string.Format("{0} entered trigger of {1}", other.name, this.name));

			IDamageReviever affectedObject = other.GetComponent<IDamageReviever> ();

			if (affectedObject != null)
			{
				//Debug.Log ("Damage " + other.name + " with damage: " + _damage);
				affectedObject.OnDamage ( _damage ); 
			}
		}

		private void Show ()
		{
			_visual.SetActive (true);
			_collider.enabled = true;
		}

		private void Hide ()
		{
			_visual.SetActive (false);
			_collider.enabled = false;
		}

        public void OnExplosion()
        {
            if (!IsPollout)
            {
                Destroy(gameObject);
            }
        }
    }
}