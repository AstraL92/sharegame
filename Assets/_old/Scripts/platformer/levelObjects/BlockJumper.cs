﻿using UnityEngine;
using platformer.utils;
using platformer.common.api;
using System;

namespace platformer.levelObjects
{
	public class BlockJumper : MonoBehaviour, IExplosion
	{
		public bool IsJumperPlatform;

		[SerializeField] private Animator _animator;
		[SerializeField] private Vector2 _jumpForce = new Vector2(0,10);

        void OnTriggerEnter2D ( Collider2D other )
		{
			//MonoLog.Log (MonoChannel.LevelEvents, string.Format("{0} entered trigger of {1}", other.name, this.name));

			IVelocityImpact affectedObject = other.GetComponent<IVelocityImpact> ();

			if (affectedObject != null)
			{
				//Debug.Log ("Affected force " + _jumpForce.ConvertToVector3 ());
				affectedObject.ImpactVelocity (_jumpForce.ConvertToVector3 ()); 
			}

            if (other.gameObject.GetComponent<ExplosionArea>())
            {
                other.gameObject.GetComponent<ExplosionArea>().OnExplosion += OnExplosion;
            }
            if (other.tag == "Player" && IsJumperPlatform)
            {
                _animator.SetTrigger("Jump");
            }
			
        }

        public void OnExplosion()
        {
            Destroy(gameObject);
        }
    }
}
