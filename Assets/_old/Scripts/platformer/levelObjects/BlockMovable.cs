﻿using UnityEngine;
using platformer.utils;
using platformer.player;
using platformer.domain;
using platformer.common;

namespace platformer.levelObjects
{
	public class BlockMovable : MonoBehaviour 
	{
        [SerializeField]
		private Vector3 _velocity;

		void Update ()
		{
			if (_velocity != Vector3.zero)
			{
				Move (_velocity * Time.deltaTime);
			}
		}

		void OnCollisionEnter2D (Collision2D other)
		{
			PlayerController player = other.collider.GetComponent<PlayerController> ();
			if (player != null)
			{
				MonoLog.Log (MonoChannel.Player, string.Format("Player collided with movable block {0}", this.name));
				_velocity = new Vector3(player.Velocity.x * 0.3f,0,0);
			}
		}

		void OnCollisionExit2D (Collision2D other)
		{
			_velocity = Vector3.zero;
		}

		void Move ( Vector3 velocity )
		{
			transform.Translate (velocity);
		}


	}
}
