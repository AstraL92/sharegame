﻿using UnityEngine;
using platformer.utils;
using platformer.player;

namespace platformer.levelObjects
{
	[RequireComponent(typeof(BoxCollider2D))]
	public class Checkpoint : MonoBehaviour 
	{
		private bool _isPassed;
		public bool IsPassed
		{
			get { return _isPassed; }
		}

		private Vector3 _position;
		public Vector3 Position
		{
			get { return _position; }
		}

		void Start ()
		{
			_position = this.transform.position;
		}

		void OnTriggerEnter2D ( Collider2D other )
		{
			MonoLog.Log (MonoChannel.LevelEvents, string.Format("{0} entered trigger of {1}", other.name, this.name));

			_isPassed = true;

			PlayerController player = other.GetComponent<PlayerController> ();
			if (player != null)
			{
				MonoLog.Log (MonoChannel.Player, string.Format("Player passed checkpoint {0}", this.name));

				player.PassCheckpoint ( this );
			}
		}
	}
}
