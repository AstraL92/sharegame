﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using platformer.utils;
using platformer.player;

namespace platformer.levelObjects
{
	public class LevelExit : MonoBehaviour 
	{
        [SerializeField]
        AudioSource _doorAS;
        [SerializeField]
        AudioSource _levelEndAS;

        [SerializeField]
        Transform _door;

        private GameObject _winPopUp;

        bool _exit = false;

        void Start()
        {
            _winPopUp = GameObject.FindGameObjectWithTag("WinPopUp");
        }

        void OnTriggerEnter2D ( Collider2D other )
		{
            //MonoLog.Log (MonoChannel.LevelEvents, string.Format("{0} entered trigger of {1}", other.name, this.name));
            if (!_exit && other.tag == "Player")
            {
                StartCoroutine(_playerExit());
                _exit = true;
            }
        }

        IEnumerator _playerExit()
        {
            if (PlayerController.instance != null)
                PlayerController.instance.SwitchOffPlayerMovemen();

            Transform player = null;
            if (PlayerController.instance != null)
                player = PlayerController.instance.transform;

            if (player != null)
            {
                PlayerController.instance.PlayerAnimator.enabled = false;
                player.DOMove(transform.position, 0.5f);
            }

            _doorAS.Play();
            _door.DOLocalMoveY(1, 0.5f);

            yield return new WaitForSeconds(0.5f);

            _door.GetComponent<SpriteRenderer>().sortingOrder = 100;
            if (PlayerController.instance != null)
                PlayerController.instance.HideFace();

            _door.DOLocalMoveY(0, 0.5f);

            yield return new WaitForSeconds(1f);
            //_levelEndAS.Play();
            WinPopUp winPopUp = _winPopUp.GetComponent<WinPopUp>();
            if (winPopUp != null)
            {
                
                winPopUp.SetPanelActive();
            }
        }
	}
}
