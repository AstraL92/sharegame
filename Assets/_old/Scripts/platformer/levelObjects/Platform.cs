﻿using UnityEngine;
using System.Collections.Generic;
using platformer.common;
using platformer.levelObjects.api;

namespace platformer.levelObjects
{
	[RequireComponent(typeof(Raycaster))]
	public class Platform : MonoBehaviour, IPausable
	{
		[SerializeField] private LayerMask _objectsMask;

		[SerializeField] private Vector3[] _localWaypoints;
		[SerializeField] private float _speed = 2;
		[SerializeField] private bool _isCycling;
		[SerializeField] private float _waitTime;

		private Vector3[] _globalWaypoints;
		private int _fromWaipointIndex;
		private float _percentBetweenWaypoints;
		private float _nextMoveTime;

		private Raycaster _rayCaster;

		private const float kSkinWidth = .015f;

		private bool _isPaused;

		void Start ()
		{
			_rayCaster = GetComponent<Raycaster> ();

			_globalWaypoints = new Vector3[ _localWaypoints.Length ];

			for (int i = 0; i < _globalWaypoints.Length; i++)
			{
				_globalWaypoints [i] = _localWaypoints [i] + transform.position;
			}
		}
			
		void Update ()
		{
			_rayCaster.UpdateRaycastOrigins ();

			Vector3 velocity = CalculateMovement ();

			MoveObjects (velocity);
			transform.Translate (velocity);
		}

		void OnEnable ()
		{
			platformer.game.pause.PauseController.OnGamePaused += Pause;
		}

		void OnDisable ()
		{
			platformer.game.pause.PauseController.OnGamePaused -= Pause;
		}

		public void Pause (bool pause)
		{
			_isPaused = pause;
		}

		Vector3 CalculateMovement () //TODO: rename method
		{
			if (_isPaused)
				return Vector3.zero;

			if (Time.time < _nextMoveTime)
				return Vector3.zero;

			_fromWaipointIndex %= _globalWaypoints.Length;
			int toWaypointIndex = (_fromWaipointIndex + 1) % _globalWaypoints.Length;
			float distanceBetweenWaypoints = Vector3.Distance (_globalWaypoints [_fromWaipointIndex], _globalWaypoints [toWaypointIndex]);
			_percentBetweenWaypoints += Time.deltaTime * _speed / distanceBetweenWaypoints;

			Vector3 pos = Vector3.Lerp (_globalWaypoints [_fromWaipointIndex], _globalWaypoints [toWaypointIndex], _percentBetweenWaypoints);

			if (_percentBetweenWaypoints >= 1)
			{
				_percentBetweenWaypoints = 0;
				_fromWaipointIndex++;

				if (_fromWaipointIndex >= _globalWaypoints.Length - 1 && !_isCycling)
				{
					_fromWaipointIndex = 0;
					System.Array.Reverse (_globalWaypoints);
				}

				_nextMoveTime = Time.time + _waitTime;
			}

			return pos - transform.position;

		}

		void MoveObjects ( Vector3 velocity )
		{
			HashSet<Transform> movedObjects = new HashSet<Transform> (); //each object shoud be moved one time per frame

			float dirX = Mathf.Sign (velocity.x);
			float dirY = Mathf.Sign (velocity.y);

			if (velocity.y != 0)
			{
				float rayLength = Mathf.Abs (velocity.y) + kSkinWidth;

				for (int i = 0; i < _rayCaster.VertRayCount; i++)
				{
					Vector2 rayOrigin = (dirY == -1) ? _rayCaster.Origins.bottomLeft : _rayCaster.Origins.topLeft;
					rayOrigin += Vector2.right * (_rayCaster.VertRaySpacing * i);

					RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.up * dirY, rayLength, _objectsMask);

					if (hit)
					{
						if (movedObjects.Contains (hit.transform))
							return;

						movedObjects.Add (hit.transform);

						float pushX = dirY == 1 ? velocity.x : 0;
						float pushY = velocity.y - (hit.distance - kSkinWidth) * dirY;
						//moving object
						hit.transform.Translate (new Vector3 (pushX, pushY));
					}
				}
			}

			if (velocity.x != 0) {
				float rayLength = Mathf.Abs (velocity.x) + kSkinWidth;

				for (int i = 0; i < _rayCaster.HorRayCount; i ++) {
					Vector2 rayOrigin = dirX == -1 ? _rayCaster.Origins.bottomLeft : _rayCaster.Origins.bottomRight;
					rayOrigin += Vector2.up * (_rayCaster.HorRaySpacing * i);

					RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * dirX, rayLength, _objectsMask);

					if (hit) 
					{
						if (hit.distance == 0)
							continue;

						if (movedObjects.Contains (hit.transform))
							return;

						movedObjects.Add(hit.transform);
						float pushX = velocity.x - (hit.distance - kSkinWidth) * dirX;
						float pushY = 0;

						hit.transform.Translate(new Vector3(pushX,pushY));
					}
				}
			}

			if (dirY == -1 || (velocity.y == 0 && velocity.x != 0))
			{
				float rayLength = kSkinWidth * 2;

				for (int i = 0; i < _rayCaster.VertRayCount; i++)
				{
					Vector2 rayOrigin = _rayCaster.Origins.topLeft + Vector2.right * (_rayCaster.VertRaySpacing * i);

					RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.up, rayLength, _objectsMask);

					if (hit)
					{
						if (movedObjects.Contains (hit.transform))
							return;

						movedObjects.Add (hit.transform);

						float pushX = velocity.x;
						float pushY = velocity.y;
						hit.transform.Translate (new Vector3 (pushX, pushY));
					}
				}
			}
		}

		void OnDrawGizmos ()
		{
			if (_localWaypoints != null)
			{
				Gizmos.color = Color.red;
				float size = 0.3f;

				for (int i = 0; i < _localWaypoints.Length; i++)
				{
					Vector3 w =  Application.isPlaying ? _globalWaypoints [i] : _localWaypoints[i] + transform.position;
					Gizmos.DrawLine (w - Vector3.up * size, w + Vector3.up * size);
					Gizmos.DrawLine (w - Vector3.left * size, w + Vector3.left * size);
				}
			}
		}
	}
}
