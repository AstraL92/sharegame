﻿using UnityEngine;
using System.Collections;

namespace platformer.levelObjects.api
{
	public interface IPausable
	{
		void Pause (bool pause);
	}
}