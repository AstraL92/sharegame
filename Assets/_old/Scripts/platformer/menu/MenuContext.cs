﻿using Assets.Scripts.MenuControllers;
using UnityEngine;
using strange.extensions.context.impl;
using platformer.menu.view;
using strange.extensions.context.api;
using platformer.menu.signals;
using platformer.menu.controller;
using platformer.menu.model.api;
using platformer.menu.model.impl;
using platformer.network.service.api;
using platformer.network.service.impl;
using platformer.network.signals;
using platformer.menu.controller.ui;

namespace platformer.menu
{
	public class MenuContext : MVCSContext 
	{
		public MenuContext (MonoBehaviour view) : base(view)
		{
		}

		override public IContext Start()
		{
			base.Start();
			MenuSceneStartSignal startSignal = (MenuSceneStartSignal)injectionBinder.GetInstance<MenuSceneStartSignal>();
			startSignal.Dispatch();

			ServerLoginSignal loginSignal = (ServerLoginSignal)injectionBinder.GetInstance<ServerLoginSignal>();
			loginSignal.Dispatch();

			return this;
		}

		protected override void mapBindings ()
		{
			injectionBinder.Bind<IMenuModel> ().To<MenuModel> ().ToSingleton();
			injectionBinder.Bind<ILevelPostModel> ().To<LevelPostModel> ();
			injectionBinder.Bind<INetworkService>().To<NetworkService>().ToSingleton();

			//view
			mediationBinder.Bind<MenuView> ().To<MenuMediator> (); // main view
			mediationBinder.Bind<MainPanelView> ().To<MainPanelMediator> ();

			mediationBinder.Bind<PageFeedView> ().To<PageFeedMediator> ();
			mediationBinder.Bind<PageShopView> ().To<PageShopMediator> ();
			mediationBinder.Bind<PageCreateView> ().To<PageCreateMediator> ();
			mediationBinder.Bind<PageElementsView> ().To<PageElementsMediator> ();
			mediationBinder.Bind<PageProfileView> ().To<PageProfileMediator> ();

			mediationBinder.Bind<PanelLikesView> ().To<PanelLikesMediator> ();
		    mediationBinder.Bind<LevelReportPopUpView>().To<LevelReportPopUpMediator>();
		    mediationBinder.Bind<TopPanelView>().To<TopPanelMediator>();

//			injectionBinder.Bind<PageButtonClickedSignal>().ToSingleton();
            injectionBinder.Bind<OpenLevelReportPanelSignal>().ToSingleton();

            //commands
            commandBinder.Bind<MenuSceneStartSignal> ().To<MenuSceneStartCommand> ().Once ();
			commandBinder.Bind<ServerLoginSignal> ().To<ServerLoginCommand> ().To<RequestUpdateUserDataCommand>().To<RequestUpdateLevelDataCommand>().InSequence();
			commandBinder.Bind<RequestUpdateUserDataSignal> ().To<RequestUpdateUserDataCommand> (); //probably not needed

			commandBinder.Bind<PageFeedRequestUpdateSignal> ().To<PageFeedRequestUpdateCommand> ();
            commandBinder.Bind<RequestUpdateLevelDataSignal>().To<RequestUpdateLevelDataCommand>();
		    commandBinder.Bind<OpenLevelReportPanelSignal>().To<OpenLevelReportPopUpWindowCommand>();
			commandBinder.Bind<RequestLikesForLevelSignal> ().To<RequestLikesForLevelCommand> ();

			//ui commands
			commandBinder.Bind<FetchAuthorDataSignal>().To<FetchAuthorDataCommand>();

			//network
			injectionBinder.Bind<LoginSuccessSignal>().ToSingleton();
			injectionBinder.Bind<LoginFailSignal>().ToSingleton();

			injectionBinder.Bind<PageFeedDataUpdatedSignal> ().ToSingleton ();
			injectionBinder.Bind<GetUserProfileSuccessSignal> ().ToSingleton ();

			injectionBinder.Bind<RequestLikesForLevelSuccessSignal> ().ToSingleton ();
			injectionBinder.Bind<RequestLikesForLevelFailSignal> ().ToSingleton ();

		}
	}
}
