﻿using UnityEngine;
using strange.extensions.context.impl;

namespace platformer.menu
{
	public class MenuContextView : ContextView 
	{
		void Awake ()
		{
			context = new MenuContext (this);
		}
	}
}
