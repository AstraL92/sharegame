﻿using System;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;
using platformer.menu.view;
using platformer.utils;

namespace platformer.menu.controller
{
	public class MenuSceneStartCommand : Command
	{
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView{ get; set; }

		public override void Execute ()
		{
			MonoLog.Log (MonoChannel.All, "Executing MenuSceneStartCommand");

			GameObject go = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/Menu/MenuView")) as GameObject;
			go.name = "MenuView";
			go.transform.SetParent (contextView.transform);
		}
	}
}

