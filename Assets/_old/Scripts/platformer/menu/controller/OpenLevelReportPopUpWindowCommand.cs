﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;

namespace platformer.menu.controller
{
    public class OpenLevelReportPopUpWindowCommand : Command
    {
        public override void Execute()
        {
            GameObject reportGO = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/Menu/LevelReportPopUp")) as GameObject;
            RectTransform reportRT = reportGO.GetComponent<RectTransform>();
            reportGO.name = "LevelReportPopUp";
            reportGO.transform.SetParent(GameObject.Find("Canvas").transform);
            reportGO.transform.localScale = Vector3.one;
            reportRT.anchoredPosition = Vector3.zero;
			reportRT.sizeDelta = reportRT.sizeDelta;
        }
    }
}

