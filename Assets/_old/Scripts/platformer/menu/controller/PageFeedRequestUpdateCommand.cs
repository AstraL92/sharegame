﻿using System;
using strange.extensions.command.impl;
using platformer.network.service.api;
using platformer.network.signals;

namespace platformer.menu.controller
{
	public class PageFeedRequestUpdateCommand : Command
	{
		[Inject]
		public INetworkService service { get; set; }

		[Inject]
		public PageFeedDataUpdatedSignal signal { get; set; }

		public override void Execute ()
		{
			UnityEngine.Debug.Log ("PageFeedRequestUpdateCommand");
			//service.PageFeedRequesUpdate ().Then( ()=> ( signal.Dispatch() ));
		}
	}
}

