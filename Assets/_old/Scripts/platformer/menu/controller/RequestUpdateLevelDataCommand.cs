﻿using System;
using platformer.domain;
using strange.extensions.command.impl;
using platformer.network.service.api;

namespace platformer.menu.controller
{
	public class RequestUpdateLevelDataCommand : Command
	{
		[Inject] public INetworkService service { get; set; }

		public override void Execute ()
		{
			service.RequestLevelsData (new Guid[]{ Guid.Empty });
		}
	}
}

