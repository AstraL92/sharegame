﻿using System;
using strange.extensions.command.impl;
using platformer.network.service.api;

namespace platformer.menu.controller
{
	public class RequestUpdateUserDataCommand : Command
	{
		[Inject] public INetworkService service { get; set; }

		public override void Execute ()
		{
			Guid testGuidRegisteredUser = Guid.Empty;

			service.RequestUsersData ( new Guid[]{ testGuidRegisteredUser } );
		}
	}
}

