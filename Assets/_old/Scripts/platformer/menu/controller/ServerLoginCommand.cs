﻿using System;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;
using platformer.menu.view;
using platformer.network.service.api;
using platformer.network.signals;

namespace platformer.menu.controller
{
	public class ServerLoginCommand : Command
	{
		[Inject]
		public INetworkService service {get; set;}

		public override void Execute ()
		{
			Retain ();
			service.loginSuccessSignal.AddListener (OnLoginSuccess);
			service.loginFailSignal.AddListener (OnLoginFail);

			service.Login ();
		}

		//[ListensTo(typeof(LoginSuccessSignal))]
		public void OnLoginSuccess ()
		{
			Debug.Log ("login succeed");

			service.loginSuccessSignal.RemoveListener (OnLoginSuccess);
			service.loginFailSignal.RemoveListener (OnLoginFail);

			Release ();
		}

		//[ListensTo(typeof(LoginFailSignal))]
		public void OnLoginFail ( int errorCode )
		{
			Debug.Log ("login error: code: " + errorCode);
			service.loginSuccessSignal.RemoveListener (OnLoginSuccess);
			service.loginFailSignal.RemoveListener (OnLoginFail);

			Release ();
		}

	}
}

