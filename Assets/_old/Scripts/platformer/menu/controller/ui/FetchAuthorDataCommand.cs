﻿using System;
using strange.extensions.command.impl;
using platformer.network.service.api;
using UnityEngine.UI;
using platformer.domain;
using UnityEngine;
using System.Collections;
using strange.extensions.context.api;

namespace platformer.menu.controller.ui
{
	public class FetchAuthorDataCommand : Command
	{
		[Inject]
		public Guid authorGuid { get; set; }

		[Inject]
		public Text authorName { get; set; }

		[Inject]
		public Image authorAvatar { get; set; }

		[Inject]
		public INetworkService service { get; set; }

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView {get;set;}

		private MonoBehaviour _root;

		public override void Execute ()
		{
			_root = contextView.GetComponent<MonoBehaviour>();

			service.RequestUsersData (new Guid[] { authorGuid }).Then(OnAuthorDataRequestedSuccess);
		}

		private void OnAuthorDataRequestedSuccess ( UserData[] data )
		{
			Debug.Assert (data.Length == 1); //we sent only one guid, so expext to recieve only one UserData object
			//TODO: use try-catch?

			authorName.text = data [0].Name;

			_root.StartCoroutine( UpdateAuthorAvatar ( data[0].AvatarURL ) );
		}

		IEnumerator UpdateAuthorAvatar (string avatarURL)
		{
			WWW www = new WWW(avatarURL);
			yield return www;

			Sprite av = Sprite.Create (www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);

			authorAvatar.sprite = av;
		}
	}
}

