﻿using UnityEngine;
using platformer.domain;
using platformer.domain.level;

namespace platformer.menu.model.api
{
	public interface ILevelPostModel 
	{
		LevelData Level { get; }
	}
}
