﻿using System;
using platformer.domain;
using platformer.domain.level;

namespace platformer.menu.model.api
{
	public interface IMenuModel
	{
		ILevelPostModel[] FeedData { get; set; }
	}
}

