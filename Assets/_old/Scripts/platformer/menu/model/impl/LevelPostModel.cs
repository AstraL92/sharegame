﻿using UnityEngine;
using platformer.domain;
using platformer.menu.model.api;
using platformer.domain.level;

namespace platformer.menu.model.impl
{
	public class LevelPostModel : ILevelPostModel 
	{
		public LevelData Level { get; private set; }

		public LevelPostModel ()
		{
			Level = LevelData.GetMockObject ();
		}
	}
}
