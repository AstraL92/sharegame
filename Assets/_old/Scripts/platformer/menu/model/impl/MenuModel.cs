﻿using System;
using platformer.domain.level;
using platformer.menu.model.api;
using platformer.utils;
using platformer.domain;

namespace platformer.menu.model.impl
{
	public class MenuModel : IMenuModel
	{
		private ILevelPostModel[] _feedData;
		public ILevelPostModel[] FeedData 
		{ 
			get { return _feedData; }
			set
			{
				_feedData = value;
				MonoLog.Log (MonoChannel.Data, "FeedData was updated in model");
			}
		}

	    public MenuModel ()
		{
		}
	}
}

