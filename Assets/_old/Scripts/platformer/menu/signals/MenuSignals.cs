﻿using System;
using strange.extensions.signal.impl;
using platformer.domain.level;
using UnityEngine.UI;

namespace platformer.menu.signals
{
//	public enum PageType
//	{
//		None,
//		Feed,
//		Shop,
//		Create,
//		Elements,
//		Profile
//	}

	public class MenuSceneStartSignal : Signal {} //start signal, entry point

	//common, network
	public class ServerLoginSignal : Signal {}
	public class PageFeedRequestUpdateSignal : Signal {} //request new levels for feed
	public class RequestUpdateUserDataSignal : Signal {} //request user data for page profile
	public class RequestUpdateLevelDataSignal : Signal {} //request level data 
    public class OpenLevelReportPanelSignal: Signal {}
	public class RequestLikesForLevelSignal: Signal<LevelData>  {} //request likes for selected level


    //feed
    public class OpenMenuMoreSignal : Signal {}

	public class OpenUserProfileSignal : Signal {}


//	public class PageButtonClickedSignal : Signal<PageType> {};


	//ui data/requests
	public class FetchAuthorDataSignal : Signal<Guid, Text, Image> {}; // authorGuid, authorName, authorAvatar
}

