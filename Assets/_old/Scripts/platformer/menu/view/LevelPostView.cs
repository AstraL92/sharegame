﻿using UnityEngine;
using strange.extensions.mediation.impl;
using platformer.menu.model.api;
using UnityEngine.UI;
using System.Collections;
using platformer.utils;
using platformer.menu.signals;

namespace platformer.menu.view
{
	public class LevelPostView : View 
	{
		public Image AuthorAvatar;
		public Text AuthorName;

		public Image LevelPreview;
		public Text LevelDescription;

	    public Text MoreComments;
	    public Text LikedUsers;
        public Text PlayCount;
	    public GameObject PostMoreBtnPanel;

	    [Inject] public OpenLevelReportPanelSignal openLevelReportPanelSignal { get; set; }
		[Inject] public FetchAuthorDataSignal fetchAuthorDataSignal { get; set; }

		private ILevelPostModel _data;

	    public void Init ( ILevelPostModel data )
		{
			_data = data;
			StartCoroutine( UpdateLevelPreview (_data.Level.LevelPreviewURL) );

			LevelDescription.text = _data.Level.Description;
			MoreComments.text = string.Format("<b>{0} more comments</b>", _data.Level.Comments.Count );
			LikedUsers.text = string.Format("van_game, arto, 31337 and {0} others", _data.Level.likedUsersIds.Length );
			PlayCount.text = "42"; //this data has to be in statistic module
            
		}

		[PostConstruct]
		public void PostConstruct()
		{
			fetchAuthorDataSignal.Dispatch (_data.Level.AuthorId, AuthorName, AuthorAvatar);
		}

		public void OnAuthorAvatarClicked ()
		{
			//open userProfile
			Debug.Log("OnAuthorAvatarClicked");
        }

		public void OnAuthorNameClicked ()
		{
			//open userProfile
			Debug.Log("OnAuthorNameClicked");
		}

		public void OnBtnMoreClicked ()
		{
			//open more menu
			Debug.Log("OnBtnMoreClicked");
            PostMoreBtnPanel.SetActive(!PostMoreBtnPanel.activeInHierarchy);
        }

		public void OnBtnLikeClicked ()
		{
			//like level
			Debug.Log("OnBtnLikeClicked");
		}

		public void OnBtnCommentClicked ()
		{
			//comment level
			Debug.Log("OnBtnCommentClicked");
		}

        public void OnBtnFBShareClicked()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnFBShareClicked");
        }

        public void OnBtnTwitterShareClicked()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnTwitterShareClicked");
        }

        public void OnBtnCopyLinkClicked()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnCopyLinkClicked");
        }

        public void OnBtnReportClicked()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnReportClicked");
			openLevelReportPanelSignal.Dispatch();
        }

		IEnumerator UpdateLevelPreview (string previewURL)
		{
            Debug.Log("UpdateLevelPreview");

			WWW www = new WWW(previewURL);
			yield return www;
            
			Sprite preview = Sprite.Create (www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);

			LevelPreview.sprite = preview;
		}
	}
}
