﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;

namespace platformer.menu.view
{
    public class LevelReportPopUpMediator : Mediator
    {
        [Inject]
        public LevelReportPopUpView view { get; set; }


        public override void OnRegister()
        {
            view.btnReportSignal.AddListener(OnBtnReportSignal);
            view.btnBackSignal.AddListener(OnBtnBackSignal);
            view.Init();
        }

        public override void OnRemove()
        {
            view.btnReportSignal.RemoveListener(OnBtnReportSignal);
            view.btnBackSignal.RemoveListener(OnBtnBackSignal);
        }

        private void OnBtnReportSignal()
        {

        }

        private void OnBtnBackSignal()
        {

        }
    }
}
