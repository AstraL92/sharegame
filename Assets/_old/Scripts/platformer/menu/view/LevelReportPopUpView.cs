﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using platformer.utils;
using strange.extensions.signal.impl;
using UnityEngine.UI;

namespace platformer.menu.view
{
    public class LevelReportPopUpView : View
    {
        public Image UserAvatar;
        public Text UserName;
        public Text CommentText;
        public Signal btnBackSignal = new Signal();
        public Signal btnReportSignal = new Signal();

        public void Init()
        {
            MonoLog.Log(MonoChannel.UI, "LevelReportPopUp inited");
        }

        public void OnBtnBackClick()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnBack clicked");
            btnBackSignal.Dispatch();
            Destroy(gameObject);
        }

        public void OnBtnReportClick()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnReport clicked");
            btnReportSignal.Dispatch();
            Destroy(gameObject);
        }
    }
}
