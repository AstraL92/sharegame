﻿using System;
using Assets.Scripts.MenuControllers;
using strange.extensions.mediation.impl;
using platformer.menu.signals;
using platformer.network.signals;

namespace platformer.menu.view
{
	public class MainPanelMediator : Mediator
	{
		[Inject]
		public MainPanelView view { get; set;}

		public override void OnRegister ()
		{
//			view.Init ();

			UnityEngine.Debug.Log ("MainPanelMediator");
		}
			
		public override void OnRemove ()
		{
		}

//		[ListensTo(typeof(PageButtonClickedSignal))]
//		public void OnPageButtonClickSignal ( MainPanelView.PageType type )
//		{
//			view.SwitchPageTo (type);
//		}
	}
}

