﻿using System;
using strange.extensions.mediation.impl;
using platformer.menu.signals;

namespace platformer.menu.view
{
	public class MenuMediator : Mediator
	{
		[Inject]
		public MenuView view { get; set;}

		public override void OnRegister ()
		{
			view.Init ();
		}

		public override void OnRemove ()
		{
		}
	}
}

