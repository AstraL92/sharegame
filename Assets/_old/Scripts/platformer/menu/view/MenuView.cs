﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI.Extensions;
using strange.extensions.mediation.impl;
using platformer.menu.signals;
using strange.extensions.context.api;

namespace platformer.menu.view
{
	public class MenuView : View
	{
		public Transform Canvas;

		public void Init ()
		{
			GameObject go = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/Menu/MainPanelView")) as GameObject;
			go.name = "MainPanelView";
			go.transform.SetParent (Canvas);
			go.transform.localScale = Vector3.one;

            RectTransform rt = go.GetComponent<RectTransform> ();
			rt.anchoredPosition = Vector2.zero;
			rt.sizeDelta = new Vector2 (Screen.width / GameObject.Find("Canvas").transform.localScale.x, Screen.height / GameObject.Find("Canvas").transform.localScale.x);

            GameObject topPanelGO = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/Menu/TopPanelView")) as GameObject;
            RectTransform topPanelRT = topPanelGO.GetComponent<RectTransform>();
            Vector2 topPanelSizeDelta = topPanelRT.sizeDelta;
            topPanelGO.name = "TopPanelView";		    
            topPanelGO.transform.SetParent(Canvas);
            topPanelGO.transform.localScale = Vector3.one;
            topPanelRT.anchoredPosition = Vector3.zero;
		    topPanelRT.sizeDelta = topPanelSizeDelta;

            GameObject bottomPanelGO = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("Prefabs/Menu/BottomPanelView")) as GameObject;
            RectTransform bottomPanelRT = bottomPanelGO.GetComponent<RectTransform>();
            Vector2 bottomPanelSizeDelta = bottomPanelRT.sizeDelta;
            bottomPanelGO.name = "BottomPanelView";
            bottomPanelGO.transform.SetParent(Canvas);
            bottomPanelGO.transform.localScale = Vector3.one;
            bottomPanelRT.anchoredPosition = Vector3.zero;
            bottomPanelRT.sizeDelta = bottomPanelSizeDelta;
        }
	}
}