﻿using UnityEngine;
using strange.extensions.mediation.impl;

namespace platformer.menu.view
{
	public class PageCreateMediator : Mediator 
	{
		[Inject]
		public PageCreateView view { get; set; }

		public override void OnRegister ()
		{
			view.Init ();
		}

		public override void OnRemove ()
		{
			
		}
	}
}
