﻿using UnityEngine;
using strange.extensions.mediation.impl;
using platformer.utils;
using UnityEngine.SceneManagement;

namespace platformer.menu.view
{
	public class PageCreateView : View 
	{
		public void Init()
		{
			MonoLog.Log (MonoChannel.UI, "PageCreate inited");
		}

	    public void OnBtnCreateLevelClicked()
	    {
            Debug.Log("OnBtnCreateLevelClicked");
            //Load Create levelScene
//	        SceneManager.LoadScene("");
	    }
        public void OnBtnCreateElementClicked()
        {
            Debug.Log("OnBtnCreateElementClicked");

        }
	}
}
