﻿using UnityEngine;
using strange.extensions.mediation.impl;

namespace platformer.menu.view
{
	public class PageElementsMediator : Mediator 
	{
		[Inject]
		public PageElementsView view { get; set; }

		public override void OnRegister ()
		{
			view.Init ();
		}

		public override void OnRemove ()
		{
			
		}
	}
}
