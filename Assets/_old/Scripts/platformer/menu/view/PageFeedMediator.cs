﻿using UnityEngine;
using strange.extensions.mediation.impl;
using platformer.network.signals;
using platformer.network.service.api;
using platformer.menu.model.api;
using platformer.menu.signals;

namespace platformer.menu.view
{
	public class PageFeedMediator : Mediator 
	{
		[Inject]
		public PageFeedView view { get; set; }

		[Inject]
		public IMenuModel model { get; set; }

		[Inject]
		public PageFeedRequestUpdateSignal pageFeedRequestUpdateSignal { get; set; }

		[Inject]
		public PageFeedDataUpdatedSignal pageFeedDataUpdatedSignal { get; set; }

		public override void OnRegister ()
		{
			Debug.Log ("PageFeedMediator");
			pageFeedDataUpdatedSignal.AddListener (OnFeedDataUpdated);
//			view.Init ();

			pageFeedRequestUpdateSignal.Dispatch ();
		}

		public override void OnRemove ()
		{
			pageFeedDataUpdatedSignal.RemoveListener (OnFeedDataUpdated);
		}

		public void OnFeedDataUpdated ()
		{
			Debug.Log ("PageFeedDataUpdatedSignal OnFeedDataUpdated");
//			view.UpdateFeed (model.FeedData);
		}
	}
}
