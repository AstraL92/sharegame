﻿using UnityEngine;
using strange.extensions.mediation.impl;
using platformer.menu.model.api;
using platformer.network.signals;

namespace platformer.menu.view
{
	public class PageProfileMediator : Mediator 
	{
		[Inject]
		public PageProfileView view { get; set; }

		[Inject]
		public IMenuModel model { get; set; }

		public override void OnRegister ()
		{
			view.Init ();
		}

		public override void OnRemove ()
		{
			
		}

		[ListensTo(typeof(GetUserProfileSuccessSignal))]
		public void OnUserDataUpdated ()
		{
			//FIXME
			//view.UpdateUser (model.UserData); 
		}

        public void OnLevelDataUpdated()
        {
            //FIXME
        }

	}
}
