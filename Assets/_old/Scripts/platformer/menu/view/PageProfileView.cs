﻿using System.Collections;
using platformer.domain;
using platformer.domain.level;
using platformer.menu.model.api;
using platformer.menu.model.impl;
using platformer.utils;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace platformer.menu.view
{
    public class PageProfileView : View
    {
        public Text registrationDate;
        public Image userAvatar;
        public Text userName;

        public Image userSkin;
        public ScrollRect scroll;
        private LevelPostView[] _contentElements;

        public void Init()
        {
            MonoLog.Log(MonoChannel.UI, "PageProfile inited");
        }

        public void UpdateUser(UserData user)
        {
            StartCoroutine(UpdateUserAvatar(user.AvatarURL));

            userName.text = user.Name;
//            registrationDate.text = user.RegistrationDate.ToString();
        }

        public void UpdateLevelData(ILevelPostModel[] levelData)
        {
//            _contentElements = new LevelPostView[levelData.Length];
//            for (int i = 0; i < levelData.Length; i++)
//            {
//            GameObject go = Instantiate(Resources.Load<GameObject>("Prefabs/Menu/pageFeed/levelPost"));
//
//                go.transform.SetParent(scroll.content.transform);
//                go.transform.localScale = Vector3.one;
//                _contentElements[i] = go.GetComponent<LevelPostView>();
//
//                _contentElements[i].Init(levelData[i]);
//            }
        }

        public void OnBtnChangeSkinClick()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnChangeSkin clicked");
        }

        public void OnBtnEditAvatarClick()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnEditAvatar clicked");
        }

        private IEnumerator UpdateUserAvatar(string avatarURL)
        {
            var www = new WWW(avatarURL);
            yield return www;

            Sprite avatar = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height),
                Vector2.zero);

            userAvatar.sprite = avatar;
        }
    }
}