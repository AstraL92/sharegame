﻿using UnityEngine;
using strange.extensions.mediation.impl;

namespace platformer.menu.view
{
	public class PageShopMediator : Mediator 
	{
		[Inject]
		public PageShopView view { get; set; }

		public override void OnRegister ()
		{
			view.Init ();
		}

		public override void OnRemove ()
		{
			
		}
	}
}
