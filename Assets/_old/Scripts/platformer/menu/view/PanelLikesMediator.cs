﻿using UnityEngine;
using strange.extensions.mediation.impl;

namespace platformer.menu.view
{
	public class PanelLikesMediator : Mediator 
	{
		[Inject]
		public PanelLikesView view { get; set; }

		public override void OnRegister ()
		{
			view.Init ();
		}

		public override void OnRemove ()
		{
			
		}
	}
}
