﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using platformer.network.signals;
using platformer.menu.model.api;

namespace platformer.menu.view
{
    public class TopPanelMediator : Mediator
    {
        [Inject]
        public TopPanelView view { get; set; }

        [Inject]
        public IMenuModel model { get; set; }

        public override void OnRegister()
        {
            view.btnPlusGoldSignal.AddListener(OnBtnPlusGoldSignal);
            view.btnPlusGoldSignal.AddListener(OnBtnPlusCrystalsSignal);
            view.Init();
        }

        public override void OnRemove()
        {
            view.btnPlusGoldSignal.RemoveListener(OnBtnPlusGoldSignal);
            view.btnPlusGoldSignal.RemoveListener(OnBtnPlusCrystalsSignal);
        }

        [ListensTo(typeof(GetUserProfileSuccessSignal))]
        public void OnUserDataUpdated()
        {
			//FIXME
            //view.UpdateTopPanel(model.UserData);
        }

        public void OnBtnPlusGoldSignal()
        {

        }

        public void OnBtnPlusCrystalsSignal()
        {

        }
    }
}


