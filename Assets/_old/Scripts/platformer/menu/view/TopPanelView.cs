﻿using UnityEngine;
using System.Collections;
using platformer.utils;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using platformer.domain;

namespace platformer.menu.view
{
    public class TopPanelView : View
    {
        public Text UserLevel;
        public Image LevelProgresBarFill;
        public Text LevelProgresText;
        public Text GoldCountText;
        public Text CrystalsCountText;
        public Signal btnPlusGoldSignal = new Signal();
        public Signal btnPlusCrystalsSignal = new Signal();

        public void UpdateTopPanel(UserData user)
        {
            UserLevel.text = user.Level.ToString();
            GoldCountText.text = user.Coins.ToString();
            CrystalsCountText.text = user.Сrystals.ToString();
        }

        public void Init()
        {
            MonoLog.Log(MonoChannel.UI, "TopPanelView inited");
        }

        public void OnBtnPlusGoldClick()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnPlusGold clicked");
            btnPlusGoldSignal.Dispatch();
        }

        public void OnBtnPlusCrystalsClick()
        {
            MonoLog.Log(MonoChannel.UI, "OnBtnPlusCrystals clicked");
            btnPlusCrystalsSignal.Dispatch();
        }
    }
}
