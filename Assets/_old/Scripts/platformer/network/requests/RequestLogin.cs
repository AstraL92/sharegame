﻿using UnityEngine;
using System.Collections;
using System;

namespace platformer.network.requests
{
	[Serializable]
	public class RequestLogin 
	{
		public string DeviceId;

		public static RequestLogin GetMockObject ()
		{
			RequestLogin mock = new RequestLogin();
			mock.DeviceId = "testdeviceid";

			return mock;
		}
	}
}