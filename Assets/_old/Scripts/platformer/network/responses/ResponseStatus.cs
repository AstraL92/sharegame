﻿using UnityEngine;
using System.Collections;

namespace platformer.network.responses
{
	public enum ResponseStatus
	{
		None,
		NotConnected,
		ResponseException,
		ServerError
	}
}