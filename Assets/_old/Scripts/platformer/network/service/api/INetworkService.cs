﻿using System;
using platformer.domain;
using platformer.network.signals;
using platformer.domain.level;
using strange.extensions.promise.api;

namespace platformer.network.service.api
{
	public interface INetworkService
	{
		void Login ();
		LoginSuccessSignal loginSuccessSignal { get; }
		LoginFailSignal loginFailSignal { get; }

		IPromise PageFeedRequesUpdate ();

		IPromise<UserData[]> RequestUsersData ( Guid[] usersIds );
		IPromise<LevelData[]> RequestLevelsData ( Guid[] levelsIds );

//		[Obsolete("will be deleted")]
//        void RequestLevelsByUser(UserData userData);
//        GetLevelDataSuccessSignal getLevelDataSuccessSignal { get; }

		void RequestLikesForLevel(LevelData levelData);
		RequestLikesForLevelSuccessSignal requestLikesForLevelSuccessSignal { get; }
		RequestLikesForLevelFailSignal requestLikesForLevelFailSignal { get; }


	}
}