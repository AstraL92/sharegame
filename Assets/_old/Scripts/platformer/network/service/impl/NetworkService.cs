﻿using System;
using System.Collections;
using System.Collections.Generic;
using platformer.domain;
using platformer.domain.level;
using UnityEngine;
using platformer.network.responses;
using platformer.utils;
using platformer.network.requests;
using platformer.network.signals;
using strange.extensions.context.api;
using platformer.network.service.api;
using platformer.menu.model.api;
using strange.extensions.promise.api;
using strange.extensions.promise.impl;

namespace platformer.network.service.impl
{
    public class NetworkService : INetworkService
    {
		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView {get;set;}

		[Inject] public IMenuModel dataModel { get; set; }
		[Inject] public IMenuModel levelData { get; set; }

		[Inject] public LoginSuccessSignal loginSuccessSignal {get;set;}
		[Inject] public LoginFailSignal loginFailSignal {get;set;}

		[Inject] public RequestLikesForLevelSuccessSignal requestLikesForLevelSuccessSignal { get; set; }
		[Inject] public RequestLikesForLevelFailSignal requestLikesForLevelFailSignal { get; set; }

        private const string BaseServerURL = "http://dev.bidon-tech.com:9999/ShareGame"; //move to config

		private MonoBehaviour _root;
        //private string _accessToken;

		private IPromise pageFeedUpdatedPromise = new Promise();
		private IPromise<UserData[]> usersDataRequestedPromise = new Promise<UserData[]>();
		private IPromise<LevelData[]> levelsDataRequestedPromise = new Promise<LevelData[]>();

		public NetworkService ()
		{
		}

		[PostConstruct]
		public void PostConstruct ()
		{
			_root = contextView.GetComponent<MonoBehaviour>();
		}

		public void Login ()
		{
			string requestURL = string.Format(NetworkRequests.LoginRequest, BaseServerURL);
			var request = new HTTP.Request("POST", requestURL);
			request.headers.Set("Content-Type", "application/json");

			string deviceId = SystemInfo.deviceUniqueIdentifier;
			RequestLogin rLogin = new RequestLogin() { DeviceId = deviceId };

			request.Text = JsonUtility.ToJson (rLogin);

			Debug.Log(JsonUtility.ToJson( rLogin ));

			_root.StartCoroutine(SendRequest(request, (response) =>
			{
				//ResponseLogin respLogin = JsonUtility.FromJson<ResponseLogin>(response.Text);

				//_accessToken = respLogin.access_token;

				loginSuccessSignal.Dispatch();

			}, (errorStatus) =>
			{
				loginFailSignal.Dispatch((int)errorStatus); 
			}));
		}

		public IPromise PageFeedRequesUpdate ()
		{
			//test data
			ILevelPostModel mock = new platformer.menu.model.impl.LevelPostModel() as ILevelPostModel;
			ILevelPostModel[] test = new ILevelPostModel[] {mock, mock, mock, mock, mock};
			Debug.Log ("PageFeedRequesUpdate " + test.Length);
			dataModel.FeedData = test;

			pageFeedUpdatedPromise.Dispatch (); //we operate with mock data, so dispatch promise as soon as test data ready
			return pageFeedUpdatedPromise;
		}

		public IPromise<UserData[]> RequestUsersData ( Guid[] usersIds )
		{
			//test data
			platformer.domain.UserData testData = platformer.domain.UserData.GetMockObject();
			UserData[] data = new UserData[]{ testData };
			//dataModel.UserData = testData;

			usersDataRequestedPromise.Dispatch ( data );
			return usersDataRequestedPromise;
		}

		public IPromise<LevelData[]> RequestLevelsData ( Guid[] levelsIds )
		{
			//test data
			LevelData testData = LevelData.GetMockObject();
			LevelData[] data = new LevelData[]{ testData };

			levelsDataRequestedPromise.Dispatch ( data );

			return levelsDataRequestedPromise;
		}

		public void RequestLikesForLevel(LevelData levelData)
		{
			//test data

		}

		private IEnumerator SendRequest(HTTP.Request request, Action<HTTP.Response> onSuccess, Action<ResponseStatus> onFail)
        {
//            if (!IsConnectedToInternet())
//            {
//                if (onFail != null)
//                    onFail(ResponseStatus.NotConnected);
//
//				MonoLog.LogError (MonoChannel.Network, "Not connected to Internet");
//            }

            yield return request.Send();

            if (request.exception != null)
            {
                if (onFail != null)
                    onFail(ResponseStatus.ResponseException);

				MonoLog.LogError (MonoChannel.Network, "ResponseException: " + request.exception);

                yield break;
            }

            var response = request.response;
            if (response.status != 200)
            {
                if (onFail != null)
					onFail(ResponseStatus.ServerError);

				MonoLog.LogError (MonoChannel.Network, "Server error: " + response.status + ". Response: " + response.Text.ToString());

                yield break;
            }

            if (onSuccess != null)
                onSuccess(response);

			MonoLog.Log (MonoChannel.Network, response.Text.ToString ());
        }

        
    }
}
