﻿using UnityEngine;
using strange.extensions.signal.impl;

namespace platformer.network.signals
{
	public class LoginSuccessSignal : Signal {}
	public class LoginFailSignal : Signal<int> {}

	public class PageFeedDataUpdatedSignal : Signal {} //new levels for feed arrived //TODO: add failed signal?

	public class GetUserProfileSuccessSignal : Signal {}

	public class RequestLikesForLevelSuccessSignal : Signal {}
	public class RequestLikesForLevelFailSignal : Signal {}
}
