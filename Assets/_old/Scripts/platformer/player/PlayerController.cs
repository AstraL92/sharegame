﻿using UnityEngine;
using platformer.common;
using platformer.common.api;
using platformer.levelObjects;
using System;
using UnityEngine.UI;

namespace platformer.player
{
	[RequireComponent(typeof(PlayerView))]
	public class PlayerController : MonoBehaviour, IVelocityImpact, IDamageReviever, IExplosion
	{
        public static PlayerController instance;

		public GameObject Sprite;
        public GameObject LosePopUp;
		public PlayerAnimatorHandler PlayerAnimator;

		[SerializeField] [Range (0,1)] private float _speedCoef;
		[SerializeField] [Range (0,1)] private float _jumpCoef;
		[SerializeField] [Range (0,1)] private float _onIceSpeedCoef = 0.2f;

        [SerializeField] private PlayerView _view;
		public PlayerView View { get { return _view; } }

		private PlayerModel _model;
		public PlayerModel Model { get { return _model; } }

		private bool _isRun;

        private bool _onSlimeBlock;
        public bool OnSlimeBlock
        {
            get
            {
                return _onSlimeBlock;
            }
            set
            {
                _onSlimeBlock = value;
            }
        }

        private bool _onSlimeBlockDoubleJump;
        public bool OnSlimeBlockDoubleJump
        {
            get
            {
                return _onSlimeBlockDoubleJump;
            }
            set
            {
                _onSlimeBlockDoubleJump = value;
            }
        }

        private Vector3 _velocity;
		public Vector3 Velocity
        {
            set
            {
                _velocity = value;
            }
            get
            {
                return _velocity;
            }
        }

		private float _velocityXSmoothing;
		private bool _isDoubleJumped;

		public event Action OnPlayerDied;
		private Vector3 _initionalPosition;

        bool _popupShowed = false;

        bool _onIce = false;

        int _iceDir = 0;

        [SerializeField]
        private PlayerSoundHandler _playerSoundHandler;
        private bool _isFirstHit = false;

        void Awake ()
		{
            if (instance != null && instance != this)
            {
                DestroyImmediate(this.gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(this.gameObject);

            _initionalPosition = this.transform.position;
			_model = new PlayerModel ();
		}

		void Start ()
		{
            LosePopUp = GameObject.FindGameObjectWithTag("LosePopUp");
            ResetPlayer ();
		}

		void Update ()
		{
            if (!_popupShowed && this.transform.position.y < -35)
            {
                WinPopUp winPopUp = LosePopUp.GetComponent<WinPopUp>();
                if (winPopUp != null)
                    winPopUp.SetPanelActive();
                _popupShowed = true;
                //this.transform.position = _initionalPosition;
            }
            //OnDie ();
        }

        public void HideFace()
        {
            _view._playerAvatar.sortingOrder = 4;
        }

        public void SwitchOffPlayerMovemen()
        {
            PlayerAnimator.MuteSteps();
            _view.enabled = false;
        }

		void ResetPlayer ()
		{
            _popupShowed = false;
			_model.CurrentHP = _model.MaxHP;
			_view.HP.Init (_model.MaxHP);
		}

		public void OnDamage ( int recievedDamage )
		{
			_model.CurrentHP -= recievedDamage;
			_view.HP.UpdateHP (_model.CurrentHP);
			PlayerAnimator.Harmed ();
			if (_model.CurrentHP <= 0)
            {
                //LosePopUp.GetComponent<Image>().enabled = true;
                LosePopUp.GetComponent<WinPopUp>().SetPanelActive();
                //OnDie ();
            }
        }

		public void CheckCollisionInfo ()
		{
			if (_model.CollisionInfo.IsAbove || _model.CollisionInfo.IsBelow)
			{
				_velocity.y = 0;
			}
		}

		public void PassCheckpoint ( Checkpoint checkpoint )
		{
			_model.LastCheckpoint = checkpoint;
		}

		public void ApplyInput (Vector2 input)
		{
            if (_onIce && Mathf.Abs(input.x) < 0.01f)
            {
                if (_iceDir > 0)
                {
                    _velocity.x -= Time.deltaTime * _onIceSpeedCoef;
                }
                else
                {
                    _velocity.x += Time.deltaTime * _onIceSpeedCoef;
                }
                
                PlayerAnimator.StopRun();

                if (Math.Abs(_velocity.x) < 0.25f)
                {
                    _isRun = false;
                    _velocity.x = 0f;
                }
            }
            else
            {
                if (_onSlimeBlock)
                {
                    float targetVelocity = (input.x / 2) * _model.MoveSpeed;
                    float accelerationTime = _model.CollisionInfo.IsBelow ? _model.AccelTimeGrounded : _model.AccelTimeAirborne;
                    _velocity.x = Mathf.SmoothDamp(_velocity.x * _speedCoef, targetVelocity, ref _velocityXSmoothing, accelerationTime);
                }
                else
                {
                    float targetVelocity = input.x * _model.MoveSpeed;
                    float accelerationTime = _model.CollisionInfo.IsBelow ? _model.AccelTimeGrounded : _model.AccelTimeAirborne;
                    _velocity.x = Mathf.SmoothDamp(_velocity.x * _speedCoef, targetVelocity, ref _velocityXSmoothing, accelerationTime);
                }

                _iceDir = _velocity.x > 0 ? 1 : -1;

                if (Math.Abs(_velocity.x) < 0.25f)
                {
                    _isRun = false;
                    PlayerAnimator.StopRun();
                    _velocity.x = 0f;
                }
                else
                {
                    _isRun = true;
                    PlayerAnimator.Run();
                }
            }
		}

		public void ApplyJump ()
		{
            _isFirstHit = true;

            if (!_model.CollisionInfo.IsBelow)
			{
				if (!_isDoubleJumped)
				{
                    if (_onSlimeBlockDoubleJump)
                    {
						PlayerAnimator.Jump ();
						_velocity.y = (_model.DoubleJumpVelocity * _jumpCoef) / 1.5f;
                        _isDoubleJumped = true;
                        _onSlimeBlockDoubleJump = false;
                    }
                    else
                    {
						PlayerAnimator.Jump ();
						_velocity.y = _model.DoubleJumpVelocity * _jumpCoef;
                        _isDoubleJumped = true;
                    }
				}
				return;
			}

            if (OnSlimeBlock)
            {
				PlayerAnimator.Jump ();
				_velocity.y = (_model.DoubleJumpVelocity * _jumpCoef) / 1.5f;
                _isDoubleJumped = false;
                _onSlimeBlockDoubleJump = true;
            }
            else
            {
				PlayerAnimator.Jump ();
				_velocity.y = _model.JumpVelocity * _jumpCoef;
                _isDoubleJumped = false;
                _onSlimeBlockDoubleJump = false;
            }
		}

		public void ApplyGravityForce ()
		{
			_velocity.y += _model.GravityForce * Time.deltaTime;

			if (_velocity.y < -1) 
			{
				PlayerAnimator.Fall ();
			}

			Move (_velocity * Time.deltaTime);
		}

		public void ImpactVelocity (Vector3 velocity)
		{
			_velocity = velocity;
			Move ( _velocity * Time.deltaTime );
		}


        

        void CheckVerticalCollisions ( ref Vector3 velocity )
		{
			float dirY = Mathf.Sign(velocity.y);
			float rayLength = Mathf.Abs(velocity.y) + PlayerModel.kSkinWidth;

			for (int i = 0; i < _view.Raycaster.VertRayCount; i++)
			{
				Vector2 rayOrigin = (dirY == -1) ? _view.Raycaster.Origins.bottomLeft : _view.Raycaster.Origins.topLeft;
				rayOrigin += Vector2.right * (_view.Raycaster.VertRaySpacing * i + velocity.x);

				RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * dirY, rayLength, _view.Raycaster.CollisionMask);

                //Debug.DrawRay (rayOrigin, Vector2.up * dirY * rayLength, Color.green);

                if (hit)
                {
                    if (_isFirstHit)
                    {
                        _isFirstHit = false;
                        _playerSoundHandler.Landing();

                    }
                    // platform collision
                    if (!(hit.collider.tag == "Platform" && dirY == 1))
                    {
                        velocity.y = (hit.distance - PlayerModel.kSkinWidth) * dirY;

                        rayLength = hit.distance;

                        _model.CollisionInfo.IsAbove = dirY == 1;
                        _model.CollisionInfo.IsBelow = dirY == -1;

                        IceBlock ice = hit.collider.GetComponent<IceBlock>();
                        if (ice != null)
                        {
                            _onIce = true;
                            PlayerAnimator.StayOnGround();
                        }
                        else
                        {
                            _onIce = false;
                            PlayerAnimator.StayOnGround();
                        }
                    }
                }
            }
		}

		void CheckHorizontalCollisions ( ref Vector3 velocity )
		{
			float dirX = Mathf.Sign (velocity.x);
			float rayLength = Mathf.Abs (velocity.x) + PlayerModel.kSkinWidth;

			for (int i = 0; i < _view.Raycaster.HorRayCount; i++)
			{
				Sprite.transform.localScale = (dirX == -1) ? new Vector3 (-1,1,1) : Vector3.one;

				Vector2 rayOrigin = (dirX == -1) ? _view.Raycaster.Origins.bottomLeft : _view.Raycaster.Origins.bottomRight;
				rayOrigin += Vector2.up * (_view.Raycaster.HorRaySpacing * i);

				RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.right * dirX, rayLength, _view.Raycaster.CollisionMask);

				Debug.DrawRay (rayOrigin, Vector2.right * dirX * rayLength, Color.green);

				if (hit)
				{
					velocity.x = (hit.distance - PlayerModel.kSkinWidth) * dirX;

					rayLength = hit.distance;

					_model.CollisionInfo.IsLeft = dirX == -1;
					_model.CollisionInfo.IsRight = dirX == 1;
				}
			}
		}

		void Move ( Vector3 velocity )
		{
            if (!_model.CollisionInfo.IsBelow)
                _onIce = false;

			_model.CollisionInfo.Reset ();
			_view.Raycaster.UpdateRaycastOrigins ();

			if (velocity.x != 0)
				CheckHorizontalCollisions (ref velocity);

			if (velocity.y != 0)
				CheckVerticalCollisions (ref velocity);

			transform.Translate (velocity);
		}

		public void OnDie ()
		{
			//this.transform.position = _model.LastCheckpoint.Position;

			ResetPlayer ();

			if (OnPlayerDied != null)
				OnPlayerDied ();
		}

        public void OnExplosion()
        {
			_model.CurrentHP -= 1;
			_view.HP.UpdateHP (_model.CurrentHP);
			PlayerAnimator.Harmed ();
			if (_model.CurrentHP <= 0)
			{
				//LosePopUp.GetComponent<Image>().enabled = true;
				LosePopUp.GetComponent<WinPopUp>().SetPanelActive();
				//OnDie ();
			}
        }
    }
}
