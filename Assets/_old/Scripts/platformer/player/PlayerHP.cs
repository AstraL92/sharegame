﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace platformer.player
{
	[RequireComponent(typeof(Image))]
	public class PlayerHP : MonoBehaviour
	{
		private Image _hp;
		private int _maxHP;

		void Awake ()
		{
			_hp = this.GetComponent<Image> ();
		}

		void Start ()
		{
		}

		public void Init ( int maxHP )
		{
			_maxHP = maxHP;
			UpdateHP (_maxHP);
		}

		public void UpdateHP ( int HP )
		{
			float percent = Convert.ToSingle (HP) / Convert.ToSingle (_maxHP);
			_hp.fillAmount = percent;
		}

	}
}