﻿using UnityEngine;
using platformer.domain;
using platformer.utils;
using platformer.levelObjects;

namespace platformer.player
{
	public class PlayerModel 
	{
		public Checkpoint LastCheckpoint;

		public CollisionInfo CollisionInfo;

		public float GravityForce;
		public float JumpVelocity;
		public float DoubleJumpVelocity;
		public float MoveSpeed = 6f;
		public float AccelTimeAirborne;
		public float AccelTimeGrounded;

		public int CurrentHP;
		public int MaxHP;

		public const float kSkinWidth = .015f;

        //TODO: move to playerConfig
        [Header ("Player config")]
        [SerializeField]
        private const float _kJumpHeight = 3f;
        [SerializeField]
        private const float _kTimeToJumpApex = 0.5f;
        [SerializeField]
        private const float _kAccelTimeAirborne = 0.2f;
        [SerializeField]
        private const float _kAccelTimeGrounded = 0.1f;
        [SerializeField]
        private const int _kMaxHP = 3;

		public PlayerModel ()
		{
			MaxHP = _kMaxHP;
			CurrentHP = MaxHP;

			AccelTimeAirborne = _kAccelTimeAirborne;
			AccelTimeGrounded = _kAccelTimeGrounded;

			//calculate initial values of gravity force and jump velocity
			GravityForce = -(2 * _kJumpHeight) / Mathf.Pow( _kTimeToJumpApex, 2);
			JumpVelocity = Mathf.Abs (GravityForce) * _kTimeToJumpApex;
			DoubleJumpVelocity = JumpVelocity * 1.2f;

			MonoLog.Log (MonoChannel.Player, "Setted GravityForce to " + GravityForce + " and JumpVelocity to " + JumpVelocity); 
		}
	}
}
