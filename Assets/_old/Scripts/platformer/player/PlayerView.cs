﻿using UnityEngine;
using platformer.common;
using UnityEngine.UI;
using Com.LuisPedroFonseca.ProCamera2D;
using Assets.Scripts.MenuControllers.Login;

namespace platformer.player
{
	[RequireComponent(typeof(Raycaster))]
	[RequireComponent(typeof(PlayerController))]
	public class PlayerView : MonoBehaviour 
	{
		public SpriteRenderer _playerAvatar;

		[SerializeField] private PlayerHP _HP;
		public PlayerHP HP { get { return _HP; } }


		//player settings
		[SerializeField] private bool _isDoubleJumpAllowed = true;
		public bool IsDoubleJumpAllowed { get { return _isDoubleJumpAllowed; } }

		private PlayerController _controller;

		private Raycaster _raycaster;
		public Raycaster Raycaster { get { return _raycaster; } }

		void Start ()
		{
			_controller = this.GetComponent<PlayerController> ();

			_raycaster = this.GetComponent<Raycaster> ();

			TrySetPlayerAvater ();

			ProCamera2D.Instance.AddCameraTarget (transform);
		}

		void TrySetPlayerAvater ()
		{
			if (LoginController.UserAvatarStatic == null)
				return;
			
			_playerAvatar.material.SetTexture("_FaceTex", LoginController.UserAvatarStatic);
			_playerAvatar.gameObject.SetActive (true);

		}

		void Update ()
		{
			_controller.CheckCollisionInfo ();

			Vector2 input = Vector2.zero;

			input = new Vector2 (ETCInput.GetAxis ("Horizontal"), ETCInput.GetAxis ("Vertical"));

			_controller.ApplyInput (input);

			if (ETCInput.GetButtonDown ("Button jump"))
			{
				_controller.ApplyJump ();
			}

			_controller.ApplyGravityForce ();
		}

		void OnGUI ()
		{
			//GUI.Label(new Rect(0, 0, 100, 20), string.Format("IsAbove: {0}", _controller.Model.CollisionInfo.IsAbove));
			//GUI.Label(new Rect(0, 20, 100, 20), string.Format("IsBelow: {0}", _controller.Model.CollisionInfo.IsBelow));
			//GUI.Label(new Rect(0, 40, 100, 20), string.Format("IsRight: {0}", _controller.Model.CollisionInfo.IsRight));
			//GUI.Label(new Rect(0, 60, 100, 20), string.Format("IsLeft: {0}", _controller.Model.CollisionInfo.IsLeft));
		}

	}
}