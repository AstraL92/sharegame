﻿using UnityEngine;

namespace platformer.utils
{
    public static class ConvertImageUtils
    {
        public static byte[] ToByteArray(this Texture2D convertingTexture2D)
        {
            return convertingTexture2D.EncodeToPNG();
        }

        public static byte[] ToByteArray(this Sprite convertingSprite)
        {
            return convertingSprite.texture.ToByteArray();      
        }

        public static Texture2D ToTexture2D(this byte[] byteArray)
        {
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(byteArray);
            return tex;
        }

        public static Sprite ToSprite(this byte[] byteArray)
        {
            Texture2D tex = byteArray.ToTexture2D();
            return Sprite.Create(tex,new Rect(0,0,tex.width,tex.height), Vector2.zero);
        }
    }
}
