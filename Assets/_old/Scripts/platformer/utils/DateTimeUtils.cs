﻿using UnityEngine;
using System;

namespace platformer.utils
{
	public static class DateTimeUtils 
	{
		public static Int32 ConvertToUnixTime (this DateTime dt)
		{
			TimeSpan ts = dt - new DateTime(1970, 1, 1, 0, 0, 0);
			return (Int32)ts.TotalSeconds;
		}

		public static DateTime ConvertFromUnixTime (this Int32 unixDT)
		{
			return new DateTime (1970, 1, 1, 0, 0, 0).AddSeconds (unixDT);
		}
	}
}
