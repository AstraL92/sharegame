﻿using UnityEngine;
using System.Collections.Generic;

namespace platformer.utils
{
	public enum MonoChannel
	{
		All,
		Data,
		UI,
		Player,
		LevelEvents,
		Network
	}

	public class MonoLog 
	{
		private static List<MonoChannel> _channels;

		static MonoLog ()
		{
			_channels = new List<MonoChannel> ();

			//_channels.Add (MonoChannel.All);
			_channels.Add(MonoChannel.UI);
		}

		public static void Log ( MonoChannel channel, string text )
		{
			if (_channels.Contains (channel) || _channels.Contains (MonoChannel.All))
			{
				Debug.Log (string.Format ( "[{0}]: {1}", channel.ToString(), text));
			}
		}

		public static void LogWarning ( MonoChannel channel, string text )
		{
			if (_channels.Contains (channel) || _channels.Contains (MonoChannel.All))
			{
				Debug.LogWarning (string.Format ( "[{0}]: {1}", channel.ToString(), text));
			}
		}

		public static void LogError ( MonoChannel channel, string text )
		{
			if (_channels.Contains (channel) || _channels.Contains (MonoChannel.All))
			{
				Debug.LogError (string.Format ( "[{0}]: {1}", channel.ToString(), text));
			}
		}
	}
}
