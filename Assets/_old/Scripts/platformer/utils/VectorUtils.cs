﻿using UnityEngine;

namespace platformer.utils
{
	public static class VectorUtils 
	{
		public static Vector3 ConvertToVector3 (this Vector2 v)
		{
			return new Vector3 (v.x, v.y, 0);
		}

		public static Vector2 ConvertToVector2 (this Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}

		public static Vector3 SetX (this Vector3 v, float newX)
		{
			return new Vector3(newX, v.y, v.z);
		}

		public static Vector3 SetY (this Vector3 v, float newY)
		{
			return new Vector3(v.x, newY, v.z);
		}

		public static Vector3 SetZ (this Vector3 v, float newZ)
		{
			return new Vector3(v.x, v.y, newZ);
		}

		public static Vector3 AddX (this Vector3 v, float x)
		{
			return new Vector3(v.x + x, v.y, v.z);
		}

		public static Vector3 AddY (this Vector3 v, float y)
		{
			return new Vector3(v.x, v.y + y, v.z);
		}

		public static Vector3 AddZ (this Vector3 v, float z)
		{
			return new Vector3(v.x, v.y, v.z + z);
		}

	}
}
